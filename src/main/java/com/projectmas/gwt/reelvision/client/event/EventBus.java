package com.projectmas.gwt.reelvision.client.event;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-21.
 */


public class EventBus {

    private EventBus() {
    }

    private static final SimpleEventBus INSTANCE = GWT
            .create(SimpleEventBus.class);

    public static SimpleEventBus get() {
        return INSTANCE;
    }
}