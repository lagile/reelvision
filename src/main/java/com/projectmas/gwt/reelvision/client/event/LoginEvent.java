package com.projectmas.gwt.reelvision.client.event;

import com.google.web.bindery.event.shared.Event;
import com.projectmas.gwt.reelvision.client.event.handler.LoginHandler;
import com.projectmas.gwt.reelvision.shared.dto.UserDTO;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-21.
 */
public class LoginEvent extends Event<LoginHandler> {
    public static final Type<LoginHandler> TYPE = new Type<LoginHandler>();
    private UserDTO user;

    public LoginEvent(UserDTO user) {
        this.user = user;
    }

    @Override
    public Type<LoginHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(LoginHandler loginHandler) {
        loginHandler.onLogin(this);
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}
