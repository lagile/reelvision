package com.projectmas.gwt.reelvision.client.event;

import com.google.web.bindery.event.shared.Event;
import com.projectmas.gwt.reelvision.client.event.handler.LogoutHandler;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-21.
 */
public class LogoutEvent extends Event<LogoutHandler> {
    public static final Type<LogoutHandler> TYPE = new Type<LogoutHandler>();

    @Override
    public Type<LogoutHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(LogoutHandler logoutHandler) {
        logoutHandler.onLogout(this);
    }

}
