package com.projectmas.gwt.reelvision.client.event;

import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.Event;
import com.projectmas.gwt.reelvision.client.event.handler.NavigationClickHandler;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-21.
 */
public class WidgetClickEvent extends Event<NavigationClickHandler> {
    public static final Type<NavigationClickHandler> TYPE = new Type<NavigationClickHandler>();
    private Widget widget;

    public WidgetClickEvent(Widget widget) {
        this.widget = widget;
    }

    @Override
    public Type<NavigationClickHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(NavigationClickHandler navigationClickHandler) {
        navigationClickHandler.onClick(this);
    }

    public Widget getWidget() {
        return widget;
    }

    public void setWidget(Widget widget) {
        this.widget = widget;
    }
}
