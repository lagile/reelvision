package com.projectmas.gwt.reelvision.client.event.handler;

import com.google.gwt.event.shared.EventHandler;
import com.projectmas.gwt.reelvision.client.event.LoginEvent;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-21.
 */

public interface LoginHandler extends EventHandler {

    public void onLogin(LoginEvent loginEvent);

}