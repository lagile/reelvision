package com.projectmas.gwt.reelvision.client.resource;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-18.
 */

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;

/**
 * Resources used by the entire application.
 **/
public interface Resources extends ClientBundle {

    public static final Resources INSTANCE = GWT.create(Resources.class);

    @ClientBundle.Source("CustomBootstrap.css")
    Style getStyle();

    interface Style extends CssResource {

        @ClassName("dropdown-menu")
        String dropdownMenu();

        @ClassName("message-preview")
        String messagePreview();

        @ClassName("panel-heading")
        String panelHeading();

        @ClassName("message-dropdown")
        String messageDropdown();

        @ClassName("alert-dropdown")
        String alertDropdown();

        @ClassName("side-nav")
        String sideNav();

        @ClassName("message-footer")
        String messageFooter();

        @ClassName("top-nav")
        String topNav();

        @ClassName("panel-green")
        String panelGreen();

        @ClassName("flot-chart")
        String flotChart();

        @ClassName("panel-red")
        String panelRed();

        @ClassName("huge")
        String huge();

        @ClassName("flot-chart-content")
        String flotChartContent();

        @ClassName("open")
        String open();

        @ClassName("panel-yellow")
        String panelYellow();

        @ClassName("wrapper")
        String wrapper();

        @ClassName("page-wrapper")
        String pageWrapper();

        @ClassName("wrapper-menu")
        String wrapperMenu();

        @ClassName("row-label")
        String rowLabel();

        @ClassName("seat-button")
        String seatButton();

        @ClassName("seat-plan")
        String seatPlan();


        @ClassName("seat-avail")
        String seatAvail();

        @ClassName("seat-occupied")
        String seatOccupied();

        @ClassName("seat-chosen")
        String seatChosen();
    }
}