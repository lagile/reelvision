package com.projectmas.gwt.reelvision.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;
import com.projectmas.gwt.reelvision.server.model.Movie;
import com.projectmas.gwt.reelvision.shared.dto.MovieDTO;

import java.util.ArrayList;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
@RemoteServiceRelativePath("MovieService")
public interface MovieService extends RemoteService {

    class App {
        private static MovieServiceAsync instance;

        public static MovieServiceAsync getInstance() {
            if (instance == null) {
                instance = GWT.create(MovieService.class);
            }
            return instance;
        }
    }

    void createMovie(String title, String director, String country, Date releaseDate, String description);

    void deleteMovie(Long id);

    void updateMovie(MovieDTO movieDTO);

    ArrayList<MovieDTO> getMovies();
}
