package com.projectmas.gwt.reelvision.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.projectmas.gwt.reelvision.server.model.Movie;
import com.projectmas.gwt.reelvision.shared.dto.MovieDTO;

import java.util.ArrayList;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
public interface MovieServiceAsync {

    void createMovie(String title, String director, String country, Date releaseDate, String description, AsyncCallback<Void> async);

    void deleteMovie(Long id, AsyncCallback<Void> async);

    void updateMovie(MovieDTO movieDTO, AsyncCallback<Void> async);

    void getMovies(AsyncCallback<ArrayList<MovieDTO>> async);
}
