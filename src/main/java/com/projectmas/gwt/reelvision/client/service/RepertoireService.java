package com.projectmas.gwt.reelvision.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.projectmas.gwt.reelvision.shared.dto.MovieDTO;
import com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO;

import java.util.ArrayList;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
@RemoteServiceRelativePath("RepertoireService")
public interface RepertoireService extends RemoteService {

    class App {
        private static RepertoireServiceAsync instance;

        public static RepertoireServiceAsync getInstance() {
            if (instance == null) {
                instance = GWT.create(RepertoireService.class);
            }
            return instance;
        }
    }

    void createRepertoire(String title, Date startDate, Date endDate);

    void deleteRepertoire(Long id);

    void updateRepertoire(RepertoireDTO repertoire);

    RepertoireDTO getRepertoire(String title);

    ArrayList<RepertoireDTO> getRepertoires();
}
