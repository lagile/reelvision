package com.projectmas.gwt.reelvision.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.projectmas.gwt.reelvision.shared.dto.MovieDTO;
import com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO;

import java.util.ArrayList;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
public interface RepertoireServiceAsync {

    void createRepertoire(String title, Date startDate, Date endDate, AsyncCallback<Void> async);

    void deleteRepertoire(Long id, AsyncCallback<Void> async);

    void updateRepertoire(RepertoireDTO repertoire, AsyncCallback<Void> async);

    void getRepertoires(AsyncCallback<ArrayList<RepertoireDTO>> async);

    void getRepertoire(String title, AsyncCallback<RepertoireDTO> async);
}
