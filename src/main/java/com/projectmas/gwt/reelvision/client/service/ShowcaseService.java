package com.projectmas.gwt.reelvision.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.projectmas.gwt.reelvision.shared.dto.MovieDTO;
import com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO;
import com.projectmas.gwt.reelvision.shared.dto.SeatDTO;
import com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO;

import java.util.ArrayList;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
@RemoteServiceRelativePath("ShowcaseService")
public interface ShowcaseService extends RemoteService {

    ArrayList<ShowcaseDTO> getShowcases(Long repertoireId);

    class App {
        private static ShowcaseServiceAsync instance;

        public static ShowcaseServiceAsync getInstance() {
            if (instance == null) {
                instance = GWT.create(ShowcaseService.class);
            }
            return instance;
        }
    }

    ArrayList<MovieDTO> getMovies(Long repertoireId);

    ArrayList<ScreeningDTO> getScreenings(Long showcaseId);

    ArrayList<SeatDTO> getSeats(Long auditoriumId);

    ArrayList<SeatDTO> getOccupiedSeats(Long auditoriumId, Long screenngId);

    Boolean isSeatOccupied(Long auditoriumId, Long screenngId, Long seatID);
}
