package com.projectmas.gwt.reelvision.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.projectmas.gwt.reelvision.shared.dto.*;

import java.util.ArrayList;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
public interface ShowcaseServiceAsync {

    void getMovies(Long repertoireId, AsyncCallback<ArrayList<MovieDTO>> async);

    void getScreenings(Long showcaseId, AsyncCallback<ArrayList<ScreeningDTO>> async);

    void getShowcases(Long repertoireId, AsyncCallback<ArrayList<ShowcaseDTO>> async);

    void getSeats(Long auditoriumId, AsyncCallback<ArrayList<SeatDTO>> async);

    void getOccupiedSeats(Long auditoriumId, Long screenngId, AsyncCallback<ArrayList<SeatDTO>> async);

    void isSeatOccupied(Long auditoriumId, Long screenngId, Long seatID, AsyncCallback<Boolean> async);
}
