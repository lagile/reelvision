package com.projectmas.gwt.reelvision.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;
import com.projectmas.gwt.reelvision.shared.dto.TicketDTO;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-13.
 */
@RemoteServiceRelativePath("TicketService")
public interface TicketService extends RemoteService {

    List<TicketDTO> createTickets(List<TicketDTO> ticketDTOList);

    class App {
        private static TicketServiceAsync instance;

        public static TicketServiceAsync getInstance() {
            if (instance == null) {
                instance = GWT.create(TicketService.class);
            }
            return instance;
        }
    }

}
