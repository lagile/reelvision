package com.projectmas.gwt.reelvision.client.service;


import com.google.gwt.user.client.rpc.AsyncCallback;
import com.projectmas.gwt.reelvision.shared.dto.TicketDTO;

import java.util.List;
import java.util.Map;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-13.
 */
public interface TicketServiceAsync {

    void createTickets(List<TicketDTO> ticketDTOList, AsyncCallback<List<TicketDTO>> async);
}
