package com.projectmas.gwt.reelvision.client.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO;
import com.projectmas.gwt.reelvision.shared.dto.UserDTO;

import java.util.ArrayList;

/**
 * Created by lukasz on 2016-04-22.
 */
@RemoteServiceRelativePath("UserService")
public interface UserService extends RemoteService {

    class App {
        private static UserServiceAsync instance;

        public static UserServiceAsync getInstance() {
            if (instance == null) {
                instance = GWT.create(UserService.class);
            }
            return instance;
        }
    }

    UserDTO loginUser(String login, String password);

    UserDTO loginUserFromSession();

    UserDTO getCurrentUser();

    void createUser(String login, String password, String privClassName);

    void removeUser(Long id);

    boolean changeUserPassword(String login, String newPassword);

    void logoutUser();

    ArrayList<UserDTO> getUsers();

    ArrayList<PrivClassDTO> getPrivClasses();
}
