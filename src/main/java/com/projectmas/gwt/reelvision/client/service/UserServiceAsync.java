package com.projectmas.gwt.reelvision.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO;
import com.projectmas.gwt.reelvision.shared.dto.UserDTO;

import java.util.ArrayList;

/**
 * Created by lukasz on 2016-04-22.
 */
public interface UserServiceAsync {

    void loginUser(String login, String password, AsyncCallback<UserDTO> async);

    void loginUserFromSession(AsyncCallback<UserDTO> async);

    void getCurrentUser(AsyncCallback<UserDTO> async);

    void createUser(String login, String password, String privClassName, AsyncCallback<Void> async);

    void removeUser(Long id, AsyncCallback<Void> async);

    void changeUserPassword(String login, String newPassword, AsyncCallback<Boolean> async);

    void logoutUser(AsyncCallback<Void> async);

    void getUsers(AsyncCallback<ArrayList<UserDTO>> async);

    void getPrivClasses(AsyncCallback<ArrayList<PrivClassDTO>> async);

}
