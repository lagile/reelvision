package com.projectmas.gwt.reelvision.client.ui;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.projectmas.gwt.reelvision.client.event.*;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.projectmas.gwt.reelvision.client.event.handler.LoginHandler;
import com.projectmas.gwt.reelvision.client.event.handler.LogoutHandler;
import com.projectmas.gwt.reelvision.client.event.handler.NavigationClickHandler;
import com.projectmas.gwt.reelvision.client.service.UserService;
import com.google.gwt.user.client.ui.RootPanel;
import com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate;
import com.projectmas.gwt.reelvision.client.ui.login.LoginForm;
import com.projectmas.gwt.reelvision.client.ui.moviemanagement.MovieManagementForm;
import com.projectmas.gwt.reelvision.client.ui.repertoiremanagement.RepertoireManagementForm;
import com.projectmas.gwt.reelvision.client.ui.sale.SaleForm;
import com.projectmas.gwt.reelvision.client.ui.usermanagement.UserManagementForm;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.UserDTO;
import org.gwtbootstrap3.client.ui.Anchor;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ApplicationEntry implements EntryPoint {

    private EntryTemplate entryTemplate = new EntryTemplate();

    public void onModuleLoad() {
        String sessionID = Cookies.getCookie("sid");
        registerEvents();

        if (sessionID != null) {
            UserService.App.getInstance().loginUserFromSession(new AsyncCallback<UserDTO>() {
                @Override
                public void onFailure(Throwable caught) {
                    onLoggedOut();
                }

                @Override
                public void onSuccess(UserDTO result) {
                    if (result != null) {
                        onLoggedIn(result);
                    } else {
                        onFailure(new Throwable());
                    }
                }
            });
        } else {
            onLoggedOut();
        }
    }

    private void registerEvents() {
        GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
            public void onUncaughtException(Throwable e) {
//                onLoggedOut();
                Window.alert(e.getMessage());
            }
        });

        EventBus.get().addHandler(LoginEvent.TYPE, new LoginHandler() {
            @Override
            public void onLogin(LoginEvent loginEvent) {
                onLoggedIn(loginEvent.getUser());
            }
        });

        EventBus.get().addHandler(LogoutEvent.TYPE, new LogoutHandler() {
            @Override
            public void onLogout(LogoutEvent logoutEvent) {
                onLoggedOut();
            }
        });

        EventBus.get().addHandler(WidgetClickEvent.TYPE, new NavigationClickHandler() {
            @Override
            public void onClick(WidgetClickEvent widgetClickEvent) {
                if (widgetClickEvent.getWidget() instanceof Anchor) {
                    Anchor anchor = (Anchor) widgetClickEvent.getWidget();
                    switch (anchor.getId()) {
                        case Constants.NEW_SALE_ANCHOR_ID: {
                            entryTemplate.setContent(new SaleForm());
                            return;
                        }
                        case Constants.USER_MANAGEMENT_ID: {
//                            entryTemplate.setContent(new UserManagementForm());
                            return;
                        }
                        case Constants.MOVIE_MANAGEMENT_ID: {
//                            entryTemplate.setContent(new MovieManagementForm());
                            return;
                        }
                        case Constants.REPERTOIRE_MANAGEMENT_ID: {
//                            entryTemplate.setContent(new RepertoireManagementForm());
                            return;
                        }
                    }
                }
            }
        });
    }

    private void setRootPanel(Widget content) {
        RootPanel.get().clear();
        RootPanel.get().add(content);
    }

    public void onLoggedIn(UserDTO user) {
        entryTemplate.addUserNavigation(user);
        setRootPanel(entryTemplate);
    }

    public void onLoggedOut() {
        UserService.App.getInstance().logoutUser(new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(Constants.LOG_OUT_PROBLEM);
            }

            @Override
            public void onSuccess(Void aVoid) {
                entryTemplate = new EntryTemplate();
                entryTemplate.addContent(new LoginForm());
                setRootPanel(entryTemplate);
            }

        });
    }
}
