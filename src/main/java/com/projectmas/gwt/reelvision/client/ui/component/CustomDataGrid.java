package com.projectmas.gwt.reelvision.client.ui.component;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;
import com.projectmas.gwt.reelvision.shared.Constants;

import com.projectmas.gwt.reelvision.shared.dto.ObjectDTO;
import javafx.scene.control.SelectionMode;
import org.gwtbootstrap3.client.ui.gwt.DataGrid;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
public class CustomDataGrid<T> extends Composite {

    @UiField
    SimpleLayoutPanel panel;
    @UiField
    DataGrid dataGrid;
    private List<AbstractEditableCell<?, ?>> editableCells;
    private List<ObjectDTO> rows = new ArrayList<>();
    private int cellHeight = 50;
    private int cellWidth = 150;

    public interface GetValue<C> {
        C getValue(ObjectDTO objectDTO);
    }

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.component.template.CustomDataGrid.ui.xml")
    interface MyUiBinder extends UiBinder<Widget, CustomDataGrid> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public CustomDataGrid() {
        initWidget(uiBinder.createAndBindUi(this));
        this.editableCells = new ArrayList<>();
    }

    public CustomDataGrid(SelectionModel selectionModel) {
        this();
        this.dataGrid.setSelectionModel(selectionModel);
    }

    public CustomDataGrid(CustomDataGrid customDataGrid) {
        this(customDataGrid.getDataGrid().getSelectionModel());
        this.rows = customDataGrid.getRows();
        this.dataGrid = customDataGrid.getDataGrid();
        this.cellHeight = customDataGrid.getCellHeight();
        this.cellWidth = customDataGrid.getCellWidth();
    }

    public <C> void addColumn(Cell<C> cell, String headerText,
                              final GetValue<C> getter) {
        Column<ObjectDTO, C> column = new Column<ObjectDTO, C>(cell) {
            @Override
            public C getValue(ObjectDTO object) {
                return getter.getValue(object);
            }
        };
        dataGrid.addColumn(column, headerText);
    }

    public <C> void addColumn(Cell<C> cell, String headerText,
                              final GetValue<C> getter, FieldUpdater<ObjectDTO, C> fieldUpdater) {
        Column<ObjectDTO, C> column = new Column<ObjectDTO, C>(cell) {
            @Override
            public C getValue(ObjectDTO object) {
                return getter.getValue(object);
            }
        };
        column.setFieldUpdater(fieldUpdater);
        if (cell instanceof AbstractEditableCell<?, ?>) {
            editableCells.add((AbstractEditableCell<?, ?>) cell);
        }
        dataGrid.addColumn(column, headerText);
    }

    public int getCellHeight() {
        return cellHeight;
    }

    public void setCellHeight(int cellHeight) {
        this.cellHeight = cellHeight;
    }

    public int getCellWidth() {
        return cellWidth;
    }

    public void setCellWidth(int cellWidth) {
        this.cellWidth = cellWidth;
    }

    private void setGridHeight(int height) {
        panel.setHeight(height + Constants.PX);
        dataGrid.setHeight(height + Constants.PX);
    }

    private void setGridWidth(int width) {
        panel.setWidth(width + Constants.PX);
        dataGrid.setWidth(width + Constants.PX);
    }

    public void addRow(ObjectDTO objectDTO) {
        if (!rows.contains(objectDTO)) {
            rows.add(objectDTO);
        }
    }

    public void clear() {
        rows.clear();
        update();
    }

    public void removeRow(ObjectDTO objectDTO) {
        if (rows.contains(objectDTO)) {
            rows.remove(objectDTO);
        }
    }

    public List<ObjectDTO> getRows() {
        return rows;
    }

    public DataGrid getDataGrid() {
        return dataGrid;
    }

    public void update() {
        dataGrid.setRowCount(rows.size(), true);
        dataGrid.setRowData(0, rows);
        setGridHeight(rows.size() * cellHeight);
        setGridWidth(dataGrid.getColumnCount() * cellWidth);
    }
}