package com.projectmas.gwt.reelvision.client.ui.component;

import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.ObjectDTO;
import org.gwtbootstrap3.client.ui.ListBox;

import java.util.Map;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-08.
 */
public class CustomListBox extends ListBox {

    private Map<String, ObjectDTO> valueMap;

    public CustomListBox() {
        setDefault();
    }

    public void setDefault() {
        if (this.getItemCount() != 0) {
            this.clear();
        }
        this.setEnabled(false);
        this.addItem(Constants.CHOOSE);
    }


    public void setListBoxValues(Map<String, ObjectDTO> valueMap) {
        setDefault();
        for (Map.Entry<String, ObjectDTO> entry : valueMap.entrySet()) {
            this.addItem(entry.getKey());
        }
        if (valueMap.size() != 0) {
            this.setEnabled(true);
        }
        this.valueMap = valueMap;
    }

    public ObjectDTO getSelectedItem() {
        return Constants.CHOOSE.equals(this.getSelectedItemText()) ? null : valueMap.get(this.getSelectedItemText());
    }

}
