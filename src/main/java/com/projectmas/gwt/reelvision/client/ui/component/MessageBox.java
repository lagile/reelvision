package com.projectmas.gwt.reelvision.client.ui.component;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import com.projectmas.gwt.reelvision.shared.Constants;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.ListItem;
import org.gwtbootstrap3.client.ui.html.OrderedList;
import org.gwtbootstrap3.client.ui.html.Paragraph;

import java.util.ConcurrentModificationException;
import java.util.List;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-17.
 */
public class MessageBox extends DialogBox implements ClickHandler {

    public MessageBox(String heading, Widget widget) {
        setText(heading);
        Button closeButton = new Button("OK", this);
        DockPanel dock = new DockPanel();
        dock.setSpacing(4);
        dock.add(widget, DockPanel.NORTH);
        dock.add(closeButton, DockPanel.SOUTH);
        dock.setCellHorizontalAlignment(closeButton, DockPanel.ALIGN_CENTER);
        this.setHeight("100%");
        this.setWidth("100%");
        setWidget(dock);
    }

    @Override
    public void onClick(ClickEvent clickEvent) {
        hide();
    }
}
