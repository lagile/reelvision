package com.projectmas.gwt.reelvision.client.ui.component;

import com.projectmas.gwt.reelvision.client.resource.Resources;
import org.gwtbootstrap3.client.ui.Label;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-11.
 */
public class RowLabel extends Label {

    public RowLabel(Character c) {
        super(String.valueOf(c));
        this.addStyleName(Resources.INSTANCE.getStyle().rowLabel());
    }

}
