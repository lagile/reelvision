package com.projectmas.gwt.reelvision.client.ui.component;

import com.projectmas.gwt.reelvision.client.resource.Resources;
import com.projectmas.gwt.reelvision.shared.dto.SeatDTO;
import org.gwtbootstrap3.client.ui.Button;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-10.
 */
public class SeatButton extends Button {

    private SeatDTO seatDTO;
    private boolean isChosen;

    public SeatButton(SeatDTO seatDTO) {
        this.seatDTO = seatDTO;
        addStyleName(Resources.INSTANCE.getStyle().seatButton());
    }

    public void setAvailable() {
        setEnabled(true);
        if (getStyleName().contains(Resources.INSTANCE.getStyle().seatOccupied())) {
            removeStyleName(Resources.INSTANCE.getStyle().seatOccupied());
        }
        if (getStyleName().contains(Resources.INSTANCE.getStyle().seatChosen())) {
            removeStyleName(Resources.INSTANCE.getStyle().seatChosen());
        }
        addStyleName(Resources.INSTANCE.getStyle().seatAvail());
        isChosen = false;
        setFocus(false);
    }

    public void setOccupied() {
        setEnabled(false);
        if (getStyleName().contains(Resources.INSTANCE.getStyle().seatAvail())) {
            removeStyleName(Resources.INSTANCE.getStyle().seatAvail());
        }
        addStyleName(Resources.INSTANCE.getStyle().seatOccupied());
        isChosen = false;
    }

    public void setChosen() {
        if (getStyleName().contains(Resources.INSTANCE.getStyle().seatAvail())) {
            removeStyleName(Resources.INSTANCE.getStyle().seatAvail());
        }
        addStyleName(Resources.INSTANCE.getStyle().seatChosen());
        isChosen = true;
        setFocus(false);
    }

    public SeatDTO getSeatDTO() {
        return seatDTO;
    }

    public void setSeatDTO(SeatDTO seatDTO) {
        this.seatDTO = seatDTO;
    }

    public boolean isChosen() {
        return isChosen;
    }
}
