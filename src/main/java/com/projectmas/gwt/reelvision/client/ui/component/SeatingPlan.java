package com.projectmas.gwt.reelvision.client.ui.component;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Widget;
import com.projectmas.gwt.reelvision.client.resource.Resources;
import com.projectmas.gwt.reelvision.client.service.ShowcaseService;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.SeatDTO;

import java.util.*;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-10.
 */
public class SeatingPlan extends Composite {

    @UiField
    Resources resources;
    @UiField
    Grid seatingPlanGrid;
    private Long auditoriumId;
    private Long screeningId;
    private List<SeatDTO> chosenSeatsList = new ArrayList<>();

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.component.template.SeatingPlan.ui.xml")
    interface MyUiBinder extends UiBinder<Widget, SeatingPlan> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public SeatingPlan() {
        initWidget(uiBinder.createAndBindUi(this));
        resources.getStyle().ensureInjected();
    }

    public void showSeatingPlan(Long auditoriumId, Long screeningId, final List<SeatDTO> chosenSeatsList) {
        if (auditoriumId == null || screeningId == null) {
            return;
        }
        if (this.auditoriumId == null || !this.auditoriumId.equals(auditoriumId)) {
            this.auditoriumId = auditoriumId;
            this.screeningId = screeningId;
            ShowcaseService.App.getInstance().getSeats(auditoriumId, new AsyncCallback<ArrayList<SeatDTO>>() {
                @Override
                public void onFailure(Throwable throwable) {
                    Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
                }

                @Override
                public void onSuccess(ArrayList<SeatDTO> seatDTOs) {
                    seatingPlanGrid.clear();
                    for (SeatDTO seatDTO : seatDTOs) {
                        int rowIndex = seatDTO.getRow() - 65;
                        int columnIndex = seatDTO.getNo();
                        if (seatingPlanGrid.getRowCount() < rowIndex + 1) {
                            seatingPlanGrid.resizeRows(rowIndex + 1);
                            if (seatingPlanGrid.getColumnCount() == 0) {
                                seatingPlanGrid.resizeColumns(1);
                            }
                            seatingPlanGrid.setWidget(rowIndex, 0, new RowLabel(seatDTO.getRow()));
                        }
                        if (seatingPlanGrid.getColumnCount() < columnIndex + 1) {
                            seatingPlanGrid.resizeColumns(columnIndex + 1);
                        }

                        SeatButton seatButton = new SeatButton(seatDTO);
                        seatButton.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent clickEvent) {
                                Object object = clickEvent.getSource();
                                if (object instanceof SeatButton) {
                                    SeatButton seatButton1 = (SeatButton) object;
                                    if (seatButton1.isChosen()) {
                                        SeatingPlan.this.chosenSeatsList.remove(seatButton1.getSeatDTO());
                                        seatButton1.setAvailable();
                                    } else {
                                        SeatingPlan.this.chosenSeatsList.add(seatButton1.getSeatDTO());
                                        seatButton1.setChosen();
                                    }
                                }
                            }
                        });
                        seatingPlanGrid.setWidget(rowIndex, columnIndex, seatButton);
                    }
                    updateSeatsStatus();
                    if (chosenSeatsList != null) {
                        setChosenSeats(chosenSeatsList);
                    }
                }
            });
            return;
        }
        this.screeningId = screeningId;
        updateSeatsStatus();
        if (chosenSeatsList != null) {
            setChosenSeats(chosenSeatsList);
        }
    }

    public void updateSeatsStatus() {
        if (auditoriumId == null || screeningId == null) {
            return;
        }
        for (int row = 0; row < seatingPlanGrid.getRowCount(); row++) {
            for (int col = 1; col < seatingPlanGrid.getColumnCount(); col++) {
                ((SeatButton) seatingPlanGrid.getWidget(row, col)).setAvailable();
            }
        }
        ShowcaseService.App.getInstance().getOccupiedSeats(auditoriumId, screeningId, new AsyncCallback<ArrayList<SeatDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
                Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
            }

            @Override
            public void onSuccess(ArrayList<SeatDTO> seatDTOs) {
                for (SeatDTO seatDTO : seatDTOs) {
                    if (chosenSeatsList.contains(seatDTO)) {
                        chosenSeatsList.remove(seatDTO);
                        Window.alert("Seat " + seatDTO.getRow() + ":" + seatDTO.getNo() + " has been occupied! Please choose another seat.");
                    }
                    ((SeatButton) seatingPlanGrid.getWidget(seatDTO.getRow() - 65, seatDTO.getNo())).setOccupied();
                }
            }
        });
    }

    private void setChosenSeats(List<SeatDTO> seatList) {
        if (auditoriumId == null || screeningId == null) {
            return;
        }
        for (SeatDTO seatDTO : seatList) {
            ((SeatButton) seatingPlanGrid.getWidget(seatDTO.getRow() - 65, seatDTO.getNo())).setChosen();
        }
        this.chosenSeatsList = seatList;
    }


    public Long getAuditoriumId() {
        return auditoriumId;
    }

    public Long getScreeningId() {
        return screeningId;
    }

    public List<SeatDTO> getChosenSeatsList() {
        return chosenSeatsList;
    }
}