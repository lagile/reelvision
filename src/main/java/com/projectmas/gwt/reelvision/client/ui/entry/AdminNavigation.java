package com.projectmas.gwt.reelvision.client.ui.entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.projectmas.gwt.reelvision.shared.Constants;
import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.html.UnorderedList;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-22.
 */
public class AdminNavigation extends Composite {

    @UiField
    UnorderedList operationList;
    @UiField
    Anchor showtimeManagement;
    @UiField
    Anchor movieManagement;
    @UiField
    Anchor repertoireManagement;
    @UiField
    Anchor userManagement;

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.entry.template.AdminNavigation.ui.xml")
    interface AdminOperationListUiBinder extends UiBinder<Widget, AdminNavigation> {
    }

    private static AdminOperationListUiBinder uiBinder = GWT.create(AdminOperationListUiBinder.class);

    public AdminNavigation() {
        initWidget(uiBinder.createAndBindUi(this));
        showtimeManagement.setId(Constants.SHOWTIME_MANAGEMENT_ID);
        movieManagement.setId(Constants.MOVIE_MANAGEMENT_ID);
        repertoireManagement.setId(Constants.REPERTOIRE_MANAGEMENT_ID);
        userManagement.setId(Constants.USER_MANAGEMENT_ID);
    }
}