package com.projectmas.gwt.reelvision.client.ui.entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.projectmas.gwt.reelvision.client.event.EventBus;
import com.projectmas.gwt.reelvision.client.event.LogoutEvent;
import com.projectmas.gwt.reelvision.client.service.UserService;
import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.Navbar;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-16.
 */
public class DropdownNavigation extends Composite {

    @UiField
    Navbar userNavigation;
    @UiField
    Anchor logoutAnchor;
    @UiField
    Anchor settingsAnchor;
    @UiField
    Anchor userToggle;

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.entry.template.DropdownNavigation.ui.xml")
    interface MyUiBinder extends UiBinder<Widget, DropdownNavigation> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public DropdownNavigation(String userLogin) {
        initWidget(uiBinder.createAndBindUi(this));
        userToggle.setText(userLogin);

        logoutAnchor.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                UserService.App.getInstance().logoutUser(new AsyncCallback<Void>() {

                    @Override
                    public void onSuccess(Void aVoid) {
                        EventBus.get().fireEvent(new LogoutEvent());
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        Window.alert("System encounter a problem! Please contact your administrator.");
                    }
                });
            }
        });
    }
}
