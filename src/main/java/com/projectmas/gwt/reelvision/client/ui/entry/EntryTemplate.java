package com.projectmas.gwt.reelvision.client.ui.entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.projectmas.gwt.reelvision.client.event.EventBus;
import com.projectmas.gwt.reelvision.client.event.LogoutEvent;
import com.projectmas.gwt.reelvision.client.resource.Resources;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.UserDTO;
import org.gwtbootstrap3.client.ui.Container;
import org.gwtbootstrap3.client.ui.Navbar;
import org.gwtbootstrap3.client.ui.html.Div;

/**
 * Created by lukasz on 2016-05-13.
 */
public class EntryTemplate extends Composite {

    @UiField
    HTMLPanel wrapper;
    @UiField
    Resources resources;
    @UiField
    Navbar navbar;
    @UiField
    Div content;

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.entry.template.Index.ui.xml")
    interface MyUiBinder extends UiBinder<Widget, EntryTemplate> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public EntryTemplate() {
        initWidget(uiBinder.createAndBindUi(this));
        resources.getStyle().ensureInjected();
    }

    public void addContent(Widget content) {
        if (this.content.getWidgetIndex(content) == -1) {
            this.content.add(content);
        }
    }

    public void setContent(Widget content) {
        clearContent();
        addContent(content);
    }

    public void clearContent() {
        this.content.clear();
    }

    public void addUserNavigation(UserDTO user) {
        navbar.add(new DropdownNavigation(user.getLogin()));
        if (user.getPrivClassDTO() != null) {
            navbar.add(new SideNavigation(user.getPrivClassDTO()));
//            wrapper.setStyleName(resources.getStyle().wrapperMenu());
        } else {
            EventBus.get().fireEvent(new LogoutEvent());
            Window.alert(Constants.PRIVCLASS_OBTAIN_PROBLEM);
        }
        content.clear();
    }
}
