package com.projectmas.gwt.reelvision.client.ui.entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.projectmas.gwt.reelvision.shared.Constants;
import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.html.UnorderedList;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-22.
 */

public class ManagerNavigation extends Composite {

    @UiField
    UnorderedList operationList;
    @UiField
    Anchor reportsAnchor;

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.entry.template.ManagerNavigation.ui.xml")
    interface ManagerOperationListUiBinder extends UiBinder<Widget, ManagerNavigation> {
    }

    private static ManagerOperationListUiBinder uiBinder = GWT.create(ManagerOperationListUiBinder.class);

    public ManagerNavigation() {
        initWidget(uiBinder.createAndBindUi(this));
        reportsAnchor.setId(Constants.REPORTS_ANCHOR_ID);
    }
}