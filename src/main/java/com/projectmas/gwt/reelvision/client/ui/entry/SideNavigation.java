package com.projectmas.gwt.reelvision.client.ui.entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.projectmas.gwt.reelvision.client.event.EventBus;
import com.projectmas.gwt.reelvision.client.event.WidgetClickEvent;
import com.projectmas.gwt.reelvision.client.resource.Resources;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO;
import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.ListItem;
import org.gwtbootstrap3.client.ui.html.UnorderedList;


/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-17.
 */
public class SideNavigation extends Composite {

    @UiField
    Resources resources;
    @UiField
    UnorderedList sideNavigation;


    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.entry.template.SideNavigation.ui.xml")
    interface MyUiBinder extends UiBinder<Widget, SideNavigation> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public SideNavigation(PrivClassDTO privClass) {
        initWidget(uiBinder.createAndBindUi(this));
        resources.getStyle().ensureInjected();
        switch (privClass.getClassName()) {
            case Constants.MANAGER: {
                ManagerNavigation managerNavigation = new ManagerNavigation();
                sideNavigation.add(managerNavigation);
                registerEvents(managerNavigation.operationList);
            }
            case Constants.ADMINISTRATOR: {
                AdminNavigation adminNavigation = new AdminNavigation();
                sideNavigation.insert(adminNavigation, 0);
                registerEvents(adminNavigation.operationList);
            }
            case Constants.USER: {
                UserNavigation userNavigation = new UserNavigation();
                sideNavigation.insert(userNavigation, 0);
                registerEvents(userNavigation.operationList);
            }
        }
    }

    public void registerEvents(UnorderedList ul) {
        for (int i = 0; i < ul.getWidgetCount(); i++) {
            if (ul.getWidget(i) instanceof ListItem) {
                ListItem li = (ListItem) ul.getWidget(i);
                for (int j = 0; j < li.getWidgetCount(); j++) {
                    if (li.getWidget(j) instanceof Anchor) {
                        final Anchor a = (Anchor) li.getWidget(j);
                        a.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent clickEvent) {
                                EventBus.get().fireEvent(new WidgetClickEvent(a));
                            }
                        });
                    } else if (li.getWidget(j) instanceof UnorderedList) {
                        registerEvents((UnorderedList) li.getWidget(j));
                    }
                }
            }
        }
    }
}