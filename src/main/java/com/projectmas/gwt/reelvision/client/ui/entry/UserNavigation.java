package com.projectmas.gwt.reelvision.client.ui.entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.projectmas.gwt.reelvision.shared.Constants;
import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.html.UnorderedList;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-22.
 */
public class UserNavigation extends Composite {

    @UiField
    UnorderedList operationList;
    @UiField
    Anchor saleAnchor;
    @UiField
    UnorderedList saleCollapse;
    @UiField
    Anchor reserveAnchor;
    @UiField
    UnorderedList reserveCollapse;
    @UiField
    Anchor dashboardAnchor;
    @UiField
    Anchor newSaleAnchor;
    @UiField
    Anchor searchSaleAnchor;
    @UiField
    Anchor newReserveAnchor;
    @UiField
    Anchor searchReserveAnchor;
    @UiField
    Anchor showtimesAnchor;
    @UiField
    Anchor moviesAnchor;
    @UiField
    Anchor audiencesAnchor;

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.entry.template.UserNavigation.ui.xml")
    interface UserOperationListUiBinder extends UiBinder<Widget, UserNavigation> {
    }

    private static UserOperationListUiBinder uiBinder = GWT.create(UserOperationListUiBinder.class);

    public UserNavigation() {
        initWidget(uiBinder.createAndBindUi(this));
        saleAnchor.setDataTargetWidget(saleCollapse);
        reserveAnchor.setDataTargetWidget(reserveCollapse);
        dashboardAnchor.setId(Constants.DASHBOARD_ANCHOR_ID);
        newSaleAnchor.setId(Constants.NEW_SALE_ANCHOR_ID);
        searchSaleAnchor.setId(Constants.SEARCH_SALE_ANCHOR_ID);
        newReserveAnchor.setId(Constants.NEW_RESERVE_ANCHOR_ID);
        searchReserveAnchor.setId(Constants.SEARCH_RESERVE_ANCHOR_ID);
        showtimesAnchor.setId(Constants.SHOWTIME_ANCHOR_ID);
        moviesAnchor.setId(Constants.MOVIES_ANCHOR_ID);
        audiencesAnchor.setId(Constants.AUDIENCES_ANCHOR_ID);
    }
}