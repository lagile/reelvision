package com.projectmas.gwt.reelvision.client.ui.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.projectmas.gwt.reelvision.client.event.EventBus;
import com.projectmas.gwt.reelvision.client.event.LoginEvent;
import com.projectmas.gwt.reelvision.client.service.*;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.UserDTO;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Input;

import java.util.Date;

/**
 * Created by lukasz on 2016-05-14.
 */
public class LoginForm extends Composite {

    @UiField
    Input loginInput;
    @UiField
    Input passwordInput;
    @UiField
    Button submitButton;

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.login.template.LoginForm.ui.xml")
    interface MyUiBinder extends UiBinder<Widget, LoginForm> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public LoginForm() {
        initWidget(uiBinder.createAndBindUi(this));

        submitButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                UserService.App.getInstance().loginUser(loginInput.getValue(), passwordInput.getValue(), new AsyncCallback<UserDTO>() {
                    @Override
                    public void onSuccess(UserDTO result) {
                        if (result != null) {
                            String sessionID = result.getSessionId();
                            Date expires = new Date(System.currentTimeMillis() + Constants.COOKIE_EXPIRY);
                            Cookies.setCookie("sid", sessionID, expires, null, "/", false);
                            EventBus.get().fireEvent(new LoginEvent(result));
                        } else {
                            Window.alert(Constants.WRONG_PASSWORD);
                        }
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        Window.alert(Constants.WRONG_PASSWORD);
                    }
                });
            }
        });
    }
}
