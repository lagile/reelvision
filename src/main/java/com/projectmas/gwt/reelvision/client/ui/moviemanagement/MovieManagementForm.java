package com.projectmas.gwt.reelvision.client.ui.moviemanagement;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;
import com.projectmas.gwt.reelvision.client.service.MovieService;
import com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid;
import com.projectmas.gwt.reelvision.client.util.FormHelper;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.MovieDTO;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.TextArea;

import java.util.ArrayList;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-29.
 */

public class MovieManagementForm extends Composite {

    @UiField
    Form newMovieForm;
    @UiField
    FormGroup movieData;
    @UiField
    Input titleInput;
    @UiField
    Input directorInput;
    @UiField
    Input releaseDateInput;
    @UiField
    Input countryInput;
    @UiField
    TextArea descriptionTextArea;
    @UiField
    Button addNewMovieButton;
    @UiField
    Button updateMovieButton;
    @UiField
    Button deleteMovieButton;
    private CustomDataGrid customDataGrid;
    private SingleSelectionModel<MovieDTO> dataGridSelectionModel;

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.moviemanagement.template.MovieManagementForm.ui.xml")
    interface MyUiBinder extends UiBinder<Widget, MovieManagementForm> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public MovieManagementForm() {
        initWidget(uiBinder.createAndBindUi(this));
        initDataGrid();
//        loadDataGrid();
        registerHandlers();
    }

    private void registerHandlers() {

        addNewMovieButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                MovieService.App.getInstance().createMovie(titleInput.getText(), directorInput.getText(),
                        countryInput.getText(), DateTimeFormat.getFormat("yyyy-mm-dd").parse(releaseDateInput.getText()),
                        descriptionTextArea.getText(), new AsyncCallback<Void>() {

                            @Override
                            public void onFailure(Throwable throwable) {
                                Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
                            }

                            @Override
                            public void onSuccess(Void aVoid) {
                                FormHelper.clearFormWidget(newMovieForm);
//                                loadDataGrid();
                                Window.alert(Constants.MOVIE_ADDED);
                            }
                        });
            }
        });

        updateMovieButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if (dataGridSelectionModel.getSelectedObject() == null) {
                    Window.alert(Constants.PLEASE_SELECT_MOVIE_TO_EDIT);
                    return;
                }
                MovieDTO edited = dataGridSelectionModel.getSelectedObject();
                edited.setTitle(titleInput.getText());
                edited.setDirector(directorInput.getText());
                edited.setCountry(countryInput.getText());
                edited.setReleaseDate(DateTimeFormat.getShortDateFormat().parse(releaseDateInput.getText()));
                edited.setDescription(descriptionTextArea.getText());
                MovieService.App.getInstance().updateMovie(edited, new AsyncCallback<Void>() {

                    @Override
                    public void onFailure(Throwable throwable) {
                        Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
                    }

                    @Override
                    public void onSuccess(Void aVoid) {
                        FormHelper.clearFormWidget(newMovieForm);
//                        loadDataGrid();
                        Window.alert(Constants.MOVIE_EDITED);
                    }
                });
            }
        });

        deleteMovieButton.addClickHandler(new ClickHandler() {
                                              @Override
                                              public void onClick(ClickEvent clickEvent) {
                                                  if (dataGridSelectionModel.getSelectedObject() == null) {
                                                      Window.alert(Constants.PLEASE_SELECT_MOVIE_TO_DELETE);
                                                      return;
                                                  }

                                                  MovieService.App.getInstance().deleteMovie(dataGridSelectionModel.getSelectedObject().getId(), new AsyncCallback<Void>() {
                                                      @Override
                                                      public void onFailure(Throwable throwable) {
                                                          Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
                                                      }

                                                      @Override
                                                      public void onSuccess(Void aVoid) {
//                                                          loadDataGrid();
                                                          Window.alert(Constants.MOVIE_DELETED);
                                                      }
                                                  });
                                              }
                                          }
        );
    }

    private void initDataGrid() {
//        customDataGrid = new CustomDataGrid(220, 50);
//        Map<String, TextColumn<MovieDTO>> columns = new HashMap<>();
//        TextColumn<MovieDTO> title = new TextColumn<MovieDTO>() {
//            @Override
//            public String getValue(MovieDTO movie) {
//                return movie.getTitle() == null ? Constants.DASH : movie.getTitle();
//            }
//        };
//        columns.put(Constants.TITLE, title);
//        TextColumn<MovieDTO> director = new TextColumn<MovieDTO>() {
//            @Override
//            public String getValue(MovieDTO movie) {
//                return movie.getDirector() == null ? Constants.DASH : movie.getDirector();
//            }
//        };
//        columns.put(Constants.DIRECTOR, director);
//        TextColumn<MovieDTO> releaseDate = new TextColumn<MovieDTO>() {
//            @Override
//            public String getValue(MovieDTO movie) {
//                return movie.getReleaseDate() == null ? Constants.DASH : movie.getReleaseDate().toString();
//            }
//        };
//        columns.put(Constants.RELEASE_DATE, releaseDate);
//        TextColumn<MovieDTO> country = new TextColumn<MovieDTO>() {
//            @Override
//            public String getValue(MovieDTO movie) {
//                return movie.getCountry() == null ? Constants.DASH : movie.getCountry();
//            }
//        };
//        columns.put(Constants.COUNTRY, country);
//        TextColumn<MovieDTO> description = new TextColumn<MovieDTO>() {
//            @Override
//            public String getValue(MovieDTO movie) {
//                return movie.getDescription() == null ? Constants.DASH : movie.getDescription();
//            }
//        };
//        columns.put(Constants.DESCRIPTION, description);
//
//        dataGridSelectionModel = new SingleSelectionModel<>();
//        dataGridSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
//            @Override
//            public void onSelectionChange(SelectionChangeEvent selectionChangeEvent) {
//                loadForm();
//            }
//        });
//        customDataGrid.init(columns, dataGridSelectionModel);
//        movieData.add(customDataGrid);
    }

    private void loadForm() {
        MovieDTO selected = dataGridSelectionModel.getSelectedObject();
        titleInput.setText(selected.getTitle());
        directorInput.setText(selected.getDirector());
        countryInput.setText(selected.getCountry());
        releaseDateInput.setText(DateTimeFormat.getShortDateFormat().format(selected.getReleaseDate()));
        descriptionTextArea.setText(selected.getDescription());
    }

//    private void loadDataGrid() {
//        MovieService.App.getInstance().getMovies(new AsyncCallback<ArrayList<MovieDTO>>() {
//            @Override
//            public void onFailure(Throwable throwable) {
//                Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
//            }
//
//            @Override
//            public void onSuccess(ArrayList<MovieDTO> movieDTOs) {
//                customDataGrid.updateData(movieDTOs);
//            }
//        });
//    }
}


