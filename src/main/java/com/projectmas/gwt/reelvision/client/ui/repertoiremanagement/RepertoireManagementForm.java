package com.projectmas.gwt.reelvision.client.ui.repertoiremanagement;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;
import com.projectmas.gwt.reelvision.client.service.MovieService;
import com.projectmas.gwt.reelvision.client.service.RepertoireService;
import com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.MovieDTO;
import com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO;
import org.gwtbootstrap3.client.ui.*;

import java.util.ArrayList;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-29.
 */
public class RepertoireManagementForm extends Composite {

    @UiField
    Form newRepertoireForm;
    @UiField
    FormGroup repertoireData;
    //    @UiField
//    FormGroup movieInRepertoireData;
    @UiField
    FormGroup movieData;
    @UiField
    Input titleInput;
    @UiField
    Input startDateInput;
    @UiField
    Input endDateInput;
    @UiField
    Button addNewRepertoireButton;
    @UiField
    Button updateRepertoireButton;
    @UiField
    Button deleteRepertoireButton;

    private CustomDataGrid repertoireDataGrid;
    private CustomDataGrid movieDataGrid;
    private CustomDataGrid movieInRepertDataGrid;
    private SingleSelectionModel<MovieDTO> movieDataGridSelection;
    private SingleSelectionModel<MovieDTO> repertoireDataGridSelection;

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.repertoiremanagement.template.RepertoireManagementForm.ui.xml")
    interface MyUiBinder extends UiBinder<Widget, RepertoireManagementForm> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public RepertoireManagementForm() {
        initWidget(uiBinder.createAndBindUi(this));
//        initRepertoireDataGrid();
//        initMovieDataGrid();
        loadRepertoireData();
        loadMovieData();
    }

    private void registerHandlers() {

//        addNewMovieButton.addClickHandler(new ClickHandler() {
//            @Override
//            public void onClick(ClickEvent clickEvent) {
//                MovieService.App.getInstance().createMovie(titleInput.getText(), directorInput.getText(),
//                        countryInput.getText(), DateTimeFormat.getFormat("yyyy-mm-dd").parse(releaseDateInput.getText()),
//                        descriptionTextArea.getText(), new AsyncCallback<Void>() {
//
//                            @Override
//                            public void onFailure(Throwable throwable) {
//                                Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
//                            }
//
//                            @Override
//                            public void onSuccess(Void aVoid) {
//                                FormHelper.clearFormWidget(newMovieForm);
//                                loadDataGrid();
//                                Window.alert(Constants.MOVIE_ADDED);
//                            }
//                        });
//            }
//        });
//
//        updateMovieButton.addClickHandler(new ClickHandler() {
//            @Override
//            public void onClick(ClickEvent clickEvent) {
//                if (dataGridSelectionModel.getSelectedObject() == null) {
//                    Window.alert(Constants.PLEASE_SELECT_MOVIE_TO_EDIT);
//                    return;
//                }
//                MovieDTO edited = dataGridSelectionModel.getSelectedObject();
//                edited.setTitle(titleInput.getText());
//                edited.setDirector(directorInput.getText());
//                edited.setCountry(countryInput.getText());
//                edited.setReleaseDate(DateTimeFormat.getShortDateFormat().parse(releaseDateInput.getText()));
//                edited.setDescription(descriptionTextArea.getText());
//                MovieService.App.getInstance().updateMovie(edited, new AsyncCallback<Void>() {
//
//                    @Override
//                    public void onFailure(Throwable throwable) {
//                        Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
//                    }
//
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        FormHelper.clearFormWidget(newMovieForm);
//                        loadDataGrid();
//                        Window.alert(Constants.MOVIE_EDITED);
//                    }
//                });
//            }
//        });
//
//        deleteMovieButton.addClickHandler(new ClickHandler() {
//                                              @Override
//                                              public void onClick(ClickEvent clickEvent) {
//                                                  if (dataGridSelectionModel.getSelectedObject() == null) {
//                                                      Window.alert(Constants.PLEASE_SELECT_MOVIE_TO_DELETE);
//                                                      return;
//                                                  }
//
//                                                  MovieService.App.getInstance().deleteMovie(dataGridSelectionModel.getSelectedObject().getId(), new AsyncCallback<Void>() {
//                                                      @Override
//                                                      public void onFailure(Throwable throwable) {
//                                                          Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
//                                                      }
//
//                                                      @Override
//                                                      public void onSuccess(Void aVoid) {
//                                                          loadDataGrid();
//                                                          Window.alert(Constants.MOVIE_DELETED);
//                                                      }
//                                                  });
//                                              }
//                                          }
//        );
    }

//    private void initMovieDataGrid() {
//        movieDataGrid = new CustomDataGrid(220, 50);
//        Map<String, TextColumn<MovieDTO>> columns = new HashMap<>();
//        TextColumn<MovieDTO> title = new TextColumn<MovieDTO>() {
//            @Override
//            public String getValue(MovieDTO movie) {
//                return movie.getTitle() == null ? Constants.DASH : movie.getTitle();
//            }
//        };
//        columns.put(Constants.TITLE, title);
//        TextColumn<MovieDTO> director = new TextColumn<MovieDTO>() {
//            @Override
//            public String getValue(MovieDTO movie) {
//                return movie.getDirector() == null ? Constants.DASH : movie.getDirector();
//            }
//        };
//        columns.put(Constants.DIRECTOR, director);
//        TextColumn<MovieDTO> releaseDate = new TextColumn<MovieDTO>() {
//            @Override
//            public String getValue(MovieDTO movie) {
//                return movie.getReleaseDate() == null ? Constants.DASH : movie.getReleaseDate().toString();
//            }
//        };
//        columns.put(Constants.RELEASE_DATE, releaseDate);
//        TextColumn<MovieDTO> country = new TextColumn<MovieDTO>() {
//            @Override
//            public String getValue(MovieDTO movie) {
//                return movie.getCountry() == null ? Constants.DASH : movie.getCountry();
//            }
//        };
//        columns.put(Constants.COUNTRY, country);
//        TextColumn<MovieDTO> description = new TextColumn<MovieDTO>() {
//            @Override
//            public String getValue(MovieDTO movie) {
//                return movie.getDescription() == null ? Constants.DASH : movie.getDescription();
//            }
//        };
//        columns.put(Constants.DESCRIPTION, description);
//
//        movieDataGridSelection = new SingleSelectionModel<>();
////        dataGridSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
////            @Override
////            public void onSelectionChange(SelectionChangeEvent selectionChangeEvent) {
////                loadForm();
////            }
////        });
//        movieDataGrid.init(columns, movieDataGridSelection);
//        movieData.add(movieDataGrid);
//    }
//
//    private void initRepertoireDataGrid() {
//        repertoireDataGrid = new CustomDataGrid(100, 50);
//        Map<String, TextColumn<RepertoireDTO>> columns = new HashMap<>();
//        TextColumn<RepertoireDTO> title = new TextColumn<RepertoireDTO>() {
//            @Override
//            public String getValue(RepertoireDTO repertoire) {
//                return repertoire.getTitle() == null ? Constants.DASH : repertoire.getTitle();
//            }
//        };
//        columns.put(Constants.TITLE, title);
//        TextColumn<RepertoireDTO> startDate = new TextColumn<RepertoireDTO>() {
//            @Override
//            public String getValue(RepertoireDTO repertoire) {
//                return repertoire.getStartDate() == null ? Constants.DASH : repertoire.getStartDate().toString();
//            }
//        };
//        columns.put(Constants.START_DATE, startDate);
//        TextColumn<RepertoireDTO> endDate = new TextColumn<RepertoireDTO>() {
//            @Override
//            public String getValue(RepertoireDTO repertoire) {
//                return repertoire.getEndDate() == null ? Constants.DASH : repertoire.getEndDate().toString();
//            }
//        };
//        columns.put(Constants.END_DATE, endDate);
//
//        repertoireDataGridSelection = new SingleSelectionModel<>();
////        dataGridSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
////            @Override
////            public void onSelectionChange(SelectionChangeEvent selectionChangeEvent) {
////                loadForm();
////            }
////        });
//        repertoireDataGrid.init(columns, repertoireDataGridSelection);
//        repertoireData.add(repertoireDataGrid);
//    }

//    private void loadForm() {
//        MovieDTO selected = dataGridSelectionModel.getSelectedObject();
//        titleInput.setText(selected.getTitle());
//        directorInput.setText(selected.getDirector());
//        countryInput.setText(selected.getCountry());
//        releaseDateInput.setText(DateTimeFormat.getShortDateFormat().format(selected.getReleaseDate()));
//        descriptionTextArea.setText(selected.getDescription());
//    }

    private void loadMovieData() {
        MovieService.App.getInstance().getMovies(new AsyncCallback<ArrayList<MovieDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
                Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
            }

            @Override
            public void onSuccess(ArrayList<MovieDTO> movieDTOs) {
//                movieDataGrid.updateData(movieDTOs);
            }
        });
    }

    private void loadRepertoireData() {
        RepertoireService.App.getInstance().getRepertoires(new AsyncCallback<ArrayList<RepertoireDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
                Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
            }

            @Override
            public void onSuccess(ArrayList<RepertoireDTO> repertoireDTOs) {
//                repertoireDataGrid.updateData(repertoireDTOs);
            }
        });
    }
}