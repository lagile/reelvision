package com.projectmas.gwt.reelvision.client.ui.sale;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;
import com.projectmas.gwt.reelvision.client.service.RepertoireService;
import com.projectmas.gwt.reelvision.client.service.ShowcaseService;
import com.projectmas.gwt.reelvision.client.service.TicketService;
import com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid;
import com.projectmas.gwt.reelvision.client.ui.component.CustomListBox;
import com.projectmas.gwt.reelvision.client.ui.component.MessageBox;
import com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.*;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Panel;
import org.gwtbootstrap3.client.ui.constants.ListGroupItemType;
import org.gwtbootstrap3.client.ui.gwt.ButtonCell;
import java.util.*;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-29.
 */
public class SaleForm extends Composite {

    @UiField
    CustomListBox repertoireListBox;
    @UiField
    CustomListBox movieListBox;
    @UiField
    CustomListBox screeningListBox;
    @UiField
    Button confirmSeatsButton;
    @UiField
    Button confirmButton;
    @UiField
    Panel seatingPlanPanel;
    @UiField
    Panel ticketsPanel;


    private CustomDataGrid ticketDataGrid;
    private SeatingPlan seatingPlan;
    private SingleSelectionModel<TicketDTO> ticketSingleSelModel;

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.sale.template.SaleForm.ui.xml")
    interface MyUiBinder extends UiBinder<Widget, SaleForm> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public SaleForm() {
        initWidget(uiBinder.createAndBindUi(this));
        updateRepertoireListBox();
        initSeatingGrid();
        initTicketDataGrid();
        registerHandlers();
    }

    private void registerHandlers() {
        repertoireListBox.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                updateMovieListBox();
            }
        });
        movieListBox.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                updateScreeningListBox();
            }
        });
        screeningListBox.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                updateSeatingGrid();
            }
        });
        confirmSeatsButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                List<SeatDTO> seatDTOs = seatingPlan.getChosenSeatsList();
                Collections.sort(seatDTOs, new Comparator<SeatDTO>() {
                    @Override
                    public int compare(SeatDTO o1, SeatDTO o2) {
                        Character row1 = o1.getRow();
                        Character row2 = o2.getRow();
                        int rowRes = row1.compareTo(row2);

                        if (rowRes != 0) {
                            return rowRes;
                        } else {
                            Integer no1 = o1.getNo();
                            Integer no2 = o2.getNo();
                            return no1.compareTo(no2);
                        }
                    }
                });
                ListIterator<TicketDTO> iter = ticketDataGrid.getRows().listIterator();
                while (iter.hasNext()) {
                    if (iter.next().getScreening().equals(screeningListBox.getSelectedItem())) {
                        iter.remove();
                    }
                }

                for (SeatDTO seatDTO : seatDTOs) {
                    TicketDTO ticketDTO = new TicketDTO();
                    ticketDTO.setTicketType(TicketDTO.TicketType.NORMAL);
                    ticketDTO.setScreening((ScreeningDTO) screeningListBox.getSelectedItem());
                    ticketDTO.setSeat(seatDTO);
                    ticketDataGrid.addRow(ticketDTO);
                }
                ticketDataGrid.update();
            }
        });

        confirmButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if (ticketDataGrid.getRows().size() != 0) {
                    TicketService.App.getInstance().createTickets(ticketDataGrid.getRows(), new AsyncCallback<List<TicketDTO>>() {
                        @Override
                        public void onFailure(Throwable throwable) {
                            Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
                        }

                        @Override
                        public void onSuccess(List<TicketDTO> ticketDTOs) {
                            Double charge = 0D;
                            ListGroup listGroup = new ListGroup();
                            ListGroupItem listGroupItem;
                            seatingPlan.getChosenSeatsList().clear();
                            seatingPlan.updateSeatsStatus();
                            ticketDataGrid.clear();
                            CustomDataGrid dataGrid = new CustomDataGrid();
                            dataGrid.addColumn(new TextCell(), "Movie", new CustomDataGrid.GetValue<String>() {
                                @Override
                                public String getValue(ObjectDTO objectDTO) {
                                    return ((TicketDTO) objectDTO).getScreening().getShowcase().getMovie().getTitle();
                                }
                            });
                            dataGrid.addColumn(new TextCell(), "Type", new CustomDataGrid.GetValue<String>() {
                                @Override
                                public String getValue(ObjectDTO objectDTO) {
                                    return ((TicketDTO) objectDTO).getTicketType().toString();
                                }
                            });
                            dataGrid.addColumn(new TextCell(), "Screening Date", new CustomDataGrid.GetValue<String>() {
                                @Override
                                public String getValue(ObjectDTO objectDTO) {
                                    return DateTimeFormat.getMediumDateTimeFormat().format(((TicketDTO) objectDTO).getScreening().getScreeningDate());
                                }
                            });
                            dataGrid.addColumn(new TextCell(), "Auditorium", new CustomDataGrid.GetValue<String>() {
                                @Override
                                public String getValue(ObjectDTO objectDTO) {
                                    AuditoriumDTO auditoriumDTO = ((TicketDTO) objectDTO).getScreening().getAuditorium();
                                    return auditoriumDTO.getNo() + "-'" + auditoriumDTO.getName() + "'";
                                }
                            });
                            dataGrid.addColumn(new TextCell(), "Seat", new CustomDataGrid.GetValue<String>() {
                                @Override
                                public String getValue(ObjectDTO objectDTO) {
                                    SeatDTO seatDTO = ((TicketDTO) objectDTO).getSeat();
                                    return seatDTO.getRow() + ":" + seatDTO.getNo();
                                }
                            });
                            dataGrid.addColumn(new TextCell(), "Price", new CustomDataGrid.GetValue<String>() {
                                @Override
                                public String getValue(ObjectDTO objectDTO) {
                                    return ((TicketDTO) objectDTO).getPrice() + "PLN";
                                }
                            });
                            for (TicketDTO ticketDTO : ticketDTOs) {
                                charge += ticketDTO.getPrice();
                                dataGrid.addRow(ticketDTO);
                            }
                            dataGrid.update();
                            listGroupItem = new ListGroupItem();
                            listGroupItem.setType(ListGroupItemType.DEFAULT);
                            listGroupItem.add(dataGrid);
                            listGroup.add(listGroupItem);
                            listGroupItem = new ListGroupItem();
                            listGroupItem.setType(ListGroupItemType.SUCCESS);
                            listGroupItem.setText("Charge: " + charge + "PLN");
                            listGroup.add(listGroupItem);
                            new MessageBox("Summary", listGroup).center();
                        }
                    });
                }
            }
        });
    }

    private List<SeatDTO> getSeatListFromTicketGrid() {
        ArrayList<SeatDTO> seatDTOs = new ArrayList<>();
        if (ticketDataGrid.getRows().size() != 0) {
            for (Object object : ticketDataGrid.getRows()) {
                TicketDTO ticketDTO = (TicketDTO) object;
                if (ticketDTO.getScreening().equals(screeningListBox.getSelectedItem())) {
                    seatDTOs.add(ticketDTO.getSeat());
                }
            }
        }
        return seatDTOs;
    }

    private void updateRepertoireListBox() {
        if (repertoireListBox.getSelectedItem() == null) {
            movieListBox.setDefault();
            screeningListBox.setDefault();
        }
        RepertoireService.App.getInstance().getRepertoires(new AsyncCallback<ArrayList<RepertoireDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
                Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
            }

            @Override
            public void onSuccess(ArrayList<RepertoireDTO> repertoireDTOs) {
                Map<String, ObjectDTO> map = new HashMap<>();
                for (RepertoireDTO repertoireDTO : repertoireDTOs) {
                    map.put(repertoireDTO.getTitle(), repertoireDTO);
                }
                repertoireListBox.setListBoxValues(map);
            }
        });
    }

    private void updateMovieListBox() {
        if (repertoireListBox.getSelectedItem() == null) {
            movieListBox.setDefault();
            screeningListBox.setDefault();
            return;
        }
        ShowcaseService.App.getInstance().getShowcases(((RepertoireDTO) repertoireListBox.getSelectedItem()).getId(), new AsyncCallback<ArrayList<ShowcaseDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
                Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
            }

            @Override
            public void onSuccess(ArrayList<ShowcaseDTO> showcaseDTOs) {
                Map<String, ObjectDTO> map = new HashMap<>();
                for (ShowcaseDTO showcaseDTO : showcaseDTOs) {
                    map.put(showcaseDTO.getMovie().getTitle(), showcaseDTO);
                }
                movieListBox.setListBoxValues(map);
            }
        });
    }

    private void updateScreeningListBox() {
        if (movieListBox.getSelectedItem() == null) {
            screeningListBox.setDefault();
            return;
        }
        ShowcaseService.App.getInstance().getScreenings(((ShowcaseDTO) movieListBox.getSelectedItem()).getId(), new AsyncCallback<ArrayList<ScreeningDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
                Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
            }

            @Override
            public void onSuccess(ArrayList<ScreeningDTO> screeningDTOs) {
                Map<String, ObjectDTO> map = new HashMap<>();
                for (ScreeningDTO screeningDTO : screeningDTOs) {
                    map.put(DateTimeFormat.getMediumDateTimeFormat().format(screeningDTO.getScreeningDate()), screeningDTO);
                }
                screeningListBox.setListBoxValues(map);
            }
        });
    }

    private void initSeatingGrid() {
        seatingPlan = new SeatingPlan();
        seatingPlanPanel.add(seatingPlan);
    }

    private void updateSeatingGrid() {
        if (screeningListBox.getSelectedItem() == null) {
            screeningListBox.setDefault();
            return;
        }
        seatingPlan.showSeatingPlan(((ScreeningDTO) screeningListBox.getSelectedItem()).getAuditorium().getId(),
                ((ScreeningDTO) screeningListBox.getSelectedItem()).getId(), getSeatListFromTicketGrid());
    }

    private void initTicketDataGrid() {
        ticketDataGrid = new CustomDataGrid(ticketSingleSelModel);
        ticketDataGrid.setCellWidth(240);
        ticketDataGrid.setCellHeight(70);
        ticketDataGrid.addColumn(new TextCell(), "Movie", new CustomDataGrid.GetValue<String>() {
            @Override
            public String getValue(ObjectDTO objectDTO) {
                return ((TicketDTO) objectDTO).getScreening().getShowcase().getMovie().getTitle();
            }
        });

        ticketDataGrid.addColumn(new SelectionCell(new ArrayList<>(Arrays.asList(new String[]{TicketDTO.TicketType.NORMAL.toString(),
                TicketDTO.TicketType.REDUCED.toString()}))), "Ticket Type", new CustomDataGrid.GetValue<String>() {
            @Override
            public String getValue(ObjectDTO objectDTO) {
                return ((TicketDTO) objectDTO).getTicketType().toString();
            }
        }, new FieldUpdater() {
            @Override
            public void update(int i, Object o, Object o2) {
                if (o instanceof TicketDTO) {
                    TicketDTO ticketDTO = (TicketDTO) o;
                    if (o2 instanceof String) {
                        ticketDTO.setTicketType(TicketDTO.TicketType.valueOf((String) o2));
                    }
                }
            }
        });

        ticketDataGrid.addColumn(new TextCell(), "Screening Date", new CustomDataGrid.GetValue<String>() {
            @Override
            public String getValue(ObjectDTO objectDTO) {
                return DateTimeFormat.getMediumDateTimeFormat().format(((TicketDTO) objectDTO).getScreening().getScreeningDate());
            }
        });
        ticketDataGrid.addColumn(new TextCell(), "Auditorium", new CustomDataGrid.GetValue<String>() {
            @Override
            public String getValue(ObjectDTO objectDTO) {
                AuditoriumDTO auditoriumDTO = ((TicketDTO) objectDTO).getScreening().getAuditorium();
                return auditoriumDTO.getNo() + "-'" + auditoriumDTO.getName() + "'";
            }
        });
        ticketDataGrid.addColumn(new TextCell(), "Seat", new CustomDataGrid.GetValue<String>() {
            @Override
            public String getValue(ObjectDTO objectDTO) {
                SeatDTO seatDTO = ((TicketDTO) objectDTO).getSeat();
                return seatDTO.getRow() + ":" + seatDTO.getNo();
            }
        });
        ticketDataGrid.addColumn(new ButtonCell(), "", new CustomDataGrid.GetValue<String>() {
                    @Override
                    public String getValue(ObjectDTO objectDTO) {
                        return "Cancel";
                    }
                }, new FieldUpdater<ObjectDTO, String>() {
                    @Override
                    public void update(int index, ObjectDTO object, String value) {
                        ticketDataGrid.removeRow(object);
                        ticketDataGrid.update();
                        updateSeatingGrid();
                    }
                }
        );
        ticketsPanel.add(ticketDataGrid);
    }
}