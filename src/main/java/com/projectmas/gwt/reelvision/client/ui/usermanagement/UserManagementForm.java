package com.projectmas.gwt.reelvision.client.ui.usermanagement;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;
import com.projectmas.gwt.reelvision.client.service.UserService;
import com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid;
import com.projectmas.gwt.reelvision.client.util.FormHelper;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO;
import com.projectmas.gwt.reelvision.shared.dto.UserDTO;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.ListBox;

import java.util.*;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
public class UserManagementForm extends Composite {

    @UiField
    Form newUserForm;
    @UiField
    FormGroup userData;
    @UiField
    Input loginInput;
    @UiField
    Input passwordInput;
    @UiField
    Input confirmPasswordInput;
    @UiField
    Button addNewUserButton;
    @UiField
    Button deleteUserButton;
    @UiField
    ListBox privClassDropDown;
    private CustomDataGrid userDataGrid;
    private SingleSelectionModel<UserDTO> ticketSingleSelModel;

    @UiTemplate("com.projectmas.gwt.reelvision.client.ui.usermanagement.template.UserManagementForm.ui.xml")
    interface MyUiBinder extends UiBinder<Widget, UserManagementForm> {
    }

    private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

    public UserManagementForm() {
        initWidget(uiBinder.createAndBindUi(this));
        initDataGrid();
        loadPrivClasses();
        loadUsers();
        registerHandlers();
    }

    private void registerHandlers() {

        addNewUserButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                if (!passwordInput.getText().equals(confirmPasswordInput.getText())) {
                    Window.alert(Constants.PROVIDED_PASSWORDS_ARE_DIFFERENT);
                    return;
                }
                UserService.App.getInstance().createUser(loginInput.getText(), passwordInput.getText(), privClassDropDown.getSelectedItemText(), new AsyncCallback<Void>() {

                    @Override
                    public void onFailure(Throwable throwable) {
                        Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
                    }

                    @Override
                    public void onSuccess(Void aVoid) {
                        FormHelper.clearFormWidget(newUserForm);
                        loadUsers();
                        Window.alert(Constants.USER_ADDED);
                    }
                });
            }
        });

        deleteUserButton.addClickHandler(new ClickHandler() {
                                             @Override
                                             public void onClick(ClickEvent clickEvent) {

                                                 UserService.App.getInstance().getCurrentUser(new AsyncCallback<UserDTO>() {
                                                     @Override
                                                     public void onFailure(Throwable throwable) {
                                                         Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
                                                     }

                                                     @Override
                                                     public void onSuccess(UserDTO user) {
                                                         UserDTO selected = ticketSingleSelModel.getSelectedObject();
                                                         if (Objects.equals(selected.getId(), user.getId())) {
                                                             Window.alert(Constants.CANNOT_DELETE_CURRENT_LOGGED_IN_USER);
                                                             return;
                                                         }
                                                         UserService.App.getInstance().removeUser(selected.getId(), new AsyncCallback<Void>() {
                                                             @Override
                                                             public void onFailure(Throwable throwable) {
                                                                 Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
                                                             }

                                                             @Override
                                                             public void onSuccess(Void aVoid) {
                                                                 loadUsers();
                                                                 Window.alert(Constants.USER_DELETED);
                                                             }
                                                         });
                                                     }
                                                 });

                                             }
                                         }
        );
    }

    private void initDataGrid() {
//        userDataGrid = new CustomDataGrid(220, 50);
        Map<String, TextColumn<UserDTO>> columns = new HashMap<>();
        TextColumn<UserDTO> login = new TextColumn<UserDTO>() {
            @Override
            public String getValue(UserDTO user) {
                return user.getLogin() == null ? Constants.DASH : user.getLogin();
            }
        };
        columns.put(Constants.LOGIN, login);
        TextColumn<UserDTO> createdAt = new TextColumn<UserDTO>() {
            @Override
            public String getValue(UserDTO user) {
                return user.getCreatedAt().toString();
            }
        };
        columns.put(Constants.CREATED, createdAt);
        TextColumn<UserDTO> updatedAt = new TextColumn<UserDTO>() {
            @Override
            public String getValue(UserDTO user) {
                return user.getUpdatedAt() == null ? Constants.DASH : user.getUpdatedAt().toString();
            }
        };
        columns.put(Constants.UPDATED, updatedAt);
        TextColumn<UserDTO> lastLogin = new TextColumn<UserDTO>() {
            @Override
            public String getValue(UserDTO user) {
                return user.getLastLogin() == null ? Constants.DASH : user.getLastLogin().toString();
            }
        };
        columns.put(Constants.LAST_LOGIN, lastLogin);
        TextColumn<UserDTO> privClass = new TextColumn<UserDTO>() {
            @Override
            public String getValue(UserDTO user) {
                return user.getPrivClassDTO() == null ? Constants.DASH : user.getPrivClassDTO().getClassName();
            }
        };
        columns.put(Constants.PRIVILAGE, privClass);
        ticketSingleSelModel = new SingleSelectionModel<>();
//        userDataGrid.init(columns, ticketSingleSelModel);
        userData.add(userDataGrid);
    }

    private void loadUsers() {
        UserService.App.getInstance().getUsers(new AsyncCallback<ArrayList<UserDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
                Window.alert(Constants.UNEXPECTED_ERROR + throwable.getMessage());
            }

            @Override
            public void onSuccess(ArrayList<UserDTO> userDTOs) {
//                userDataGrid.updateData(userDTOs);
            }
        });
    }

    private void loadPrivClasses() {
        UserService.App.getInstance().getPrivClasses(new AsyncCallback<ArrayList<PrivClassDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
                Window.alert(throwable.getMessage());
            }

            @Override
            public void onSuccess(ArrayList<PrivClassDTO> privClassDTOs) {
                for (PrivClassDTO privClass : privClassDTOs) {
                    privClassDropDown.addItem(privClass.getClassName());
                }
            }
        });
    }
}