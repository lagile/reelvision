package com.projectmas.gwt.reelvision.client.util;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.projectmas.gwt.reelvision.shared.Constants;
import org.gwtbootstrap3.client.ui.Input;
import org.gwtbootstrap3.client.ui.ListBox;
import org.gwtbootstrap3.client.ui.TextArea;

import java.util.Iterator;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
public class FormHelper {

    public static void clearFormWidget(Widget widget) {
        if (widget == null) {
            return;
        }
        if (widget instanceof HasWidgets) {
            Iterator<Widget> iter = ((HasWidgets) widget).iterator();
            while (iter.hasNext()) {
                Widget child = iter.next();
                if (child instanceof Input) {
                    ((Input) child).setText(Constants.EMPTY_STRING);
                } else if (child instanceof TextArea) {
                    ((TextArea) child).setText(Constants.EMPTY_STRING);
                } else {
                    clearFormWidget(child);
                }
            }
        }
    }

}
