package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.*;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-02.
 */

@Entity
@Table(name = "AUDITORIUM")
public class Auditorium {

    private Long id;
    private Integer no;
    private String name;

    public Auditorium() {
    }

    public Auditorium(Long id, Integer no, String name) {
        this.id = id;
        this.no = no;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AUDIT_NO")
    public Integer getNo() {
        return no;
    }

    public void setNo(Integer number) {
        this.no = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
