package com.projectmas.gwt.reelvision.server.model;

import com.projectmas.gwt.reelvision.shared.dto.MovieDTO;

import javax.persistence.*;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
@Entity
@Table(name = "MOVIE")
public class Movie {

    private Long id;
    private String title;
    private String director;
    private String country;
    private Date releaseDate;
    private String description;

    public Movie() {
    }

    public Movie(Long id, String title, String director, String country, Date releaseDate, String description) {
        this.id = id;
        this.title = title;
        this.director = director;
        this.country = country;
        this.releaseDate = releaseDate;
        this.description = description;
    }

    public Movie(MovieDTO movie) {
        this.id = movie.getId();
        this.title = movie.getTitle();
        this.director = movie.getDirector();
        this.country = movie.getCountry();
        this.releaseDate = movie.getReleaseDate();
        this.description = movie.getDescription();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TITLE", unique = true)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "RELEASE_DATE")
    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
