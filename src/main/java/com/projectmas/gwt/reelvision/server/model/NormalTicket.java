package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-12.
 */

@Entity(name = "NORMAL_TICKET")
public class NormalTicket extends Ticket{

    private Integer discountRate;
    public final static Double TICKET_PRICE = 22D;

    public NormalTicket() {
    }

    public NormalTicket(Screening screening, Date saleDate, Integer discountRate) {
        super(screening, saleDate, TICKET_PRICE - ( TICKET_PRICE * (discountRate * 0.01)));
        this.discountRate = discountRate;
    }

    @Column(name = "DISCOUNT_RATE")
    public Integer getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Integer discountRate) {
        this.discountRate = discountRate;
    }
}
