package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.*;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-02.
 */
@Entity
@Table(name = "OCCUP_SEAT")
public class OccupiedSeat {

    private Long id;
    private Seat seat;
    private Screening screening;
    private Reservation reservation;
    private Ticket ticket;

    public OccupiedSeat() {
    }

    public OccupiedSeat(Long id, Seat seat, Screening screening, Reservation reservation, Ticket ticket) {
        this.id = id;
        this.seat = seat;
        this.screening = screening;
        this.reservation = reservation;
        this.ticket = ticket;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OCCUP_SEAT2SEAT")
    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OCCUP_SEAT2RESERVATION")
    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OCCUP_SEAT2TICKET")
    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OCCUP_SEAT2SCREENING")
    public Screening getScreening() {
        return screening;
    }

    public void setScreening(Screening screening) {
        this.screening = screening;
    }
}
