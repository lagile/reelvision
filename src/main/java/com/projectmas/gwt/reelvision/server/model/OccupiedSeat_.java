package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-11.
 */

@StaticMetamodel(OccupiedSeat.class)
public class OccupiedSeat_ {
    public static volatile SingularAttribute<OccupiedSeat, Long> id;
    public static volatile SingularAttribute<OccupiedSeat, Seat> seat;
    public static volatile SingularAttribute<OccupiedSeat, Screening> screening;
    public static volatile SingularAttribute<OccupiedSeat, Reservation> reservation;
    public static volatile SingularAttribute<OccupiedSeat, Ticket> ticket;
}