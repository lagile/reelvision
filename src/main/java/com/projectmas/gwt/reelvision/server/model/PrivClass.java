package com.projectmas.gwt.reelvision.server.model;

import com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO;
import javax.persistence.*;


/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-24.
 */
@Entity
@Table(name = "PRIV_CLASS")
public class PrivClass {

    private Long id;
    private String className;
    private String description;

    public PrivClass() {
    }

    public PrivClass(PrivClassDTO privClass) {
        this.id = privClass.getId();
        this.className = privClass.getClassName();
        this.description = privClass.getDescription();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(unique = true, name = "CLASS_NAME", nullable = false)
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


