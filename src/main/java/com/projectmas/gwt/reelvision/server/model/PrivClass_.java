package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Set;


@StaticMetamodel(PrivClass.class)
public class PrivClass_ {
    public static volatile SingularAttribute<PrivClass, Long> id;
    public static volatile SingularAttribute<PrivClass, String> className;
    public static volatile SingularAttribute<PrivClass, String> description;
    public static volatile SetAttribute<PrivClass, Set<User>> users;
}
