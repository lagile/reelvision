package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-12.
 */

@Entity(name = "REDUCED_TICKET")
public class ReducedTicket extends Ticket {

    private Integer reductionRate = 50;

    public ReducedTicket() {
    }

    public ReducedTicket(Screening screening, Date saleDate, Integer reductionRate) {
        super(screening, saleDate, NormalTicket.TICKET_PRICE * (reductionRate * 0.01));
    }

    @Column(name = "REDUCTION_RATE")
    public Integer getReductionRate() {
        return reductionRate;
    }

    public void setReductionRate(Integer reductionRate) {
        this.reductionRate = reductionRate;
    }
}
