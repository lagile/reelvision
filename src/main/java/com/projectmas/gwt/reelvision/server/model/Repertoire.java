package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.*;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
@Entity
@Table(name = "REPERTOIRE")
public class Repertoire {

    private Long id;
    private String title;
    private Date startDate;
    private Date endDate;

    public Repertoire() {
    }

    public Repertoire(Long id, String title, Date startDate, Date endDate) {
        this.id = id;
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "START_DATE")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Column(name = "END_DATE")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
