package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@StaticMetamodel(Repertoire.class)
public class Repertoire_ {
    public static volatile SingularAttribute<Repertoire, Long> id;
    public static volatile SingularAttribute<Repertoire, String> title;
    public static volatile SingularAttribute<Repertoire, Date> startDate;
    public static volatile SingularAttribute<Repertoire, Date> endDate;
}