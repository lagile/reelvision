package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.*;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-02.
 */
@Entity
@Table(name = "RESERVATION")
public class Reservation {

    private Long id;
    private boolean paid;
    private boolean active;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
