package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.*;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-02.
 */
@Entity
@Table(name = "SCREENING")
public class Screening {

    private Long id;
    private Showcase showcase;
    private Auditorium auditorium;
    private Date screeningDate;

    public Screening() {
    }

    public Screening(Long id, Showcase showcase, Auditorium auditorium, Date screeningDate) {
        this.id = id;
        this.showcase = showcase;
        this.auditorium = auditorium;
        this.screeningDate = screeningDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SCREENING2SHOWCASE")
    public Showcase getShowcase() {
        return showcase;
    }

    public void setShowcase(Showcase showcase) {
        this.showcase = showcase;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SCREENING2AUDITORIUM")
    public Auditorium getAuditorium() {
        return auditorium;
    }

    public void setAuditorium(Auditorium auditorium) {
        this.auditorium = auditorium;
    }

    @Column(name = "SCREEN_DATE")
    public Date getScreeningDate() {
        return screeningDate;
    }

    public void setScreeningDate(Date screeningDate) {
        this.screeningDate = screeningDate;
    }
}
