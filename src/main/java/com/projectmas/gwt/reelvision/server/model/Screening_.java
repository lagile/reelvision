package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-08.
 */
@StaticMetamodel(Screening.class)
public class Screening_ {
    public static volatile SingularAttribute<Screening, Long> id;
    public static volatile SingularAttribute<Screening, Showcase> showcase;
    public static volatile SingularAttribute<Screening, Auditorium> auditorium;
    public static volatile SingularAttribute<Screening, Date> screeningDate;
}
