package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.*;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-02.
 */
@Entity
@Table(name = "SEAT")
public class Seat {

    private Long id;
    private Character row;
    private Integer no;
    private Auditorium auditorium;

    public Seat() {
    }

    public Seat(Long id, Character row, Integer no, Auditorium auditorium) {
        this.id = id;
        this.row = row;
        this.no = no;
        this.auditorium = auditorium;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "SEAT_ROW")
    public Character getRow() {
        return row;
    }

    public void setRow(Character row) {
        this.row = row;
    }

    @Column(name = "SEAT_NO")
    public Integer getNo() {
        return no;
    }

    public void setNo(Integer number) {
        this.no = number;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SEAT2AUDITORIUM")
    public Auditorium getAuditorium() {
        return auditorium;
    }

    public void setAuditorium(Auditorium auditorium) {
        this.auditorium = auditorium;
    }
}
