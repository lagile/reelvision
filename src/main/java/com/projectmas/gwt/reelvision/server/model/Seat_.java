package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-10.
 */

@StaticMetamodel(Repertoire.class)
public class Seat_ {
    public static volatile SingularAttribute<Seat, Long> id;
    public static volatile SingularAttribute<Seat, Character> row;
    public static volatile SingularAttribute<Seat, Integer> no;
    public static volatile SingularAttribute<Seat, Auditorium> auditorium;
}


