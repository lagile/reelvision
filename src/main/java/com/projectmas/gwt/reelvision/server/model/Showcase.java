package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.*;
import java.util.Set;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
@Entity
@Table(name = "SHOWCASE")
public class Showcase {

    private Long id;
    private Movie movie;
    private Repertoire repertoire;
//    private Set<Screening> screenings;


    public Showcase() {
    }

    public Showcase(Long id, Movie movie, Repertoire repertoire) {
        this.id = id;
        this.movie = movie;
        this.repertoire = repertoire;
    }

    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SHOWCASE2MOVIE")
    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SHOWCASE2REPERTOIRE")
    public Repertoire getRepertoire() {
        return repertoire;
    }

    public void setRepertoire(Repertoire repertoire) {
        this.repertoire = repertoire;
    }
//
//    @OneToMany(fetch = FetchType.EAGER)
//    public Set<Screening> getScreenings() {
//        return screenings;
//    }
//
//    public void setScreenings(Set<Screening> screenings) {
//        this.screenings = screenings;
//    }
}
