package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Repertoire.class)
public class Showcase_ {
    public static volatile SingularAttribute<Showcase, Long> id;
    public static volatile SingularAttribute<Showcase, Movie> movie;
    public static volatile SingularAttribute<Showcase, Repertoire> repertoire;
}