package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.*;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-02.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "TICKET")
public abstract class Ticket {

    private Long id;
    private Screening screening;
    private Date saleDate;
    private Double price;

    public Ticket(Screening screening, Date saleDate, Double price) {
        this.screening = screening;
        this.saleDate = saleDate;
        this.price = price;
    }

    public Ticket() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKET2SCREENING")
    public Screening getScreening() {
        return screening;
    }

    public void setScreening(Screening screening) {
        this.screening = screening;
    }

    @Column(name = "SALE_DATE")
    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date sale_date) {
        this.saleDate = sale_date;
    }

    @Column(name = "PRICE")
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
