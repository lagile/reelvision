package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-13.
 */
@StaticMetamodel(Ticket.class)
public class Ticket_ {
    public static volatile SingularAttribute<Ticket, Long> id;
    public static volatile SingularAttribute<Ticket, Screening> screening;
}