package com.projectmas.gwt.reelvision.server.model;

import com.projectmas.gwt.reelvision.shared.dto.UserDTO;
import javax.persistence.*;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-24.
 */
@Entity
@Table(name = "APP_USER")
public class User {

    private Long id;
    private String login;
    private String password;
    private Date lastLogin;
    private Date updatedAt;
    private Date createdAt;
    private PrivClass privClass;

    public User() {
    }

    public User(String login, String password, Date lastLogin, Date updatedAt, Date createdAt, PrivClass privClass) {
        this.login = login;
        this.password = password;
        this.lastLogin = lastLogin;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.privClass = privClass;
    }

    public User(UserDTO user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.lastLogin = user.getLastLogin();
        this.updatedAt = user.getUpdatedAt();
        this.createdAt = user.getCreatedAt();
        this.privClass = new PrivClass(user.getPrivClassDTO());
    }


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(unique = true, nullable = false)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "LAST_LOGIN")
    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Column(name = "UPDATED_AT")
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Column(name = "CREATED_AT")
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER2CLASS")
    public PrivClass getPrivClass() {
        return privClass;
    }

    public void setPrivClass(PrivClass privClass) {
        this.privClass = privClass;
    }
}


