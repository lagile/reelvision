package com.projectmas.gwt.reelvision.server.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@StaticMetamodel(User.class)
public class User_ {
    public static volatile SingularAttribute<User, Long> id;
    public static volatile SingularAttribute<User, String> login;
    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, Date> lastLogin;
    public static volatile SingularAttribute<User, Date> updatedAt;
    public static volatile SingularAttribute<User, Date> createdAt;
    public static volatile SingularAttribute<User, PrivClass> privClass;
}
