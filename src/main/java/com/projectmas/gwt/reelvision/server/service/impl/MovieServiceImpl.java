package com.projectmas.gwt.reelvision.server.service.impl;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.projectmas.gwt.reelvision.client.service.MovieService;
import com.projectmas.gwt.reelvision.server.model.Movie;
import com.projectmas.gwt.reelvision.server.model.Movie_;
import com.projectmas.gwt.reelvision.server.model.User;
import com.projectmas.gwt.reelvision.server.model.User_;
import com.projectmas.gwt.reelvision.server.util.PersistenceManager;
import com.projectmas.gwt.reelvision.shared.dto.MovieDTO;
import com.projectmas.gwt.reelvision.shared.dto.UserDTO;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
public class MovieServiceImpl extends RemoteServiceServlet implements MovieService {

    @Override
    public void createMovie(String title, String director, String country, Date releaseDate, String description) {
        if (getMovie(title) == null) {
            PersistenceManager persistanceManager = new PersistenceManager();
            persistanceManager.persist(new Movie(null, title, director, country, releaseDate, description));
        }
    }

    @Override
    public void deleteMovie(Long id) {
        PersistenceManager persistanceManager = new PersistenceManager();
        persistanceManager.delete(Movie.class, id);
    }

    @Override
    public void updateMovie(MovieDTO movie) {
        PersistenceManager persistanceManager = new PersistenceManager();
        persistanceManager.merge(new Movie(movie));
    }

    @Override
    public ArrayList<MovieDTO> getMovies() {
        ArrayList temp;
        ArrayList<MovieDTO> result = null;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<Movie> criteria = criteriaBuilder.createQuery(Movie.class);
        Root<Movie> root = criteria.from(Movie.class);
        criteria.select(root);
        temp = (ArrayList) persistanceManager.executeStatement(criteria);
        if (temp != null) {
            result = new ArrayList<>();
            for (Object movie : temp) {
                if (movie instanceof Movie) {
                    result.add(createMovieDTO((Movie) movie));
                }
            }
        }

        return result;
    }

    private MovieDTO getMovie(String title) {
        ArrayList<Movie> result;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<Movie> criteria = criteriaBuilder.createQuery(Movie.class);
        Root<Movie> root = criteria.from(Movie.class);
        criteria.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(Movie_.title), title)
                )
        );
        result = (ArrayList<Movie>) persistanceManager.executeStatement(criteria);

        return result.size() == 1 ? createMovieDTO(result.get(0)) : null;
    }

    private MovieDTO getMovie(Long id) {
        ArrayList<Movie> result;
        PersistenceManager persistanceManager = new PersistenceManager();
        result = (ArrayList<Movie>) persistanceManager.find(Movie.class, id);

        return result.size() == 1 ? createMovieDTO(result.get(0)) : null;
    }

    public static MovieDTO createMovieDTO(Movie movie) {
        return new MovieDTO(movie.getId(), movie.getTitle(), movie.getDirector(), movie.getCountry(), movie.getReleaseDate(), movie.getDescription());
    }
}