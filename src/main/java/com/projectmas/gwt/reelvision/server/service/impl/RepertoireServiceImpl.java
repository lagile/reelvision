package com.projectmas.gwt.reelvision.server.service.impl;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.projectmas.gwt.reelvision.client.service.RepertoireService;
import com.projectmas.gwt.reelvision.server.model.*;
import com.projectmas.gwt.reelvision.server.util.Converter;
import com.projectmas.gwt.reelvision.server.util.PersistenceManager;
import com.projectmas.gwt.reelvision.shared.dto.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-28.
 */
public class RepertoireServiceImpl extends RemoteServiceServlet implements RepertoireService {


    @Override
    public void createRepertoire(String title, Date startDate, Date endDate) {
        if (getRepertoire(title) == null) {
            PersistenceManager persistanceManager = new PersistenceManager();
            persistanceManager.persist(new Repertoire(null, title, startDate, endDate));
        }
    }

    @Override
    public void deleteRepertoire(Long id) {
        PersistenceManager persistanceManager = new PersistenceManager();
        persistanceManager.delete(Repertoire.class, id);
    }

    @Override
    public void updateRepertoire(RepertoireDTO repertoire) {
        PersistenceManager persistanceManager = new PersistenceManager();
//        persistanceManager.merge(new Repertoire(repertoire));
    }

    @Override
    public RepertoireDTO getRepertoire(String title) {
        ArrayList<Repertoire> result;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<Repertoire> criteria = criteriaBuilder.createQuery(Repertoire.class);
        Root<Repertoire> root = criteria.from(Repertoire.class);
        criteria.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(Repertoire_.title), title)
                )
        );
        result = (ArrayList<Repertoire>) persistanceManager.executeStatement(criteria);

        return result.size() == 1 ? Converter.createRepertoireDTO(result.get(0)) : null;
    }

    @Override
    public ArrayList<RepertoireDTO> getRepertoires() {
        ArrayList temp;
        ArrayList<RepertoireDTO> result = null;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<Repertoire> criteria = criteriaBuilder.createQuery(Repertoire.class);
        Root<Repertoire> root = criteria.from(Repertoire.class);
        criteria.select(root);
        temp = (ArrayList) persistanceManager.executeStatement(criteria);
        if (temp != null) {
            result = new ArrayList<>();
            for (Object repertoire : temp) {
                if (repertoire instanceof Repertoire) {
                    result.add(Converter.createRepertoireDTO((Repertoire) repertoire));
                }
            }
        }

        return result;
    }

}