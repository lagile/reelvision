package com.projectmas.gwt.reelvision.server.service.impl;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.projectmas.gwt.reelvision.client.service.ShowcaseService;
import com.projectmas.gwt.reelvision.server.model.*;
import com.projectmas.gwt.reelvision.server.util.Converter;
import com.projectmas.gwt.reelvision.server.util.PersistenceManager;
import com.projectmas.gwt.reelvision.shared.dto.MovieDTO;

import com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO;
import com.projectmas.gwt.reelvision.shared.dto.SeatDTO;
import com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.ArrayList;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-06.
 */
public class ShowcaseServiceImpl extends RemoteServiceServlet implements ShowcaseService {

    @Override
    public ArrayList<MovieDTO> getMovies(Long repertoireId) {
        ArrayList temp;
        ArrayList<MovieDTO> result = null;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<Showcase> criteria = criteriaBuilder.createQuery(Showcase.class);
        Root<Showcase> root = criteria.from(Showcase.class);
        criteria.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(Showcase_.repertoire), repertoireId)
                )
        );
        temp = (ArrayList) persistanceManager.executeStatement(criteria);
        if (temp != null) {
            result = new ArrayList<>();
            for (Object showacase : temp) {
                if (showacase instanceof Showcase) {
                    result.add(Converter.createMovieDTO(((Showcase) showacase).getMovie()));
                }
            }
        }

        return result;
    }

    @Override
    public ArrayList<ScreeningDTO> getScreenings(Long showcaseId) {
        ArrayList temp;
        ArrayList<ScreeningDTO> result = null;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<Screening> criteria = criteriaBuilder.createQuery(Screening.class);
        Root<Screening> root = criteria.from(Screening.class);
        criteria.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(Screening_.showcase), showcaseId)
                )
        );
        temp = (ArrayList) persistanceManager.executeStatement(criteria);
        if (temp != null) {
            result = new ArrayList<>();
            for (Object screening : temp) {
                if (screening instanceof Screening) {
                    result.add(Converter.createScreeningDTO((Screening) screening));
                }
            }
        }

        return result;
    }

    @Override
    public ArrayList<SeatDTO> getSeats(Long auditoriumId) {
        ArrayList temp;
        ArrayList<SeatDTO> result = null;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<Seat> criteria = criteriaBuilder.createQuery(Seat.class);
        Root<Seat> root = criteria.from(Seat.class);
        criteria.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(Seat_.auditorium), auditoriumId)
                )
        );
        temp = (ArrayList) persistanceManager.executeStatement(criteria);
        if (temp != null) {
            result = new ArrayList<>();
            for (Object seat : temp) {
                if (seat instanceof Seat) {
                    result.add(Converter.createSeatDTO((Seat) seat));
                }
            }
        }

        return result;
    }

    @Override
    public ArrayList<SeatDTO> getOccupiedSeats(Long auditoriumId, Long screeningId) {
        ArrayList temp;
        ArrayList<SeatDTO> result = null;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<OccupiedSeat> criteria = criteriaBuilder.createQuery(OccupiedSeat.class);
        Root<OccupiedSeat> root = criteria.from(OccupiedSeat.class);
        Join<OccupiedSeat, Seat> seatJoin = root.join(OccupiedSeat_.seat);
        criteria.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(OccupiedSeat_.screening), screeningId),
                        criteriaBuilder.equal(seatJoin.get(Seat_.auditorium), auditoriumId)
                )
        );
        temp = (ArrayList) persistanceManager.executeStatement(criteria);
        if (temp != null) {
            result = new ArrayList<>();
            for (Object reservedSeat : temp) {
                if (reservedSeat instanceof OccupiedSeat) {
                    result.add(Converter.createSeatDTO(((OccupiedSeat) reservedSeat).getSeat()));
                }
            }
        }

        return result;
    }

    @Override
    public Boolean isSeatOccupied(Long auditoriumId, Long screeningId, Long seatID) {
        ArrayList temp;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<OccupiedSeat> criteria = criteriaBuilder.createQuery(OccupiedSeat.class);
        Root<OccupiedSeat> root = criteria.from(OccupiedSeat.class);
        Join<OccupiedSeat, Seat> seatJoin = root.join(OccupiedSeat_.seat);
        criteria.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(OccupiedSeat_.screening), screeningId),
                        criteriaBuilder.equal(seatJoin.get(Seat_.auditorium), auditoriumId),
                        criteriaBuilder.equal(seatJoin.get(Seat_.id), seatID)
                )
        );
        temp = (ArrayList) persistanceManager.executeStatement(criteria);

        return temp.size() != 0;
    }

    @Override
    public ArrayList<ShowcaseDTO> getShowcases(Long repertoireId) {
        ArrayList temp;
        ArrayList<ShowcaseDTO> result = null;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<Showcase> criteria = criteriaBuilder.createQuery(Showcase.class);
        Root<Showcase> root = criteria.from(Showcase.class);
        criteria.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(Showcase_.repertoire), repertoireId)
                )
        );
        temp = (ArrayList) persistanceManager.executeStatement(criteria);
        if (temp != null) {
            result = new ArrayList<>();
            for (Object showcase : temp) {
                if (showcase instanceof Showcase) {
                    result.add(Converter.createShowcaseDTO((Showcase) showcase));
                }
            }
        }

        return result;
    }

}
