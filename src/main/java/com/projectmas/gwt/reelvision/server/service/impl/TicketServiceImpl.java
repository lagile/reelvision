package com.projectmas.gwt.reelvision.server.service.impl;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.projectmas.gwt.reelvision.client.service.TicketService;
import com.projectmas.gwt.reelvision.server.model.*;
import com.projectmas.gwt.reelvision.server.util.Converter;
import com.projectmas.gwt.reelvision.server.util.PersistenceManager;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.TicketDTO;
import org.hibernate.HibernateException;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transaction;
import java.text.ParseException;
import java.util.*;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-13.
 */
public class TicketServiceImpl extends RemoteServiceServlet implements TicketService {


    @Override
    public List<TicketDTO> createTickets(List<TicketDTO> ticketDTOList) {
        Ticket newTicket = null;
        ShowcaseServiceImpl showcaseService = new ShowcaseServiceImpl();
        EntityManager entityManager = PersistenceManager.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            for (TicketDTO ticketDTO : ticketDTOList) {
                if (showcaseService.isSeatOccupied(ticketDTO.getSeat().getAuditorium().getId(), ticketDTO.getScreening().getId(), ticketDTO.getSeat().getId())) {
                    throw new HibernateException("Chosen seat is occupied!");
                }
                if (ticketDTO.getTicketType() == TicketDTO.TicketType.NORMAL) {
                    newTicket = new NormalTicket(Converter.createScreeningFromDTO(ticketDTO.getScreening()), new Date(), 0);
                } else if (ticketDTO.getTicketType() == TicketDTO.TicketType.REDUCED) {
                    newTicket = new ReducedTicket(Converter.createScreeningFromDTO(ticketDTO.getScreening()), new Date(), 50);
                }
                entityManager.persist(newTicket);
                entityManager.flush();
                entityManager.persist(new OccupiedSeat(null, Converter.createSeatFromDTO(ticketDTO.getSeat()),
                        Converter.createScreeningFromDTO(ticketDTO.getScreening()), null, newTicket));
                ticketDTO.setPrice(newTicket.getPrice());
            }
        } catch (HibernateException exc) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            throw new HibernateException(exc);
        } finally {
            if (transaction.isActive()) {
                transaction.commit();
            }
            if (entityManager.isOpen()) {
                entityManager.close();
            }
        }
        return ticketDTOList;
    }


//    @Override
//
//
//    public void createTicket(TicketDTO ticketDTO) {
//        if (ticketDTO == null) {
//            return;
//        }
//        ShowcaseServiceImpl showcaseService = new ShowcaseServiceImpl();
//        if (!showcaseService.isSeatOccupied(ticketDTO.getScreening().getAuditorium().getId(),
//                ticketDTO.getScreening().getId(), ticketDTO.getSeat().getId())) {
//            PersistenceManager persistanceManager = new PersistenceManager();
//            try {
//                persistanceManager.persist(new OccupiedSeat(null, Converter.createSeatFromDTO(ticketDTO.getSeat()), Converter.createScreeningFromDTO(ticketDTO.getScreening()), null, Converter.createTicketFromDTO(ticketDTO)));
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }
//
//    }

//    private Ticket getTicket(Long screeningID, Long seatId){
//        ArrayList temp;
//        ArrayList<Ticket> result = null;
//        PersistenceManager persistanceManager = new PersistenceManager();
//        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
//        CriteriaQuery<Ticket> criteria = criteriaBuilder.createQuery(Ticket.class);
//        Root<Ticket> root = criteria.from(Ticket.class);
//        Join<Ticket, Seat> seatJoin = root.join(Ticket_);
//        criteria.select(root).where(
//                criteriaBuilder.and(
//                        criteriaBuilder.equal(root.get(Ticket_.screening), screeningID),
//                        criteriaBuilder.equal(seatJoin.get(Seat_.id), seatId)
//                )
//        );
//        temp = (ArrayList) persistanceManager.executeStatement(criteria);
//        if (temp != null) {
//            result = new ArrayList<>();
//            for (Object reservedSeat : temp) {
//                if (reservedSeat instanceof OccupiedSeat) {
//                    result.add(Converter.createSeatDTO(((OccupiedSeat) reservedSeat).getSeat()));
//                }
//            }
//        }
//
//        return result;
//
//    }
}