package com.projectmas.gwt.reelvision.server.service.impl;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.projectmas.gwt.reelvision.client.service.UserService;

import com.projectmas.gwt.reelvision.server.model.PrivClass;
import com.projectmas.gwt.reelvision.server.model.PrivClass_;
import com.projectmas.gwt.reelvision.server.model.User;
import com.projectmas.gwt.reelvision.server.model.User_;
import com.projectmas.gwt.reelvision.server.util.PersistenceManager;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO;
import com.projectmas.gwt.reelvision.shared.dto.UserDTO;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by lukasz on 2016-04-22.
 */

public class UserServiceImpl extends RemoteServiceServlet implements UserService {

    @Override
    public UserDTO loginUser(String login, String password) {
        UserDTO user = getUser(login, password);
        if (user != null) {
            user.setLastLogin(new Date());
            addUserToSession(user);
        }

        return user;
    }

    @Override
    public UserDTO loginUserFromSession() {
        UserDTO user = getUserFromSession();
        if (user != null) {
            user.setLastLogin(new Date());
        }

        return user;
    }

    @Override
    public UserDTO getCurrentUser() {
        return getUserFromSession();
    }

    @Override
    public void createUser(String login, String password, String privClassName) {
        if (getUser(login, password) == null) {
            PersistenceManager persistanceManager = new PersistenceManager();
            persistanceManager.persist(new User(login, password, null, null, new Date(), getPrivClass(privClassName)));
        }
    }

    @Override
    public boolean changeUserPassword(String login, String newPassword) {
        return false;
    }

    @Override
    public void logoutUser() {
        UserDTO user = getUserFromSession();
        if (user != null) {
            updateUser(user);
            removeUserFromSession();
        }
    }

    @Override
    public ArrayList<UserDTO> getUsers() {
        ArrayList temp;
        ArrayList<UserDTO> result = null;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<User> criteria = criteriaBuilder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.select(root);
        temp = (ArrayList) persistanceManager.executeStatement(criteria);
        if (temp != null) {
            result = new ArrayList<>();
            for (Object user : temp) {
                if (user instanceof User) {
                    result.add(new UserDTO((User) user));
                }
            }
        }

        return result;
    }

    @Override
    public ArrayList<PrivClassDTO> getPrivClasses() {
        ArrayList temp;
        ArrayList<PrivClassDTO> result = null;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<PrivClass> criteria = criteriaBuilder.createQuery(PrivClass.class);
        Root<PrivClass> root = criteria.from(PrivClass.class);
        criteria.select(root);
        temp = (ArrayList) persistanceManager.executeStatement(criteria);
        if (temp != null) {
            result = new ArrayList<>();
            for (Object privClass : temp) {
                if (privClass instanceof PrivClass) {
                    result.add(new PrivClassDTO((PrivClass) privClass));
                }
            }
        }

        return result;
    }

    @Override
    public void removeUser(Long id) {
        PersistenceManager persistanceManager = new PersistenceManager();
        persistanceManager.delete(User.class, id);
    }

    private UserDTO getUser(String login, String password) {
        ArrayList<User> result;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<User> criteria = criteriaBuilder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(User_.login), login),
                        criteriaBuilder.equal(root.get(User_.password), password)
                )
        );
        result = (ArrayList<User>) persistanceManager.executeStatement(criteria);

        return result.size() == 1 ? new UserDTO(result.get(0)) : null;
    }

    private PrivClass getPrivClass(String privClassName) {
        ArrayList<PrivClass> result;
        PersistenceManager persistanceManager = new PersistenceManager();
        CriteriaBuilder criteriaBuilder = PersistenceManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<PrivClass> criteria = criteriaBuilder.createQuery(PrivClass.class);
        Root<PrivClass> root = criteria.from(PrivClass.class);
        criteria.select(root).where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(PrivClass_.className), privClassName)
                )
        );
        result = (ArrayList<PrivClass>) persistanceManager.executeStatement(criteria);

        return result.size() == 1 ? result.get(0) : null;
    }

    private UserDTO getUserFromSession() {
        UserDTO user = null;
        HttpServletRequest httpServletRequest = this.getThreadLocalRequest();
        HttpSession session = httpServletRequest.getSession();
        Object userObject = session.getAttribute(Constants.USER_SESSION_ATTR);
        if (userObject != null && userObject instanceof UserDTO) {
            user = (UserDTO) userObject;
        }

        return user;
    }

    private void updateUser(UserDTO user) {
        PersistenceManager persistanceManager = new PersistenceManager();
        if (user != null) {
            persistanceManager.merge(new User(user));
        }
    }

    private void addUserToSession(UserDTO user) {
        HttpServletRequest httpServletRequest = this.getThreadLocalRequest();
        HttpSession session = httpServletRequest.getSession();
        session.setAttribute(Constants.USER_SESSION_ATTR, user);
    }

    private void removeUserFromSession() {
        HttpServletRequest httpServletRequest = this.getThreadLocalRequest();
        HttpSession session = httpServletRequest.getSession();
        session.removeAttribute(Constants.USER_SESSION_ATTR);
    }
}
