package com.projectmas.gwt.reelvision.server.util;

import com.projectmas.gwt.reelvision.server.model.*;
import com.projectmas.gwt.reelvision.shared.Constants;
import com.projectmas.gwt.reelvision.shared.dto.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-06.
 */
public class Converter {

    public static AuditoriumDTO createAuditoriumDTO(Auditorium auditorium) {
        return new AuditoriumDTO(auditorium.getId(), auditorium.getNo(), auditorium.getName());
    }

    public static ScreeningDTO createScreeningDTO(Screening screening) {
        return new ScreeningDTO(screening.getId(), createShowcaseDTO(screening.getShowcase()),
                createAuditoriumDTO(screening.getAuditorium()), screening.getScreeningDate());
    }

    public static ShowcaseDTO createShowcaseDTO(Showcase showcase) {
        return new ShowcaseDTO(showcase.getId(), Converter.createMovieDTO(showcase.getMovie()), createRepertoireDTO(showcase.getRepertoire()));
    }

    public static RepertoireDTO createRepertoireDTO(Repertoire repertoire) {
        return new RepertoireDTO(repertoire.getId(), repertoire.getTitle(), repertoire.getStartDate(), repertoire.getEndDate());
    }

    public static Seat createSeatFromDTO(SeatDTO seatDTO) {
        return new Seat(seatDTO.getId(), seatDTO.getRow(), seatDTO.getNo(), createAuditoriumFromDTO(seatDTO.getAuditorium()));
    }

    public static Movie createMovieFromDTO(MovieDTO movieDTO) {
        return new Movie(movieDTO.getId(), movieDTO.getTitle(), movieDTO.getDirector(), movieDTO.getCountry(), movieDTO.getReleaseDate(), movieDTO.getDescription());
    }

    public static Showcase createShowcaseFromDTO(ShowcaseDTO showcaseDTO) {
        return new Showcase(showcaseDTO.getId(), createMovieFromDTO(showcaseDTO.getMovie()), createRepertoireFromDTO(showcaseDTO.getRepertoire()));
    }

    public static Screening createScreeningFromDTO(ScreeningDTO screeningDTO)  {
//        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.YYYY_MM_DD_HH_MM_SS);
        return new Screening(screeningDTO.getId(), createShowcaseFromDTO(screeningDTO.getShowcase()), createAuditoriumFromDTO(screeningDTO.getAuditorium()), screeningDTO.getScreeningDate());
}

//    public static Ticket createTicketFromDTO(TicketDTO ticketDTO) throws ParseException {
//        return new Ticket(null, createScreeningFromDTO(ticketDTO.getScreening()), ticketDTO.getPrice());
//    }

    public static Auditorium createAuditoriumFromDTO(AuditoriumDTO auditoriumDTO) {
        return new Auditorium(auditoriumDTO.getId(), auditoriumDTO.getNo(), auditoriumDTO.getName());
    }

    public static Repertoire createRepertoireFromDTO(RepertoireDTO repertoire) {
        return new Repertoire(repertoire.getId(), repertoire.getTitle(), repertoire.getStartDate(), repertoire.getEndDate());
    }

    public static MovieDTO createMovieDTO(Movie movie) {
        return new MovieDTO(movie.getId(), movie.getTitle(), movie.getDirector(), movie.getCountry(), movie.getReleaseDate(), movie.getDescription());
    }

    public static SeatDTO createSeatDTO(Seat seat) {
        return new SeatDTO(seat.getId(), seat.getRow(), seat.getNo(), createAuditoriumDTO(seat.getAuditorium()));
    }
}
