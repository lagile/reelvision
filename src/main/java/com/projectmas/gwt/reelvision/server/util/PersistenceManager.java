package com.projectmas.gwt.reelvision.server.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.List;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-26.
 */
public class PersistenceManager {

    private static EntityManagerFactory entityManagerFactory;

    static {
        entityManagerFactory = javax.persistence.Persistence.createEntityManagerFactory("oracle");
    }

    public List<?> executeStatement(String query, Class<?> objectClass) {
        List<?> resultSet;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultSet = entityManager.createQuery(query, objectClass).getResultList();
            entityManager.getTransaction().commit();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }

        return resultSet;
    }

    public List<?> executeStatement(CriteriaQuery criteriaQuery) {
        List<?> resultSet = new ArrayList<>();
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultSet = entityManager.createQuery(criteriaQuery).getResultList();
            entityManager.getTransaction().commit();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }

        return resultSet;
    }

    public void delete(Class objectClass, Long id) {
        Object object;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            object = entityManager.find(objectClass, id);
            entityManager.remove(object);
            entityManager.getTransaction().commit();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public Object find(Class objectClass, Long id) {
        Object object;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            object = entityManager.find(objectClass, id);
            entityManager.getTransaction().commit();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }

        return object;
    }

    public void merge(Object object) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(object);
            entityManager.getTransaction().commit();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public void persist(Object object) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(object);
            entityManager.getTransaction().commit();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public static EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }
}
