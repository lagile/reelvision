package com.projectmas.gwt.reelvision.shared;


/**
 * Created by lukasz on 2016-05-08.
 */
public class Constants {


    public final static long COOKIE_EXPIRY = 86400000;

    //Essentials
    public final static String HASH = "#";
    public final static String EMPTY_STRING = "";
    public final static String PX = "px";
    public final static String DASH = "-";
    public final static String YYYY_MM_DD = "yyyy-mm-dd";
    public final static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public final static String CHOOSE = "Choose";
    public final static String TODAY = "Today";
    //LoginForm
    public final static String LOG_IN = "Log in";
    public final static String LOG_OUT = "Log out";
    public final static String LOGIN = "Login";
    public final static String PASSWORD = "Password";
    public final static String ENTER_LOGIN = "Enter login";
    public final static String ENTER_PASSWORD = "Enter password";
    public final static String SETTINGS = "Settings";
    public final static String WRONG_PASSWORD = "Access Denied. Check your login and password";

    //SideNavigation
    public final static String DASHBOARD = "Dashboard";
    public final static String RESERVATIONS = "Reservations";
    public final static String SALE = "Sale";
    public final static String AUDIENCES = "Audiences";
    public final static String SHOWTIMES = "Showtimes";
    public final static String MOVIES = "Movies";
    public final static String SHOWTIME_MANAGEMENT = "Showtime Management";
    public final static String MOVIE_MANAGEMENT = "Movie Management";
    public final static String USER_MANAGEMENT = "User Management";
    public final static String CUSTOMERS = "Customers";
    public final static String REPORTS = "Reports";
    public final static String NEW = "New";
    public final static String FIND = "Find";
    public final static String REPERTOIRE_MANAGEMENT = "Repertoire Management";


    //Alert Messages
    public final static String PRIVCLASS_OBTAIN_PROBLEM = "Cannot obtain user privilage class! Please contact System Administrator.";
    public final static String LOG_OUT_PROBLEM = "Cannot logout. Please contact System Administrator";
    public final static String UNEXPECTED_ERROR = "System encounter unexpected error. Please contact System Administrator!";

    //Privilage class
    public final static String PRIVILAGE_CLASS = "Privilage class";
    public final static String ADMINISTRATOR = "administrator";
    public final static String MANAGER = "manager";
    public final static String USER = "user";

    //UserManagement
    public final static String CONFIRM_PASSWORD = "Confirm Password";
    public final static String USERS = "Users";
    public final static String ENTER_PASSWORD_CONFIRMATION = "Enter password confirmation";
    public final static String ADD_USER = "Add user";
    public final static String DELETE_USER = "Delete user";
    public final static String CREATED = "Created";
    public final static String UPDATED = "Updated";
    public final static String PRIVILAGE = "Privilage";
    public final static String LAST_LOGIN = "Last login";
    public final static String USER_EXISTS_ALREADY_EXISTS = "User already exists!";
    public final static String USER_ADDED = "User added!";
    public final static String USER_DELETED = "User deleted!";
    public final static String PROVIDED_PASSWORDS_ARE_DIFFERENT = "Provided passwords are different!";
    public final static String CANNOT_DELETE_CURRENT_LOGGED_IN_USER = "Cannot delete current logged in user!";

    //MovieManagement
    public final static String ADD_OR_EDIT_MOVIE = "Add/Edit Movie";
    public final static String DIRECTOR = "Director";
    public final static String TITLE = "Title";
    public final static String RELEASE_DATE = "Release Date";
    public final static String DESCRIPTION = "Description";
    public final static String COUNTRY = "Country";
    public final static String ADD_MOVIE = "Add Movie";
    public final static String EDIT_MOVIE = "Edit Movie";
    public final static String REMOVE_MOVIE = "Remove Movie";
    public final static String MOVIE_ADDED = "Movie added!";
    public final static String MOVIE_DELETED = "Movie deleted!";
    public final static String MOVIE_EDITED = "Movie edited!";
    public final static String PLEASE_SELECT_MOVIE_TO_DELETE = "Please select movie to delete.";
    public final static String PLEASE_SELECT_MOVIE_TO_EDIT = "Please select movie to edit.";

    //RepertoireManagement
    public final static String ADD_OR_EDIT_REPERTOIRE = "Add/Edit Repertoire";
    public final static String REPERTOIRES = "Repertoires";
    public final static String ADD_REPERTOIRE = "Add Repertoire";
    public final static String EDIT_REPERTOIRE = "Edit Repertoire";
    public final static String REMOVE_REPERTOIRE = "Remove Repertoire";
    public final static String START_DATE = "Start date";
    public final static String END_DATE = "End date";

    //IDs
    public final static String NEW_SALE_ANCHOR_ID = "newSaleAnchor";
    public final static String SEARCH_SALE_ANCHOR_ID = "searchSaleAnchor";
    public final static String NEW_RESERVE_ANCHOR_ID = "newReserveAnchor";
    public final static String SEARCH_RESERVE_ANCHOR_ID = "searchReserveAnchor";
    public final static String DASHBOARD_ANCHOR_ID = "dashboardAnchor";
    public final static String SHOWTIME_ANCHOR_ID = "showtimeAnchor";
    public final static String MOVIES_ANCHOR_ID = "moviesAnchor";
    public final static String AUDIENCES_ANCHOR_ID = "audiencesAnchor";
    public final static String REPORTS_ANCHOR_ID = "reportsAnchor";
    public final static String SHOWTIME_MANAGEMENT_ID = "showtimeManagement";
    public final static String MOVIE_MANAGEMENT_ID = "movieManagement";
    public final static String USER_MANAGEMENT_ID = "userManagement";
    public final static String REPERTOIRE_MANAGEMENT_ID = "repertoireManagement";

    //Session attributes
    public final static String USER_SESSION_ATTR = "user";

}
