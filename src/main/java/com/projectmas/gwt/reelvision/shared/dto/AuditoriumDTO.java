package com.projectmas.gwt.reelvision.shared.dto;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-02.
 */
public class AuditoriumDTO extends ObjectDTO {

    private Long id;
    private Integer no;
    private String name;

    public AuditoriumDTO() {
    }

    public AuditoriumDTO(Long id, int number, String name) {
        this.id = id;
        this.no = number;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
