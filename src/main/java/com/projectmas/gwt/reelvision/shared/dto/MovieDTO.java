package com.projectmas.gwt.reelvision.shared.dto;

import com.projectmas.gwt.reelvision.server.model.Movie;

import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-29.
 */
public class MovieDTO extends ObjectDTO {

    private Long id;
    private String title;
    private String director;
    private String country;
    private Date releaseDate;
    private String description;

    public MovieDTO() {
    }

    public MovieDTO(Long id, String title, String director, String country, Date releaseDate, String description) {
        this.id = id;
        this.title = title;
        this.director = director;
        this.country = country;
        this.releaseDate = releaseDate;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
