package com.projectmas.gwt.reelvision.shared.dto;

import com.projectmas.gwt.reelvision.server.model.PrivClass;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-05-24.
 */

public class PrivClassDTO extends ObjectDTO {

    private Long id;
    private String className;
    private String description;

    public PrivClassDTO() {
    }

    public PrivClassDTO(PrivClass privClass) {
        this.id = privClass.getId();
        this.className = privClass.getClassName();
        this.description = privClass.getDescription();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


