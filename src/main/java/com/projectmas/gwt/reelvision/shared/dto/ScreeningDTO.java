package com.projectmas.gwt.reelvision.shared.dto;

import java.util.Date;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-02.
 */
public class ScreeningDTO extends ObjectDTO {

    private Long id;
    private ShowcaseDTO showcase;
    private AuditoriumDTO auditorium;
    private Date screeningDate;

    public ScreeningDTO() {
    }

    public ScreeningDTO(Long id, ShowcaseDTO showcase, AuditoriumDTO auditorium, Date screeningDate) {
        this.id = id;
        this.showcase = showcase;
        this.auditorium = auditorium;
        this.screeningDate = screeningDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShowcaseDTO getShowcase() {
        return showcase;
    }

    public void setShowcase(ShowcaseDTO showcase) {
        this.showcase = showcase;
    }

    public AuditoriumDTO getAuditorium() {
        return auditorium;
    }

    public void setAuditorium(AuditoriumDTO auditorium) {
        this.auditorium = auditorium;
    }

    public Date getScreeningDate() {
        return screeningDate;
    }

    public void setScreeningDate(Date screeningDate) {
        this.screeningDate = screeningDate;
    }

    public boolean equals(Object object) {
        if (object == null) return false;
        if (object == this) return true;
        if (!(object instanceof ScreeningDTO)) return false;
        ScreeningDTO other = (ScreeningDTO) object;
        return this.getId().equals(other.getId());
    }
}
