package com.projectmas.gwt.reelvision.shared.dto;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-10.
 */
public class SeatDTO extends ObjectDTO {

    private Long id;
    private Character row;
    private Integer no;
    private AuditoriumDTO auditorium;

    public SeatDTO() {
    }

    public SeatDTO(Long id, Character row, Integer no, AuditoriumDTO auditorium) {
        this.id = id;
        this.row = row;
        this.no = no;
        this.auditorium = auditorium;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getRow() {
        return row;
    }

    public void setRow(Character row) {
        this.row = row;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public AuditoriumDTO getAuditorium() {
        return auditorium;
    }

    public void setAuditorium(AuditoriumDTO auditorium) {
        this.auditorium = auditorium;
    }

    public boolean equals(Object object) {
        if (object == null) return false;
        if (object == this) return true;
        if (!(object instanceof SeatDTO)) return false;
        SeatDTO other = (SeatDTO) object;
        return this.getId().equals(other.getId());
    }
}
