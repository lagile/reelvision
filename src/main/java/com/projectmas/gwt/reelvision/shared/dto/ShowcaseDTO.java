package com.projectmas.gwt.reelvision.shared.dto;
/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-02.
 */
public class ShowcaseDTO extends ObjectDTO {
    private Long id;
    private MovieDTO movie;
    private RepertoireDTO repertoire;
//    private Set<ScreeningDTO> screenings;

    public ShowcaseDTO() {
    }

    public ShowcaseDTO(Long id, MovieDTO movie, RepertoireDTO repertoire) {
        this.id = id;
        this.movie = movie;
        this.repertoire = repertoire;
//        this.screenings = screenings;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MovieDTO getMovie() {
        return movie;
    }

    public void setMovie(MovieDTO movie) {
        this.movie = movie;
    }

    public RepertoireDTO getRepertoire() {
        return repertoire;
    }

    public void setRepertoire(RepertoireDTO repertoire) {
        this.repertoire = repertoire;
    }
//
//    public Set<ScreeningDTO> getScreenings() {
//        return screenings;
//    }
//
//    public void setScreenings(Set<ScreeningDTO> screenings) {
//        this.screenings = screenings;
//    }
}
