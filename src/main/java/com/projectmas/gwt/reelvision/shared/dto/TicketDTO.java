package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.projectmas.gwt.reelvision.shared.Constants;

/**
 * ${CLASS}
 * Created by Lukasz Jedrzynski on 2016-06-02.
 */

public class TicketDTO extends ObjectDTO {
    private Long id;
    private ScreeningDTO screening;
    private TicketType ticketType;
    private SeatDTO seat;
    private Double price;

    public enum TicketType {
        NORMAL,
        REDUCED
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ScreeningDTO getScreening() {
        return screening;
    }

    public void setScreening(ScreeningDTO screening) {
        this.screening = screening;
    }

    public TicketType getTicketType() {
        return ticketType;
    }

    public void setTicketType(TicketType ticketType) {
        this.ticketType = ticketType;
    }

    public SeatDTO getSeat() {
        return seat;
    }

    public void setSeat(SeatDTO seat) {
        this.seat = seat;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Ticket:" +
                ", movie=''" + screening.getShowcase().getMovie().getTitle() + "'" + '\n' +
                ", screening date=" + DateTimeFormat.getFormat(Constants.YYYY_MM_DD_HH_MM_SS).format(screening.getScreeningDate()) + '\n' +
                ", type=" + ticketType.toString() + '\n' +
                ", auditorium=" + screening.getAuditorium().getNo()
                + ":" + screening.getAuditorium().getName() + '\n' +
                ", seat=" + seat.getRow() + ":" + seat.getNo() + '\n' +
                ", price=" + price + " PLN" + '\n' +
                '\n';
    }

    public boolean equals(Object object) {
        if (object == null) return false;
        if (object == this) return true;
        if (!(object instanceof TicketDTO)) return false;
        TicketDTO other = (TicketDTO) object;
        return this.getScreening().equals(other.getScreening())
                && this.getSeat().equals(other.getSeat());
    }

}
