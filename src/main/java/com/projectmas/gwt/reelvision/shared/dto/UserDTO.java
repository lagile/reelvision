package com.projectmas.gwt.reelvision.shared.dto;

import com.projectmas.gwt.reelvision.server.model.User;

import java.util.Date;

/**
 * Created by lukasz on 2016-04-23.
 */

public class UserDTO extends ObjectDTO {

    private Long id;
    private String login;
    private String password;
    private String sessionId;
    private Date lastLogin;
    private Date updatedAt;
    private Date createdAt;
    private PrivClassDTO privClassDTO;

    public UserDTO() {
    }

    public UserDTO(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.lastLogin = user.getLastLogin();
        this.updatedAt = user.getUpdatedAt();
        this.createdAt = user.getCreatedAt();
        this.privClassDTO = new PrivClassDTO(user.getPrivClass());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public PrivClassDTO getPrivClassDTO() {
        return privClassDTO;
    }

    public void setPrivClassDTO(PrivClassDTO privClassDTO) {
        this.privClassDTO = privClassDTO;
    }
}