package com.projectmas.gwt.reelvision.client.resource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class Resources_default_InlineClientBundleGenerator implements com.projectmas.gwt.reelvision.client.resource.Resources {
  private static Resources_default_InlineClientBundleGenerator _instance0 = new Resources_default_InlineClientBundleGenerator();
  private void getStyleInitializer() {
    getStyle = new com.projectmas.gwt.reelvision.client.resource.Resources.Style() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "getStyle";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? (("body{margin-top:" + ("100px")  + ";background-color:" + ("#222")  + ";}.GD413K1CII{font-size:" + ("50px")  + ";line-height:" + ("normal")  + ";font-size:" + ("40px")  + ";}.GD413K1CKJ{padding-right:" + ("0")  + ";}.GD413K1CNI{padding-right:" + ("235px")  + ";padding-left:" + ("235px")  + ";background-color:" + ("#fff")  + ";}.GD413K1CLJ{padding-right:" + ("225px")  + ";}.GD413K1CJJ{padding:") + (("0"+ " " +"15px")  + ";}.GD413K1CJJ>li{display:" + ("inline-block")  + ";float:" + ("right")  + ";}.GD413K1CJJ>li>a{padding-top:" + ("15px")  + ";padding-bottom:" + ("15px")  + ";line-height:" + ("20px")  + ";color:" + ("#999")  + ";}.GD413K1CJJ>li>a:hover,.GD413K1CJJ>li>a:focus,.GD413K1CJJ>.GD413K1CMI>a,.GD413K1CJJ>.GD413K1CMI>a:hover,.GD413K1CJJ>.GD413K1CMI>a:focus{color:" + ("#fff")  + ";background-color:" + ("#000")  + ";}.GD413K1CJJ>.GD413K1CMI>.GD413K1CFI{float:" + ("right")  + ";position:" + ("absolute") ) + (";margin-top:" + ("0")  + ";border:" + ("1px"+ " " +"solid"+ " " +"rgba(" + "0"+ ","+ " " +"0"+ ","+ " " +"0"+ ","+ " " +"0.15" + ")")  + ";border-top-right-radius:" + ("0")  + ";border-top-left-radius:" + ("0")  + ";background-color:" + ("#fff")  + ";-webkit-box-shadow:" + ("0"+ " " +"6px"+ " " +"12px"+ " " +"rgba(" + "0"+ ","+ " " +"0"+ ","+ " " +"0"+ ","+ " " +"0.175" + ")")  + ";box-shadow:" + ("0"+ " " +"6px"+ " " +"12px"+ " " +"rgba(" + "0"+ ","+ " " +"0"+ ","+ " " +"0"+ ","+ " " +"0.175" + ")")  + ";}.GD413K1CJJ>.GD413K1CMI>.GD413K1CFI>li>a{white-space:" + ("normal")  + ";}ul.GD413K1CJI{padding:" + ("0")  + ";max-height:" + ("250px")  + ";overflow-x:") + (("hidden")  + ";overflow-y:" + ("auto")  + ";}li.GD413K1CLI{width:" + ("275px")  + ";border-bottom:" + ("1px"+ " " +"solid"+ " " +"rgba(" + "0"+ ","+ " " +"0"+ ","+ " " +"0"+ ","+ " " +"0.15" + ")")  + ";}li.GD413K1CLI>a{padding-top:" + ("15px")  + ";padding-bottom:" + ("15px")  + ";}li.GD413K1CKI{margin:" + ("5px"+ " " +"0")  + ";}ul.GD413K1CEI{width:" + ("200px")  + ";}.GD413K1CIJ{position:" + ("fixed")  + ";top:" + ("51px")  + ";right:" + ("225px") ) + (";width:" + ("225px")  + ";margin-right:" + ("-225px")  + ";border:" + ("none")  + ";border-radius:" + ("0")  + ";overflow-y:" + ("auto")  + ";background-color:" + ("#222")  + ";bottom:" + ("0")  + ";overflow-x:" + ("hidden")  + ";padding-bottom:" + ("40px")  + ";}.GD413K1CIJ>li>a{width:" + ("225px")  + ";}.GD413K1CIJ li a:hover,.GD413K1CIJ li a:focus{outline:") + (("none")  + ";background-color:" + ("#000")  + " !important;}.GD413K1CIJ>li>ul{padding:" + ("0")  + ";}.GD413K1CIJ>li>ul>li>a{display:" + ("block")  + ";padding:" + ("10px"+ " " +"38px"+ " " +"10px"+ " " +"15px")  + ";text-decoration:" + ("none")  + ";color:" + ("#999")  + ";}.GD413K1CIJ>li>ul>li>ul>li>a{display:" + ("block")  + ";padding:" + ("10px"+ " " +"20px"+ " " +"10px"+ " " +"15px")  + ";text-decoration:" + ("none")  + ";color:" + ("#999") ) + (";}.GD413K1CIJ>li>ul>li>a:hover{color:" + ("#fff")  + ";}.GD413K1CGI{display:" + ("block")  + ";height:" + ("400px")  + ";}.GD413K1CHI{width:" + ("100%")  + ";height:" + ("100%")  + ";}.GD413K1COI{border-color:" + ("#5cb85c")  + ";}.GD413K1COI>.GD413K1CPI{border-color:" + ("#5cb85c")  + ";color:" + ("#fff")  + ";background-color:" + ("#5cb85c")  + ";}.GD413K1COI>a{color:" + ("#5cb85c")  + ";}.GD413K1COI>a:hover{color:") + (("#3d8b3d")  + ";}.GD413K1CAJ{border-color:" + ("#d9534f")  + ";}.GD413K1CAJ>.GD413K1CPI{border-color:" + ("#d9534f")  + ";color:" + ("#fff")  + ";background-color:" + ("#d9534f")  + ";}.GD413K1CAJ>a{color:" + ("#d9534f")  + ";}.GD413K1CAJ>a:hover{color:" + ("#b52b27")  + ";}.GD413K1CBJ{border-color:" + ("#f0ad4e")  + ";}.GD413K1CBJ>.GD413K1CPI{border-color:" + ("#f0ad4e")  + ";color:" + ("#fff")  + ";background-color:" + ("#f0ad4e") ) + (";}.GD413K1CBJ>a{color:" + ("#f0ad4e")  + ";}.GD413K1CBJ>a:hover{color:" + ("#df8a13")  + ";}.GD413K1CCJ{font-size:" + ("20px")  + ";color:" + ("white")  + ";background-color:" + ("#000")  + ";}.GD413K1CEJ{margin-bottom:" + ("10px")  + ";width:" + ("60px")  + ";height:" + ("50px")  + ";}.GD413K1CHJ{margin-right:" + ("auto")  + ";margin-left:" + ("auto")  + ";horiz-align:") + (("center")  + ";}.GD413K1CGJ{background-color:" + ("salmon")  + ";}.GD413K1CDJ{background-color:" + ("palegreen")  + ";}.GD413K1CFJ{background-color:" + ("mediumorchid")  + ";}")) : (("body{margin-top:" + ("100px")  + ";background-color:" + ("#222")  + ";}.GD413K1CII{font-size:" + ("50px")  + ";line-height:" + ("normal")  + ";font-size:" + ("40px")  + ";}.GD413K1CKJ{padding-left:" + ("0")  + ";}.GD413K1CNI{padding-left:" + ("235px")  + ";padding-right:" + ("235px")  + ";background-color:" + ("#fff")  + ";}.GD413K1CLJ{padding-left:" + ("225px")  + ";}.GD413K1CJJ{padding:") + (("0"+ " " +"15px")  + ";}.GD413K1CJJ>li{display:" + ("inline-block")  + ";float:" + ("left")  + ";}.GD413K1CJJ>li>a{padding-top:" + ("15px")  + ";padding-bottom:" + ("15px")  + ";line-height:" + ("20px")  + ";color:" + ("#999")  + ";}.GD413K1CJJ>li>a:hover,.GD413K1CJJ>li>a:focus,.GD413K1CJJ>.GD413K1CMI>a,.GD413K1CJJ>.GD413K1CMI>a:hover,.GD413K1CJJ>.GD413K1CMI>a:focus{color:" + ("#fff")  + ";background-color:" + ("#000")  + ";}.GD413K1CJJ>.GD413K1CMI>.GD413K1CFI{float:" + ("left")  + ";position:" + ("absolute") ) + (";margin-top:" + ("0")  + ";border:" + ("1px"+ " " +"solid"+ " " +"rgba(" + "0"+ ","+ " " +"0"+ ","+ " " +"0"+ ","+ " " +"0.15" + ")")  + ";border-top-left-radius:" + ("0")  + ";border-top-right-radius:" + ("0")  + ";background-color:" + ("#fff")  + ";-webkit-box-shadow:" + ("0"+ " " +"6px"+ " " +"12px"+ " " +"rgba(" + "0"+ ","+ " " +"0"+ ","+ " " +"0"+ ","+ " " +"0.175" + ")")  + ";box-shadow:" + ("0"+ " " +"6px"+ " " +"12px"+ " " +"rgba(" + "0"+ ","+ " " +"0"+ ","+ " " +"0"+ ","+ " " +"0.175" + ")")  + ";}.GD413K1CJJ>.GD413K1CMI>.GD413K1CFI>li>a{white-space:" + ("normal")  + ";}ul.GD413K1CJI{padding:" + ("0")  + ";max-height:" + ("250px")  + ";overflow-x:") + (("hidden")  + ";overflow-y:" + ("auto")  + ";}li.GD413K1CLI{width:" + ("275px")  + ";border-bottom:" + ("1px"+ " " +"solid"+ " " +"rgba(" + "0"+ ","+ " " +"0"+ ","+ " " +"0"+ ","+ " " +"0.15" + ")")  + ";}li.GD413K1CLI>a{padding-top:" + ("15px")  + ";padding-bottom:" + ("15px")  + ";}li.GD413K1CKI{margin:" + ("5px"+ " " +"0")  + ";}ul.GD413K1CEI{width:" + ("200px")  + ";}.GD413K1CIJ{position:" + ("fixed")  + ";top:" + ("51px")  + ";left:" + ("225px") ) + (";width:" + ("225px")  + ";margin-left:" + ("-225px")  + ";border:" + ("none")  + ";border-radius:" + ("0")  + ";overflow-y:" + ("auto")  + ";background-color:" + ("#222")  + ";bottom:" + ("0")  + ";overflow-x:" + ("hidden")  + ";padding-bottom:" + ("40px")  + ";}.GD413K1CIJ>li>a{width:" + ("225px")  + ";}.GD413K1CIJ li a:hover,.GD413K1CIJ li a:focus{outline:") + (("none")  + ";background-color:" + ("#000")  + " !important;}.GD413K1CIJ>li>ul{padding:" + ("0")  + ";}.GD413K1CIJ>li>ul>li>a{display:" + ("block")  + ";padding:" + ("10px"+ " " +"15px"+ " " +"10px"+ " " +"38px")  + ";text-decoration:" + ("none")  + ";color:" + ("#999")  + ";}.GD413K1CIJ>li>ul>li>ul>li>a{display:" + ("block")  + ";padding:" + ("10px"+ " " +"15px"+ " " +"10px"+ " " +"20px")  + ";text-decoration:" + ("none")  + ";color:" + ("#999") ) + (";}.GD413K1CIJ>li>ul>li>a:hover{color:" + ("#fff")  + ";}.GD413K1CGI{display:" + ("block")  + ";height:" + ("400px")  + ";}.GD413K1CHI{width:" + ("100%")  + ";height:" + ("100%")  + ";}.GD413K1COI{border-color:" + ("#5cb85c")  + ";}.GD413K1COI>.GD413K1CPI{border-color:" + ("#5cb85c")  + ";color:" + ("#fff")  + ";background-color:" + ("#5cb85c")  + ";}.GD413K1COI>a{color:" + ("#5cb85c")  + ";}.GD413K1COI>a:hover{color:") + (("#3d8b3d")  + ";}.GD413K1CAJ{border-color:" + ("#d9534f")  + ";}.GD413K1CAJ>.GD413K1CPI{border-color:" + ("#d9534f")  + ";color:" + ("#fff")  + ";background-color:" + ("#d9534f")  + ";}.GD413K1CAJ>a{color:" + ("#d9534f")  + ";}.GD413K1CAJ>a:hover{color:" + ("#b52b27")  + ";}.GD413K1CBJ{border-color:" + ("#f0ad4e")  + ";}.GD413K1CBJ>.GD413K1CPI{border-color:" + ("#f0ad4e")  + ";color:" + ("#fff")  + ";background-color:" + ("#f0ad4e") ) + (";}.GD413K1CBJ>a{color:" + ("#f0ad4e")  + ";}.GD413K1CBJ>a:hover{color:" + ("#df8a13")  + ";}.GD413K1CCJ{font-size:" + ("20px")  + ";color:" + ("white")  + ";background-color:" + ("#000")  + ";}.GD413K1CEJ{margin-bottom:" + ("10px")  + ";width:" + ("60px")  + ";height:" + ("50px")  + ";}.GD413K1CHJ{margin-left:" + ("auto")  + ";margin-right:" + ("auto")  + ";horiz-align:") + (("center")  + ";}.GD413K1CGJ{background-color:" + ("salmon")  + ";}.GD413K1CDJ{background-color:" + ("palegreen")  + ";}.GD413K1CFJ{background-color:" + ("mediumorchid")  + ";}"));
      }
      public java.lang.String alertDropdown() {
        return "GD413K1CEI";
      }
      public java.lang.String dropdownMenu() {
        return "GD413K1CFI";
      }
      public java.lang.String flotChart() {
        return "GD413K1CGI";
      }
      public java.lang.String flotChartContent() {
        return "GD413K1CHI";
      }
      public java.lang.String huge() {
        return "GD413K1CII";
      }
      public java.lang.String messageDropdown() {
        return "GD413K1CJI";
      }
      public java.lang.String messageFooter() {
        return "GD413K1CKI";
      }
      public java.lang.String messagePreview() {
        return "GD413K1CLI";
      }
      public java.lang.String open() {
        return "GD413K1CMI";
      }
      public java.lang.String pageWrapper() {
        return "GD413K1CNI";
      }
      public java.lang.String panelGreen() {
        return "GD413K1COI";
      }
      public java.lang.String panelHeading() {
        return "GD413K1CPI";
      }
      public java.lang.String panelRed() {
        return "GD413K1CAJ";
      }
      public java.lang.String panelYellow() {
        return "GD413K1CBJ";
      }
      public java.lang.String rowLabel() {
        return "GD413K1CCJ";
      }
      public java.lang.String seatAvail() {
        return "GD413K1CDJ";
      }
      public java.lang.String seatButton() {
        return "GD413K1CEJ";
      }
      public java.lang.String seatChosen() {
        return "GD413K1CFJ";
      }
      public java.lang.String seatOccupied() {
        return "GD413K1CGJ";
      }
      public java.lang.String seatPlan() {
        return "GD413K1CHJ";
      }
      public java.lang.String sideNav() {
        return "GD413K1CIJ";
      }
      public java.lang.String topNav() {
        return "GD413K1CJJ";
      }
      public java.lang.String wrapper() {
        return "GD413K1CKJ";
      }
      public java.lang.String wrapperMenu() {
        return "GD413K1CLJ";
      }
    }
    ;
  }
  private static class getStyleInitializer {
    static {
      _instance0.getStyleInitializer();
    }
    static com.projectmas.gwt.reelvision.client.resource.Resources.Style get() {
      return getStyle;
    }
  }
  public com.projectmas.gwt.reelvision.client.resource.Resources.Style getStyle() {
    return getStyleInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.projectmas.gwt.reelvision.client.resource.Resources.Style getStyle;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      getStyle(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("getStyle", getStyle());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'getStyle': return this.@com.projectmas.gwt.reelvision.client.resource.Resources::getStyle()();
    }
    return null;
  }-*/;
}
