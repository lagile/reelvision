package com.projectmas.gwt.reelvision.client.service;

import com.google.gwt.user.client.rpc.impl.RemoteServiceProxy;
import com.google.gwt.user.client.rpc.impl.ClientSerializationStreamWriter;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.impl.RequestCallbackAdapter.ResponseReader;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.RpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.core.client.impl.Impl;
import com.google.gwt.user.client.rpc.impl.RpcStatsContext;

public class TicketService_Proxy extends RemoteServiceProxy implements com.projectmas.gwt.reelvision.client.service.TicketServiceAsync {
  private static final String REMOTE_SERVICE_INTERFACE_NAME = "com.projectmas.gwt.reelvision.client.service.TicketService";
  private static final String SERIALIZATION_POLICY ="7A65801962B824A27623BA2B59D75356";
  private static final com.projectmas.gwt.reelvision.client.service.TicketService_TypeSerializer SERIALIZER = new com.projectmas.gwt.reelvision.client.service.TicketService_TypeSerializer();
  
  public TicketService_Proxy() {
    super(GWT.getModuleBaseURL(),
      "TicketService", 
      SERIALIZATION_POLICY, 
      SERIALIZER);
  }
  
  public void createTickets(java.util.List ticketDTOList, com.google.gwt.user.client.rpc.AsyncCallback async) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("TicketService_Proxy", "createTickets");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("java.util.List");
      streamWriter.writeObject(ticketDTOList);
      helper.finish(async, ResponseReader.OBJECT);
    } catch (SerializationException ex) {
      async.onFailure(ex);
    }
  }
  @Override
  public SerializationStreamWriter createStreamWriter() {
    ClientSerializationStreamWriter toReturn =
      (ClientSerializationStreamWriter) super.createStreamWriter();
    if (getRpcToken() != null) {
      toReturn.addFlags(ClientSerializationStreamWriter.FLAG_RPC_TOKEN_INCLUDED);
    }
    return toReturn;
  }
  @Override
  protected void checkRpcTokenType(RpcToken token) {
    if (!(token instanceof com.google.gwt.user.client.rpc.XsrfToken)) {
      throw new RpcTokenException("Invalid RpcToken type: expected 'com.google.gwt.user.client.rpc.XsrfToken' but got '" + token.getClass() + "'");
    }
  }
}
