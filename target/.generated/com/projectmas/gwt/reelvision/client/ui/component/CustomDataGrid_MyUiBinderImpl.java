// .ui.xml template last modified: 1465814886725
package com.projectmas.gwt.reelvision.client.ui.component;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiBinderUtil;
import com.google.gwt.user.client.ui.Widget;

public class CustomDataGrid_MyUiBinderImpl implements UiBinder<com.google.gwt.user.client.ui.Widget, com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid>, com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid.MyUiBinder {


  public com.google.gwt.user.client.ui.Widget createAndBindUi(final com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid owner) {


    return new Widgets(owner).get_f_Div1();
  }

  /**
   * Encapsulates the access to all inner widgets
   */
  class Widgets {
    private final com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid owner;


    public Widgets(final com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid owner) {
      this.owner = owner;
    }


    /**
     * Getter for clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay called 0 times. Type: GENERATED_BUNDLE. Build precedence: 1.
     */
    private com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid_MyUiBinderImpl_GenBundle get_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      return build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay();
    }
    private com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid_MyUiBinderImpl_GenBundle build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      // Creation section.
      final com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid_MyUiBinderImpl_GenBundle clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay = (com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid_MyUiBinderImpl_GenBundle) GWT.create(com.projectmas.gwt.reelvision.client.ui.component.CustomDataGrid_MyUiBinderImpl_GenBundle.class);
      // Setup section.

      return clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay;
    }

    /**
     * Getter for f_Div1 called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private org.gwtbootstrap3.client.ui.html.Div get_f_Div1() {
      return build_f_Div1();
    }
    private org.gwtbootstrap3.client.ui.html.Div build_f_Div1() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.html.Div f_Div1 = (org.gwtbootstrap3.client.ui.html.Div) GWT.create(org.gwtbootstrap3.client.ui.html.Div.class);
      // Setup section.
      f_Div1.add(get_panel());
      f_Div1.addStyleName("table-responsive");

      return f_Div1;
    }

    /**
     * Getter for panel called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private com.google.gwt.user.client.ui.SimpleLayoutPanel get_panel() {
      return build_panel();
    }
    private com.google.gwt.user.client.ui.SimpleLayoutPanel build_panel() {
      // Creation section.
      final com.google.gwt.user.client.ui.SimpleLayoutPanel panel = (com.google.gwt.user.client.ui.SimpleLayoutPanel) GWT.create(com.google.gwt.user.client.ui.SimpleLayoutPanel.class);
      // Setup section.
      panel.add(get_dataGrid());

      this.owner.panel = panel;

      return panel;
    }

    /**
     * Getter for dataGrid called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.gwt.DataGrid get_dataGrid() {
      return build_dataGrid();
    }
    private org.gwtbootstrap3.client.ui.gwt.DataGrid build_dataGrid() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.gwt.DataGrid dataGrid = (org.gwtbootstrap3.client.ui.gwt.DataGrid) GWT.create(org.gwtbootstrap3.client.ui.gwt.DataGrid.class);
      // Setup section.

      this.owner.dataGrid = dataGrid;

      return dataGrid;
    }
  }
}
