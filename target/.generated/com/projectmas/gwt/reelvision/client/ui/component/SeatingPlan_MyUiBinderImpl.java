// .ui.xml template last modified: 1465812125087
package com.projectmas.gwt.reelvision.client.ui.component;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiBinderUtil;
import com.google.gwt.user.client.ui.Widget;

public class SeatingPlan_MyUiBinderImpl implements UiBinder<com.google.gwt.user.client.ui.Widget, com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan>, com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan.MyUiBinder {


  public com.google.gwt.user.client.ui.Widget createAndBindUi(final com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan owner) {


    return new Widgets(owner).get_f_Div1();
  }

  /**
   * Encapsulates the access to all inner widgets
   */
  class Widgets {
    private final com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan owner;


    public Widgets(final com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan owner) {
      this.owner = owner;
    }


    /**
     * Getter for clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay called 0 times. Type: GENERATED_BUNDLE. Build precedence: 1.
     */
    private com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan_MyUiBinderImpl_GenBundle get_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      return build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay();
    }
    private com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan_MyUiBinderImpl_GenBundle build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      // Creation section.
      final com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan_MyUiBinderImpl_GenBundle clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay = (com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan_MyUiBinderImpl_GenBundle) GWT.create(com.projectmas.gwt.reelvision.client.ui.component.SeatingPlan_MyUiBinderImpl_GenBundle.class);
      // Setup section.

      return clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay;
    }

    /**
     * Getter for resources called 1 times. Type: IMPORTED. Build precedence: 1.
     */
    private com.projectmas.gwt.reelvision.client.resource.Resources get_resources() {
      return build_resources();
    }
    private com.projectmas.gwt.reelvision.client.resource.Resources build_resources() {
      // Creation section.
      final com.projectmas.gwt.reelvision.client.resource.Resources resources = (com.projectmas.gwt.reelvision.client.resource.Resources) GWT.create(com.projectmas.gwt.reelvision.client.resource.Resources.class);
      // Setup section.

      this.owner.resources = resources;

      return resources;
    }

    /**
     * Getter for f_Div1 called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private org.gwtbootstrap3.client.ui.html.Div get_f_Div1() {
      return build_f_Div1();
    }
    private org.gwtbootstrap3.client.ui.html.Div build_f_Div1() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.html.Div f_Div1 = (org.gwtbootstrap3.client.ui.html.Div) GWT.create(org.gwtbootstrap3.client.ui.html.Div.class);
      // Setup section.
      f_Div1.add(get_seatingPlanGrid());
      f_Div1.addStyleName("table-responsive");

      return f_Div1;
    }

    /**
     * Getter for seatingPlanGrid called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private com.google.gwt.user.client.ui.Grid get_seatingPlanGrid() {
      return build_seatingPlanGrid();
    }
    private com.google.gwt.user.client.ui.Grid build_seatingPlanGrid() {
      // Creation section.
      final com.google.gwt.user.client.ui.Grid seatingPlanGrid = (com.google.gwt.user.client.ui.Grid) GWT.create(com.google.gwt.user.client.ui.Grid.class);
      // Setup section.
      seatingPlanGrid.addStyleName("" + get_resources().getStyle().seatPlan() + "");

      this.owner.seatingPlanGrid = seatingPlanGrid;

      return seatingPlanGrid;
    }
  }
}
