// .ui.xml template last modified: 1464625208025
package com.projectmas.gwt.reelvision.client.ui.entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.safehtml.shared.UriUtils;
import com.google.gwt.uibinder.client.UiBinderUtil;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiBinderUtil;
import com.google.gwt.user.client.ui.Widget;

public class EntryTemplate_MyUiBinderImpl implements UiBinder<com.google.gwt.user.client.ui.Widget, com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate>, com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate.MyUiBinder {

  interface Template extends SafeHtmlTemplates {
    @Template("<span id='{0}'></span> <span id='{1}'></span>")
    SafeHtml html1(String arg0, String arg1);
     
  }

  Template template = GWT.create(Template.class);


  public com.google.gwt.user.client.ui.Widget createAndBindUi(final com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate owner) {


    return new Widgets(owner).get_wrapper();
  }

  /**
   * Encapsulates the access to all inner widgets
   */
  class Widgets {
    private final com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate owner;


    public Widgets(final com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate owner) {
      this.owner = owner;
      build_resources();  // more than one getter call detected. Type: IMPORTED, precedence: 1
      build_domId0();  // more than one getter call detected. Type: DOM_ID_HOLDER, precedence: 2
      build_domId1();  // more than one getter call detected. Type: DOM_ID_HOLDER, precedence: 2
      build_domId0Element();  // more than one getter call detected. Type: DEFAULT, precedence: 2
      build_domId1Element();  // more than one getter call detected. Type: DEFAULT, precedence: 2
    }

    SafeHtml template_html1() {
      return template.html1(get_domId0(), get_domId1());
    }

    /**
     * Getter for clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay called 0 times. Type: GENERATED_BUNDLE. Build precedence: 1.
     */
    private com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate_MyUiBinderImpl_GenBundle get_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      return build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay();
    }
    private com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate_MyUiBinderImpl_GenBundle build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      // Creation section.
      final com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate_MyUiBinderImpl_GenBundle clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay = (com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate_MyUiBinderImpl_GenBundle) GWT.create(com.projectmas.gwt.reelvision.client.ui.entry.EntryTemplate_MyUiBinderImpl_GenBundle.class);
      // Setup section.

      return clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay;
    }

    /**
     * Getter for COOKIE_EXPIRY called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.Long get_COOKIE_EXPIRY() {
      return build_COOKIE_EXPIRY();
    }
    private java.lang.Long build_COOKIE_EXPIRY() {
      // Creation section.
      final java.lang.Long COOKIE_EXPIRY = com.projectmas.gwt.reelvision.shared.Constants.COOKIE_EXPIRY;
      // Setup section.

      return COOKIE_EXPIRY;
    }

    /**
     * Getter for HASH called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_HASH() {
      return build_HASH();
    }
    private java.lang.String build_HASH() {
      // Creation section.
      final java.lang.String HASH = com.projectmas.gwt.reelvision.shared.Constants.HASH;
      // Setup section.

      return HASH;
    }

    /**
     * Getter for EMPTY_STRING called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_EMPTY_STRING() {
      return build_EMPTY_STRING();
    }
    private java.lang.String build_EMPTY_STRING() {
      // Creation section.
      final java.lang.String EMPTY_STRING = com.projectmas.gwt.reelvision.shared.Constants.EMPTY_STRING;
      // Setup section.

      return EMPTY_STRING;
    }

    /**
     * Getter for PX called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PX() {
      return build_PX();
    }
    private java.lang.String build_PX() {
      // Creation section.
      final java.lang.String PX = com.projectmas.gwt.reelvision.shared.Constants.PX;
      // Setup section.

      return PX;
    }

    /**
     * Getter for DASH called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DASH() {
      return build_DASH();
    }
    private java.lang.String build_DASH() {
      // Creation section.
      final java.lang.String DASH = com.projectmas.gwt.reelvision.shared.Constants.DASH;
      // Setup section.

      return DASH;
    }

    /**
     * Getter for YYYY_MM_DD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_YYYY_MM_DD() {
      return build_YYYY_MM_DD();
    }
    private java.lang.String build_YYYY_MM_DD() {
      // Creation section.
      final java.lang.String YYYY_MM_DD = com.projectmas.gwt.reelvision.shared.Constants.YYYY_MM_DD;
      // Setup section.

      return YYYY_MM_DD;
    }

    /**
     * Getter for YYYY_MM_DD_HH_MM_SS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_YYYY_MM_DD_HH_MM_SS() {
      return build_YYYY_MM_DD_HH_MM_SS();
    }
    private java.lang.String build_YYYY_MM_DD_HH_MM_SS() {
      // Creation section.
      final java.lang.String YYYY_MM_DD_HH_MM_SS = com.projectmas.gwt.reelvision.shared.Constants.YYYY_MM_DD_HH_MM_SS;
      // Setup section.

      return YYYY_MM_DD_HH_MM_SS;
    }

    /**
     * Getter for CHOOSE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CHOOSE() {
      return build_CHOOSE();
    }
    private java.lang.String build_CHOOSE() {
      // Creation section.
      final java.lang.String CHOOSE = com.projectmas.gwt.reelvision.shared.Constants.CHOOSE;
      // Setup section.

      return CHOOSE;
    }

    /**
     * Getter for TODAY called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_TODAY() {
      return build_TODAY();
    }
    private java.lang.String build_TODAY() {
      // Creation section.
      final java.lang.String TODAY = com.projectmas.gwt.reelvision.shared.Constants.TODAY;
      // Setup section.

      return TODAY;
    }

    /**
     * Getter for LOG_IN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOG_IN() {
      return build_LOG_IN();
    }
    private java.lang.String build_LOG_IN() {
      // Creation section.
      final java.lang.String LOG_IN = com.projectmas.gwt.reelvision.shared.Constants.LOG_IN;
      // Setup section.

      return LOG_IN;
    }

    /**
     * Getter for LOG_OUT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOG_OUT() {
      return build_LOG_OUT();
    }
    private java.lang.String build_LOG_OUT() {
      // Creation section.
      final java.lang.String LOG_OUT = com.projectmas.gwt.reelvision.shared.Constants.LOG_OUT;
      // Setup section.

      return LOG_OUT;
    }

    /**
     * Getter for LOGIN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOGIN() {
      return build_LOGIN();
    }
    private java.lang.String build_LOGIN() {
      // Creation section.
      final java.lang.String LOGIN = com.projectmas.gwt.reelvision.shared.Constants.LOGIN;
      // Setup section.

      return LOGIN;
    }

    /**
     * Getter for PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PASSWORD() {
      return build_PASSWORD();
    }
    private java.lang.String build_PASSWORD() {
      // Creation section.
      final java.lang.String PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.PASSWORD;
      // Setup section.

      return PASSWORD;
    }

    /**
     * Getter for ENTER_LOGIN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ENTER_LOGIN() {
      return build_ENTER_LOGIN();
    }
    private java.lang.String build_ENTER_LOGIN() {
      // Creation section.
      final java.lang.String ENTER_LOGIN = com.projectmas.gwt.reelvision.shared.Constants.ENTER_LOGIN;
      // Setup section.

      return ENTER_LOGIN;
    }

    /**
     * Getter for ENTER_PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ENTER_PASSWORD() {
      return build_ENTER_PASSWORD();
    }
    private java.lang.String build_ENTER_PASSWORD() {
      // Creation section.
      final java.lang.String ENTER_PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.ENTER_PASSWORD;
      // Setup section.

      return ENTER_PASSWORD;
    }

    /**
     * Getter for SETTINGS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SETTINGS() {
      return build_SETTINGS();
    }
    private java.lang.String build_SETTINGS() {
      // Creation section.
      final java.lang.String SETTINGS = com.projectmas.gwt.reelvision.shared.Constants.SETTINGS;
      // Setup section.

      return SETTINGS;
    }

    /**
     * Getter for WRONG_PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_WRONG_PASSWORD() {
      return build_WRONG_PASSWORD();
    }
    private java.lang.String build_WRONG_PASSWORD() {
      // Creation section.
      final java.lang.String WRONG_PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.WRONG_PASSWORD;
      // Setup section.

      return WRONG_PASSWORD;
    }

    /**
     * Getter for DASHBOARD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DASHBOARD() {
      return build_DASHBOARD();
    }
    private java.lang.String build_DASHBOARD() {
      // Creation section.
      final java.lang.String DASHBOARD = com.projectmas.gwt.reelvision.shared.Constants.DASHBOARD;
      // Setup section.

      return DASHBOARD;
    }

    /**
     * Getter for RESERVATIONS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_RESERVATIONS() {
      return build_RESERVATIONS();
    }
    private java.lang.String build_RESERVATIONS() {
      // Creation section.
      final java.lang.String RESERVATIONS = com.projectmas.gwt.reelvision.shared.Constants.RESERVATIONS;
      // Setup section.

      return RESERVATIONS;
    }

    /**
     * Getter for SALE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SALE() {
      return build_SALE();
    }
    private java.lang.String build_SALE() {
      // Creation section.
      final java.lang.String SALE = com.projectmas.gwt.reelvision.shared.Constants.SALE;
      // Setup section.

      return SALE;
    }

    /**
     * Getter for AUDIENCES called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_AUDIENCES() {
      return build_AUDIENCES();
    }
    private java.lang.String build_AUDIENCES() {
      // Creation section.
      final java.lang.String AUDIENCES = com.projectmas.gwt.reelvision.shared.Constants.AUDIENCES;
      // Setup section.

      return AUDIENCES;
    }

    /**
     * Getter for SHOWTIMES called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIMES() {
      return build_SHOWTIMES();
    }
    private java.lang.String build_SHOWTIMES() {
      // Creation section.
      final java.lang.String SHOWTIMES = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIMES;
      // Setup section.

      return SHOWTIMES;
    }

    /**
     * Getter for MOVIES called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIES() {
      return build_MOVIES();
    }
    private java.lang.String build_MOVIES() {
      // Creation section.
      final java.lang.String MOVIES = com.projectmas.gwt.reelvision.shared.Constants.MOVIES;
      // Setup section.

      return MOVIES;
    }

    /**
     * Getter for SHOWTIME_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIME_MANAGEMENT() {
      return build_SHOWTIME_MANAGEMENT();
    }
    private java.lang.String build_SHOWTIME_MANAGEMENT() {
      // Creation section.
      final java.lang.String SHOWTIME_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIME_MANAGEMENT;
      // Setup section.

      return SHOWTIME_MANAGEMENT;
    }

    /**
     * Getter for MOVIE_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_MANAGEMENT() {
      return build_MOVIE_MANAGEMENT();
    }
    private java.lang.String build_MOVIE_MANAGEMENT() {
      // Creation section.
      final java.lang.String MOVIE_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_MANAGEMENT;
      // Setup section.

      return MOVIE_MANAGEMENT;
    }

    /**
     * Getter for USER_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_MANAGEMENT() {
      return build_USER_MANAGEMENT();
    }
    private java.lang.String build_USER_MANAGEMENT() {
      // Creation section.
      final java.lang.String USER_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.USER_MANAGEMENT;
      // Setup section.

      return USER_MANAGEMENT;
    }

    /**
     * Getter for CUSTOMERS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CUSTOMERS() {
      return build_CUSTOMERS();
    }
    private java.lang.String build_CUSTOMERS() {
      // Creation section.
      final java.lang.String CUSTOMERS = com.projectmas.gwt.reelvision.shared.Constants.CUSTOMERS;
      // Setup section.

      return CUSTOMERS;
    }

    /**
     * Getter for REPORTS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPORTS() {
      return build_REPORTS();
    }
    private java.lang.String build_REPORTS() {
      // Creation section.
      final java.lang.String REPORTS = com.projectmas.gwt.reelvision.shared.Constants.REPORTS;
      // Setup section.

      return REPORTS;
    }

    /**
     * Getter for NEW called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_NEW() {
      return build_NEW();
    }
    private java.lang.String build_NEW() {
      // Creation section.
      final java.lang.String NEW = com.projectmas.gwt.reelvision.shared.Constants.NEW;
      // Setup section.

      return NEW;
    }

    /**
     * Getter for FIND called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_FIND() {
      return build_FIND();
    }
    private java.lang.String build_FIND() {
      // Creation section.
      final java.lang.String FIND = com.projectmas.gwt.reelvision.shared.Constants.FIND;
      // Setup section.

      return FIND;
    }

    /**
     * Getter for REPERTOIRE_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPERTOIRE_MANAGEMENT() {
      return build_REPERTOIRE_MANAGEMENT();
    }
    private java.lang.String build_REPERTOIRE_MANAGEMENT() {
      // Creation section.
      final java.lang.String REPERTOIRE_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.REPERTOIRE_MANAGEMENT;
      // Setup section.

      return REPERTOIRE_MANAGEMENT;
    }

    /**
     * Getter for PRIVCLASS_OBTAIN_PROBLEM called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PRIVCLASS_OBTAIN_PROBLEM() {
      return build_PRIVCLASS_OBTAIN_PROBLEM();
    }
    private java.lang.String build_PRIVCLASS_OBTAIN_PROBLEM() {
      // Creation section.
      final java.lang.String PRIVCLASS_OBTAIN_PROBLEM = com.projectmas.gwt.reelvision.shared.Constants.PRIVCLASS_OBTAIN_PROBLEM;
      // Setup section.

      return PRIVCLASS_OBTAIN_PROBLEM;
    }

    /**
     * Getter for LOG_OUT_PROBLEM called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOG_OUT_PROBLEM() {
      return build_LOG_OUT_PROBLEM();
    }
    private java.lang.String build_LOG_OUT_PROBLEM() {
      // Creation section.
      final java.lang.String LOG_OUT_PROBLEM = com.projectmas.gwt.reelvision.shared.Constants.LOG_OUT_PROBLEM;
      // Setup section.

      return LOG_OUT_PROBLEM;
    }

    /**
     * Getter for UNEXPECTED_ERROR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_UNEXPECTED_ERROR() {
      return build_UNEXPECTED_ERROR();
    }
    private java.lang.String build_UNEXPECTED_ERROR() {
      // Creation section.
      final java.lang.String UNEXPECTED_ERROR = com.projectmas.gwt.reelvision.shared.Constants.UNEXPECTED_ERROR;
      // Setup section.

      return UNEXPECTED_ERROR;
    }

    /**
     * Getter for PRIVILAGE_CLASS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PRIVILAGE_CLASS() {
      return build_PRIVILAGE_CLASS();
    }
    private java.lang.String build_PRIVILAGE_CLASS() {
      // Creation section.
      final java.lang.String PRIVILAGE_CLASS = com.projectmas.gwt.reelvision.shared.Constants.PRIVILAGE_CLASS;
      // Setup section.

      return PRIVILAGE_CLASS;
    }

    /**
     * Getter for ADMINISTRATOR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADMINISTRATOR() {
      return build_ADMINISTRATOR();
    }
    private java.lang.String build_ADMINISTRATOR() {
      // Creation section.
      final java.lang.String ADMINISTRATOR = com.projectmas.gwt.reelvision.shared.Constants.ADMINISTRATOR;
      // Setup section.

      return ADMINISTRATOR;
    }

    /**
     * Getter for MANAGER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MANAGER() {
      return build_MANAGER();
    }
    private java.lang.String build_MANAGER() {
      // Creation section.
      final java.lang.String MANAGER = com.projectmas.gwt.reelvision.shared.Constants.MANAGER;
      // Setup section.

      return MANAGER;
    }

    /**
     * Getter for USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER() {
      return build_USER();
    }
    private java.lang.String build_USER() {
      // Creation section.
      final java.lang.String USER = com.projectmas.gwt.reelvision.shared.Constants.USER;
      // Setup section.

      return USER;
    }

    /**
     * Getter for CONFIRM_PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CONFIRM_PASSWORD() {
      return build_CONFIRM_PASSWORD();
    }
    private java.lang.String build_CONFIRM_PASSWORD() {
      // Creation section.
      final java.lang.String CONFIRM_PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.CONFIRM_PASSWORD;
      // Setup section.

      return CONFIRM_PASSWORD;
    }

    /**
     * Getter for USERS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USERS() {
      return build_USERS();
    }
    private java.lang.String build_USERS() {
      // Creation section.
      final java.lang.String USERS = com.projectmas.gwt.reelvision.shared.Constants.USERS;
      // Setup section.

      return USERS;
    }

    /**
     * Getter for ENTER_PASSWORD_CONFIRMATION called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ENTER_PASSWORD_CONFIRMATION() {
      return build_ENTER_PASSWORD_CONFIRMATION();
    }
    private java.lang.String build_ENTER_PASSWORD_CONFIRMATION() {
      // Creation section.
      final java.lang.String ENTER_PASSWORD_CONFIRMATION = com.projectmas.gwt.reelvision.shared.Constants.ENTER_PASSWORD_CONFIRMATION;
      // Setup section.

      return ENTER_PASSWORD_CONFIRMATION;
    }

    /**
     * Getter for ADD_USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_USER() {
      return build_ADD_USER();
    }
    private java.lang.String build_ADD_USER() {
      // Creation section.
      final java.lang.String ADD_USER = com.projectmas.gwt.reelvision.shared.Constants.ADD_USER;
      // Setup section.

      return ADD_USER;
    }

    /**
     * Getter for DELETE_USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DELETE_USER() {
      return build_DELETE_USER();
    }
    private java.lang.String build_DELETE_USER() {
      // Creation section.
      final java.lang.String DELETE_USER = com.projectmas.gwt.reelvision.shared.Constants.DELETE_USER;
      // Setup section.

      return DELETE_USER;
    }

    /**
     * Getter for CREATED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CREATED() {
      return build_CREATED();
    }
    private java.lang.String build_CREATED() {
      // Creation section.
      final java.lang.String CREATED = com.projectmas.gwt.reelvision.shared.Constants.CREATED;
      // Setup section.

      return CREATED;
    }

    /**
     * Getter for UPDATED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_UPDATED() {
      return build_UPDATED();
    }
    private java.lang.String build_UPDATED() {
      // Creation section.
      final java.lang.String UPDATED = com.projectmas.gwt.reelvision.shared.Constants.UPDATED;
      // Setup section.

      return UPDATED;
    }

    /**
     * Getter for PRIVILAGE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PRIVILAGE() {
      return build_PRIVILAGE();
    }
    private java.lang.String build_PRIVILAGE() {
      // Creation section.
      final java.lang.String PRIVILAGE = com.projectmas.gwt.reelvision.shared.Constants.PRIVILAGE;
      // Setup section.

      return PRIVILAGE;
    }

    /**
     * Getter for LAST_LOGIN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LAST_LOGIN() {
      return build_LAST_LOGIN();
    }
    private java.lang.String build_LAST_LOGIN() {
      // Creation section.
      final java.lang.String LAST_LOGIN = com.projectmas.gwt.reelvision.shared.Constants.LAST_LOGIN;
      // Setup section.

      return LAST_LOGIN;
    }

    /**
     * Getter for USER_EXISTS_ALREADY_EXISTS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_EXISTS_ALREADY_EXISTS() {
      return build_USER_EXISTS_ALREADY_EXISTS();
    }
    private java.lang.String build_USER_EXISTS_ALREADY_EXISTS() {
      // Creation section.
      final java.lang.String USER_EXISTS_ALREADY_EXISTS = com.projectmas.gwt.reelvision.shared.Constants.USER_EXISTS_ALREADY_EXISTS;
      // Setup section.

      return USER_EXISTS_ALREADY_EXISTS;
    }

    /**
     * Getter for USER_ADDED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_ADDED() {
      return build_USER_ADDED();
    }
    private java.lang.String build_USER_ADDED() {
      // Creation section.
      final java.lang.String USER_ADDED = com.projectmas.gwt.reelvision.shared.Constants.USER_ADDED;
      // Setup section.

      return USER_ADDED;
    }

    /**
     * Getter for USER_DELETED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_DELETED() {
      return build_USER_DELETED();
    }
    private java.lang.String build_USER_DELETED() {
      // Creation section.
      final java.lang.String USER_DELETED = com.projectmas.gwt.reelvision.shared.Constants.USER_DELETED;
      // Setup section.

      return USER_DELETED;
    }

    /**
     * Getter for PROVIDED_PASSWORDS_ARE_DIFFERENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PROVIDED_PASSWORDS_ARE_DIFFERENT() {
      return build_PROVIDED_PASSWORDS_ARE_DIFFERENT();
    }
    private java.lang.String build_PROVIDED_PASSWORDS_ARE_DIFFERENT() {
      // Creation section.
      final java.lang.String PROVIDED_PASSWORDS_ARE_DIFFERENT = com.projectmas.gwt.reelvision.shared.Constants.PROVIDED_PASSWORDS_ARE_DIFFERENT;
      // Setup section.

      return PROVIDED_PASSWORDS_ARE_DIFFERENT;
    }

    /**
     * Getter for CANNOT_DELETE_CURRENT_LOGGED_IN_USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CANNOT_DELETE_CURRENT_LOGGED_IN_USER() {
      return build_CANNOT_DELETE_CURRENT_LOGGED_IN_USER();
    }
    private java.lang.String build_CANNOT_DELETE_CURRENT_LOGGED_IN_USER() {
      // Creation section.
      final java.lang.String CANNOT_DELETE_CURRENT_LOGGED_IN_USER = com.projectmas.gwt.reelvision.shared.Constants.CANNOT_DELETE_CURRENT_LOGGED_IN_USER;
      // Setup section.

      return CANNOT_DELETE_CURRENT_LOGGED_IN_USER;
    }

    /**
     * Getter for ADD_OR_EDIT_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_OR_EDIT_MOVIE() {
      return build_ADD_OR_EDIT_MOVIE();
    }
    private java.lang.String build_ADD_OR_EDIT_MOVIE() {
      // Creation section.
      final java.lang.String ADD_OR_EDIT_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.ADD_OR_EDIT_MOVIE;
      // Setup section.

      return ADD_OR_EDIT_MOVIE;
    }

    /**
     * Getter for DIRECTOR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DIRECTOR() {
      return build_DIRECTOR();
    }
    private java.lang.String build_DIRECTOR() {
      // Creation section.
      final java.lang.String DIRECTOR = com.projectmas.gwt.reelvision.shared.Constants.DIRECTOR;
      // Setup section.

      return DIRECTOR;
    }

    /**
     * Getter for TITLE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_TITLE() {
      return build_TITLE();
    }
    private java.lang.String build_TITLE() {
      // Creation section.
      final java.lang.String TITLE = com.projectmas.gwt.reelvision.shared.Constants.TITLE;
      // Setup section.

      return TITLE;
    }

    /**
     * Getter for RELEASE_DATE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_RELEASE_DATE() {
      return build_RELEASE_DATE();
    }
    private java.lang.String build_RELEASE_DATE() {
      // Creation section.
      final java.lang.String RELEASE_DATE = com.projectmas.gwt.reelvision.shared.Constants.RELEASE_DATE;
      // Setup section.

      return RELEASE_DATE;
    }

    /**
     * Getter for DESCRIPTION called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DESCRIPTION() {
      return build_DESCRIPTION();
    }
    private java.lang.String build_DESCRIPTION() {
      // Creation section.
      final java.lang.String DESCRIPTION = com.projectmas.gwt.reelvision.shared.Constants.DESCRIPTION;
      // Setup section.

      return DESCRIPTION;
    }

    /**
     * Getter for COUNTRY called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_COUNTRY() {
      return build_COUNTRY();
    }
    private java.lang.String build_COUNTRY() {
      // Creation section.
      final java.lang.String COUNTRY = com.projectmas.gwt.reelvision.shared.Constants.COUNTRY;
      // Setup section.

      return COUNTRY;
    }

    /**
     * Getter for ADD_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_MOVIE() {
      return build_ADD_MOVIE();
    }
    private java.lang.String build_ADD_MOVIE() {
      // Creation section.
      final java.lang.String ADD_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.ADD_MOVIE;
      // Setup section.

      return ADD_MOVIE;
    }

    /**
     * Getter for EDIT_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_EDIT_MOVIE() {
      return build_EDIT_MOVIE();
    }
    private java.lang.String build_EDIT_MOVIE() {
      // Creation section.
      final java.lang.String EDIT_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.EDIT_MOVIE;
      // Setup section.

      return EDIT_MOVIE;
    }

    /**
     * Getter for REMOVE_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REMOVE_MOVIE() {
      return build_REMOVE_MOVIE();
    }
    private java.lang.String build_REMOVE_MOVIE() {
      // Creation section.
      final java.lang.String REMOVE_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.REMOVE_MOVIE;
      // Setup section.

      return REMOVE_MOVIE;
    }

    /**
     * Getter for MOVIE_ADDED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_ADDED() {
      return build_MOVIE_ADDED();
    }
    private java.lang.String build_MOVIE_ADDED() {
      // Creation section.
      final java.lang.String MOVIE_ADDED = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_ADDED;
      // Setup section.

      return MOVIE_ADDED;
    }

    /**
     * Getter for MOVIE_DELETED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_DELETED() {
      return build_MOVIE_DELETED();
    }
    private java.lang.String build_MOVIE_DELETED() {
      // Creation section.
      final java.lang.String MOVIE_DELETED = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_DELETED;
      // Setup section.

      return MOVIE_DELETED;
    }

    /**
     * Getter for MOVIE_EDITED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_EDITED() {
      return build_MOVIE_EDITED();
    }
    private java.lang.String build_MOVIE_EDITED() {
      // Creation section.
      final java.lang.String MOVIE_EDITED = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_EDITED;
      // Setup section.

      return MOVIE_EDITED;
    }

    /**
     * Getter for PLEASE_SELECT_MOVIE_TO_DELETE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PLEASE_SELECT_MOVIE_TO_DELETE() {
      return build_PLEASE_SELECT_MOVIE_TO_DELETE();
    }
    private java.lang.String build_PLEASE_SELECT_MOVIE_TO_DELETE() {
      // Creation section.
      final java.lang.String PLEASE_SELECT_MOVIE_TO_DELETE = com.projectmas.gwt.reelvision.shared.Constants.PLEASE_SELECT_MOVIE_TO_DELETE;
      // Setup section.

      return PLEASE_SELECT_MOVIE_TO_DELETE;
    }

    /**
     * Getter for PLEASE_SELECT_MOVIE_TO_EDIT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PLEASE_SELECT_MOVIE_TO_EDIT() {
      return build_PLEASE_SELECT_MOVIE_TO_EDIT();
    }
    private java.lang.String build_PLEASE_SELECT_MOVIE_TO_EDIT() {
      // Creation section.
      final java.lang.String PLEASE_SELECT_MOVIE_TO_EDIT = com.projectmas.gwt.reelvision.shared.Constants.PLEASE_SELECT_MOVIE_TO_EDIT;
      // Setup section.

      return PLEASE_SELECT_MOVIE_TO_EDIT;
    }

    /**
     * Getter for ADD_OR_EDIT_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_OR_EDIT_REPERTOIRE() {
      return build_ADD_OR_EDIT_REPERTOIRE();
    }
    private java.lang.String build_ADD_OR_EDIT_REPERTOIRE() {
      // Creation section.
      final java.lang.String ADD_OR_EDIT_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.ADD_OR_EDIT_REPERTOIRE;
      // Setup section.

      return ADD_OR_EDIT_REPERTOIRE;
    }

    /**
     * Getter for REPERTOIRES called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPERTOIRES() {
      return build_REPERTOIRES();
    }
    private java.lang.String build_REPERTOIRES() {
      // Creation section.
      final java.lang.String REPERTOIRES = com.projectmas.gwt.reelvision.shared.Constants.REPERTOIRES;
      // Setup section.

      return REPERTOIRES;
    }

    /**
     * Getter for ADD_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_REPERTOIRE() {
      return build_ADD_REPERTOIRE();
    }
    private java.lang.String build_ADD_REPERTOIRE() {
      // Creation section.
      final java.lang.String ADD_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.ADD_REPERTOIRE;
      // Setup section.

      return ADD_REPERTOIRE;
    }

    /**
     * Getter for EDIT_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_EDIT_REPERTOIRE() {
      return build_EDIT_REPERTOIRE();
    }
    private java.lang.String build_EDIT_REPERTOIRE() {
      // Creation section.
      final java.lang.String EDIT_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.EDIT_REPERTOIRE;
      // Setup section.

      return EDIT_REPERTOIRE;
    }

    /**
     * Getter for REMOVE_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REMOVE_REPERTOIRE() {
      return build_REMOVE_REPERTOIRE();
    }
    private java.lang.String build_REMOVE_REPERTOIRE() {
      // Creation section.
      final java.lang.String REMOVE_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.REMOVE_REPERTOIRE;
      // Setup section.

      return REMOVE_REPERTOIRE;
    }

    /**
     * Getter for START_DATE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_START_DATE() {
      return build_START_DATE();
    }
    private java.lang.String build_START_DATE() {
      // Creation section.
      final java.lang.String START_DATE = com.projectmas.gwt.reelvision.shared.Constants.START_DATE;
      // Setup section.

      return START_DATE;
    }

    /**
     * Getter for END_DATE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_END_DATE() {
      return build_END_DATE();
    }
    private java.lang.String build_END_DATE() {
      // Creation section.
      final java.lang.String END_DATE = com.projectmas.gwt.reelvision.shared.Constants.END_DATE;
      // Setup section.

      return END_DATE;
    }

    /**
     * Getter for NEW_SALE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_NEW_SALE_ANCHOR_ID() {
      return build_NEW_SALE_ANCHOR_ID();
    }
    private java.lang.String build_NEW_SALE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String NEW_SALE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.NEW_SALE_ANCHOR_ID;
      // Setup section.

      return NEW_SALE_ANCHOR_ID;
    }

    /**
     * Getter for SEARCH_SALE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SEARCH_SALE_ANCHOR_ID() {
      return build_SEARCH_SALE_ANCHOR_ID();
    }
    private java.lang.String build_SEARCH_SALE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String SEARCH_SALE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.SEARCH_SALE_ANCHOR_ID;
      // Setup section.

      return SEARCH_SALE_ANCHOR_ID;
    }

    /**
     * Getter for NEW_RESERVE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_NEW_RESERVE_ANCHOR_ID() {
      return build_NEW_RESERVE_ANCHOR_ID();
    }
    private java.lang.String build_NEW_RESERVE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String NEW_RESERVE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.NEW_RESERVE_ANCHOR_ID;
      // Setup section.

      return NEW_RESERVE_ANCHOR_ID;
    }

    /**
     * Getter for SEARCH_RESERVE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SEARCH_RESERVE_ANCHOR_ID() {
      return build_SEARCH_RESERVE_ANCHOR_ID();
    }
    private java.lang.String build_SEARCH_RESERVE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String SEARCH_RESERVE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.SEARCH_RESERVE_ANCHOR_ID;
      // Setup section.

      return SEARCH_RESERVE_ANCHOR_ID;
    }

    /**
     * Getter for DASHBOARD_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DASHBOARD_ANCHOR_ID() {
      return build_DASHBOARD_ANCHOR_ID();
    }
    private java.lang.String build_DASHBOARD_ANCHOR_ID() {
      // Creation section.
      final java.lang.String DASHBOARD_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.DASHBOARD_ANCHOR_ID;
      // Setup section.

      return DASHBOARD_ANCHOR_ID;
    }

    /**
     * Getter for SHOWTIME_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIME_ANCHOR_ID() {
      return build_SHOWTIME_ANCHOR_ID();
    }
    private java.lang.String build_SHOWTIME_ANCHOR_ID() {
      // Creation section.
      final java.lang.String SHOWTIME_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIME_ANCHOR_ID;
      // Setup section.

      return SHOWTIME_ANCHOR_ID;
    }

    /**
     * Getter for MOVIES_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIES_ANCHOR_ID() {
      return build_MOVIES_ANCHOR_ID();
    }
    private java.lang.String build_MOVIES_ANCHOR_ID() {
      // Creation section.
      final java.lang.String MOVIES_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.MOVIES_ANCHOR_ID;
      // Setup section.

      return MOVIES_ANCHOR_ID;
    }

    /**
     * Getter for AUDIENCES_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_AUDIENCES_ANCHOR_ID() {
      return build_AUDIENCES_ANCHOR_ID();
    }
    private java.lang.String build_AUDIENCES_ANCHOR_ID() {
      // Creation section.
      final java.lang.String AUDIENCES_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.AUDIENCES_ANCHOR_ID;
      // Setup section.

      return AUDIENCES_ANCHOR_ID;
    }

    /**
     * Getter for REPORTS_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPORTS_ANCHOR_ID() {
      return build_REPORTS_ANCHOR_ID();
    }
    private java.lang.String build_REPORTS_ANCHOR_ID() {
      // Creation section.
      final java.lang.String REPORTS_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.REPORTS_ANCHOR_ID;
      // Setup section.

      return REPORTS_ANCHOR_ID;
    }

    /**
     * Getter for SHOWTIME_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIME_MANAGEMENT_ID() {
      return build_SHOWTIME_MANAGEMENT_ID();
    }
    private java.lang.String build_SHOWTIME_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String SHOWTIME_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIME_MANAGEMENT_ID;
      // Setup section.

      return SHOWTIME_MANAGEMENT_ID;
    }

    /**
     * Getter for MOVIE_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_MANAGEMENT_ID() {
      return build_MOVIE_MANAGEMENT_ID();
    }
    private java.lang.String build_MOVIE_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String MOVIE_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_MANAGEMENT_ID;
      // Setup section.

      return MOVIE_MANAGEMENT_ID;
    }

    /**
     * Getter for USER_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_MANAGEMENT_ID() {
      return build_USER_MANAGEMENT_ID();
    }
    private java.lang.String build_USER_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String USER_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.USER_MANAGEMENT_ID;
      // Setup section.

      return USER_MANAGEMENT_ID;
    }

    /**
     * Getter for REPERTOIRE_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPERTOIRE_MANAGEMENT_ID() {
      return build_REPERTOIRE_MANAGEMENT_ID();
    }
    private java.lang.String build_REPERTOIRE_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String REPERTOIRE_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.REPERTOIRE_MANAGEMENT_ID;
      // Setup section.

      return REPERTOIRE_MANAGEMENT_ID;
    }

    /**
     * Getter for USER_SESSION_ATTR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_SESSION_ATTR() {
      return build_USER_SESSION_ATTR();
    }
    private java.lang.String build_USER_SESSION_ATTR() {
      // Creation section.
      final java.lang.String USER_SESSION_ATTR = com.projectmas.gwt.reelvision.shared.Constants.USER_SESSION_ATTR;
      // Setup section.

      return USER_SESSION_ATTR;
    }

    /**
     * Getter for resources called 2 times. Type: IMPORTED. Build precedence: 1.
     */
    private com.projectmas.gwt.reelvision.client.resource.Resources resources;
    private com.projectmas.gwt.reelvision.client.resource.Resources get_resources() {
      return resources;
    }
    private com.projectmas.gwt.reelvision.client.resource.Resources build_resources() {
      // Creation section.
      resources = (com.projectmas.gwt.reelvision.client.resource.Resources) GWT.create(com.projectmas.gwt.reelvision.client.resource.Resources.class);
      // Setup section.

      this.owner.resources = resources;

      return resources;
    }

    /**
     * Getter for wrapper called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private com.google.gwt.user.client.ui.HTMLPanel get_wrapper() {
      return build_wrapper();
    }
    private com.google.gwt.user.client.ui.HTMLPanel build_wrapper() {
      // Creation section.
      final com.google.gwt.user.client.ui.HTMLPanel wrapper = new com.google.gwt.user.client.ui.HTMLPanel(template_html1().asString());
      // Setup section.
      wrapper.setStyleName("" + get_resources().getStyle().wrapper() + "");

      {
        // Attach section.
        UiBinderUtil.TempAttachment __attachRecord__ = UiBinderUtil.attachToDom(wrapper.getElement());

        get_domId0Element().get();
        get_domId1Element().get();

        // Detach section.
        __attachRecord__.detach();
      }
      wrapper.addAndReplaceElement(get_navbar(), get_domId0Element().get());
      wrapper.addAndReplaceElement(get_content(), get_domId1Element().get());

      this.owner.wrapper = wrapper;

      return wrapper;
    }

    /**
     * Getter for domId0 called 2 times. Type: DOM_ID_HOLDER. Build precedence: 2.
     */
    private java.lang.String domId0;
    private java.lang.String get_domId0() {
      return domId0;
    }
    private java.lang.String build_domId0() {
      // Creation section.
      domId0 = com.google.gwt.dom.client.Document.get().createUniqueId();
      // Setup section.

      return domId0;
    }

    /**
     * Getter for navbar called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private org.gwtbootstrap3.client.ui.Navbar get_navbar() {
      return build_navbar();
    }
    private org.gwtbootstrap3.client.ui.Navbar build_navbar() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Navbar navbar = (org.gwtbootstrap3.client.ui.Navbar) GWT.create(org.gwtbootstrap3.client.ui.Navbar.class);
      // Setup section.
      navbar.add(get_f_Navbar1());
      navbar.setStyleName("navbar navbar-inverse navbar-fixed-top");

      this.owner.navbar = navbar;

      return navbar;
    }

    /**
     * Getter for f_Navbar1 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.Navbar get_f_Navbar1() {
      return build_f_Navbar1();
    }
    private org.gwtbootstrap3.client.ui.Navbar build_f_Navbar1() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Navbar f_Navbar1 = (org.gwtbootstrap3.client.ui.Navbar) GWT.create(org.gwtbootstrap3.client.ui.Navbar.class);
      // Setup section.
      f_Navbar1.add(get_f_NavbarBrand2());
      f_Navbar1.setStyleName("navbar-header");

      return f_Navbar1;
    }

    /**
     * Getter for f_NavbarBrand2 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.NavbarBrand get_f_NavbarBrand2() {
      return build_f_NavbarBrand2();
    }
    private org.gwtbootstrap3.client.ui.NavbarBrand build_f_NavbarBrand2() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.NavbarBrand f_NavbarBrand2 = (org.gwtbootstrap3.client.ui.NavbarBrand) GWT.create(org.gwtbootstrap3.client.ui.NavbarBrand.class);
      // Setup section.
      f_NavbarBrand2.setText("ReelVision");
      f_NavbarBrand2.setHref("index.html");

      return f_NavbarBrand2;
    }

    /**
     * Getter for domId0Element called 2 times. Type: DEFAULT. Build precedence: 2.
     */
    private com.google.gwt.uibinder.client.LazyDomElement domId0Element;
    private com.google.gwt.uibinder.client.LazyDomElement get_domId0Element() {
      return domId0Element;
    }
    private com.google.gwt.uibinder.client.LazyDomElement build_domId0Element() {
      // Creation section.
      domId0Element = new com.google.gwt.uibinder.client.LazyDomElement<Element>(get_domId0());
      // Setup section.

      return domId0Element;
    }

    /**
     * Getter for domId1 called 2 times. Type: DOM_ID_HOLDER. Build precedence: 2.
     */
    private java.lang.String domId1;
    private java.lang.String get_domId1() {
      return domId1;
    }
    private java.lang.String build_domId1() {
      // Creation section.
      domId1 = com.google.gwt.dom.client.Document.get().createUniqueId();
      // Setup section.

      return domId1;
    }

    /**
     * Getter for content called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private org.gwtbootstrap3.client.ui.html.Div get_content() {
      return build_content();
    }
    private org.gwtbootstrap3.client.ui.html.Div build_content() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.html.Div content = (org.gwtbootstrap3.client.ui.html.Div) GWT.create(org.gwtbootstrap3.client.ui.html.Div.class);
      // Setup section.
      content.setStyleName("" + get_resources().getStyle().pageWrapper() + "");

      this.owner.content = content;

      return content;
    }

    /**
     * Getter for domId1Element called 2 times. Type: DEFAULT. Build precedence: 2.
     */
    private com.google.gwt.uibinder.client.LazyDomElement domId1Element;
    private com.google.gwt.uibinder.client.LazyDomElement get_domId1Element() {
      return domId1Element;
    }
    private com.google.gwt.uibinder.client.LazyDomElement build_domId1Element() {
      // Creation section.
      domId1Element = new com.google.gwt.uibinder.client.LazyDomElement<Element>(get_domId1());
      // Setup section.

      return domId1Element;
    }
  }
}
