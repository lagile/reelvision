// .ui.xml template last modified: 1464416275611
package com.projectmas.gwt.reelvision.client.ui.entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiBinderUtil;
import com.google.gwt.user.client.ui.Widget;

public class UserNavigation_UserOperationListUiBinderImpl implements UiBinder<com.google.gwt.user.client.ui.Widget, com.projectmas.gwt.reelvision.client.ui.entry.UserNavigation>, com.projectmas.gwt.reelvision.client.ui.entry.UserNavigation.UserOperationListUiBinder {


  public com.google.gwt.user.client.ui.Widget createAndBindUi(final com.projectmas.gwt.reelvision.client.ui.entry.UserNavigation owner) {


    return new Widgets(owner).get_f_ListItem1();
  }

  /**
   * Encapsulates the access to all inner widgets
   */
  class Widgets {
    private final com.projectmas.gwt.reelvision.client.ui.entry.UserNavigation owner;


    public Widgets(final com.projectmas.gwt.reelvision.client.ui.entry.UserNavigation owner) {
      this.owner = owner;
      build_NEW();  // more than one getter call detected. Type: DEFAULT, precedence: 1
      build_FIND();  // more than one getter call detected. Type: DEFAULT, precedence: 1
    }


    /**
     * Getter for clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay called 0 times. Type: GENERATED_BUNDLE. Build precedence: 1.
     */
    private com.projectmas.gwt.reelvision.client.ui.entry.UserNavigation_UserOperationListUiBinderImpl_GenBundle get_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      return build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay();
    }
    private com.projectmas.gwt.reelvision.client.ui.entry.UserNavigation_UserOperationListUiBinderImpl_GenBundle build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      // Creation section.
      final com.projectmas.gwt.reelvision.client.ui.entry.UserNavigation_UserOperationListUiBinderImpl_GenBundle clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay = (com.projectmas.gwt.reelvision.client.ui.entry.UserNavigation_UserOperationListUiBinderImpl_GenBundle) GWT.create(com.projectmas.gwt.reelvision.client.ui.entry.UserNavigation_UserOperationListUiBinderImpl_GenBundle.class);
      // Setup section.

      return clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay;
    }

    /**
     * Getter for COOKIE_EXPIRY called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.Long get_COOKIE_EXPIRY() {
      return build_COOKIE_EXPIRY();
    }
    private java.lang.Long build_COOKIE_EXPIRY() {
      // Creation section.
      final java.lang.Long COOKIE_EXPIRY = com.projectmas.gwt.reelvision.shared.Constants.COOKIE_EXPIRY;
      // Setup section.

      return COOKIE_EXPIRY;
    }

    /**
     * Getter for HASH called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_HASH() {
      return build_HASH();
    }
    private java.lang.String build_HASH() {
      // Creation section.
      final java.lang.String HASH = com.projectmas.gwt.reelvision.shared.Constants.HASH;
      // Setup section.

      return HASH;
    }

    /**
     * Getter for EMPTY_STRING called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_EMPTY_STRING() {
      return build_EMPTY_STRING();
    }
    private java.lang.String build_EMPTY_STRING() {
      // Creation section.
      final java.lang.String EMPTY_STRING = com.projectmas.gwt.reelvision.shared.Constants.EMPTY_STRING;
      // Setup section.

      return EMPTY_STRING;
    }

    /**
     * Getter for PX called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PX() {
      return build_PX();
    }
    private java.lang.String build_PX() {
      // Creation section.
      final java.lang.String PX = com.projectmas.gwt.reelvision.shared.Constants.PX;
      // Setup section.

      return PX;
    }

    /**
     * Getter for DASH called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DASH() {
      return build_DASH();
    }
    private java.lang.String build_DASH() {
      // Creation section.
      final java.lang.String DASH = com.projectmas.gwt.reelvision.shared.Constants.DASH;
      // Setup section.

      return DASH;
    }

    /**
     * Getter for YYYY_MM_DD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_YYYY_MM_DD() {
      return build_YYYY_MM_DD();
    }
    private java.lang.String build_YYYY_MM_DD() {
      // Creation section.
      final java.lang.String YYYY_MM_DD = com.projectmas.gwt.reelvision.shared.Constants.YYYY_MM_DD;
      // Setup section.

      return YYYY_MM_DD;
    }

    /**
     * Getter for YYYY_MM_DD_HH_MM_SS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_YYYY_MM_DD_HH_MM_SS() {
      return build_YYYY_MM_DD_HH_MM_SS();
    }
    private java.lang.String build_YYYY_MM_DD_HH_MM_SS() {
      // Creation section.
      final java.lang.String YYYY_MM_DD_HH_MM_SS = com.projectmas.gwt.reelvision.shared.Constants.YYYY_MM_DD_HH_MM_SS;
      // Setup section.

      return YYYY_MM_DD_HH_MM_SS;
    }

    /**
     * Getter for CHOOSE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CHOOSE() {
      return build_CHOOSE();
    }
    private java.lang.String build_CHOOSE() {
      // Creation section.
      final java.lang.String CHOOSE = com.projectmas.gwt.reelvision.shared.Constants.CHOOSE;
      // Setup section.

      return CHOOSE;
    }

    /**
     * Getter for TODAY called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_TODAY() {
      return build_TODAY();
    }
    private java.lang.String build_TODAY() {
      // Creation section.
      final java.lang.String TODAY = com.projectmas.gwt.reelvision.shared.Constants.TODAY;
      // Setup section.

      return TODAY;
    }

    /**
     * Getter for LOG_IN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOG_IN() {
      return build_LOG_IN();
    }
    private java.lang.String build_LOG_IN() {
      // Creation section.
      final java.lang.String LOG_IN = com.projectmas.gwt.reelvision.shared.Constants.LOG_IN;
      // Setup section.

      return LOG_IN;
    }

    /**
     * Getter for LOG_OUT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOG_OUT() {
      return build_LOG_OUT();
    }
    private java.lang.String build_LOG_OUT() {
      // Creation section.
      final java.lang.String LOG_OUT = com.projectmas.gwt.reelvision.shared.Constants.LOG_OUT;
      // Setup section.

      return LOG_OUT;
    }

    /**
     * Getter for LOGIN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOGIN() {
      return build_LOGIN();
    }
    private java.lang.String build_LOGIN() {
      // Creation section.
      final java.lang.String LOGIN = com.projectmas.gwt.reelvision.shared.Constants.LOGIN;
      // Setup section.

      return LOGIN;
    }

    /**
     * Getter for PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PASSWORD() {
      return build_PASSWORD();
    }
    private java.lang.String build_PASSWORD() {
      // Creation section.
      final java.lang.String PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.PASSWORD;
      // Setup section.

      return PASSWORD;
    }

    /**
     * Getter for ENTER_LOGIN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ENTER_LOGIN() {
      return build_ENTER_LOGIN();
    }
    private java.lang.String build_ENTER_LOGIN() {
      // Creation section.
      final java.lang.String ENTER_LOGIN = com.projectmas.gwt.reelvision.shared.Constants.ENTER_LOGIN;
      // Setup section.

      return ENTER_LOGIN;
    }

    /**
     * Getter for ENTER_PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ENTER_PASSWORD() {
      return build_ENTER_PASSWORD();
    }
    private java.lang.String build_ENTER_PASSWORD() {
      // Creation section.
      final java.lang.String ENTER_PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.ENTER_PASSWORD;
      // Setup section.

      return ENTER_PASSWORD;
    }

    /**
     * Getter for SETTINGS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SETTINGS() {
      return build_SETTINGS();
    }
    private java.lang.String build_SETTINGS() {
      // Creation section.
      final java.lang.String SETTINGS = com.projectmas.gwt.reelvision.shared.Constants.SETTINGS;
      // Setup section.

      return SETTINGS;
    }

    /**
     * Getter for WRONG_PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_WRONG_PASSWORD() {
      return build_WRONG_PASSWORD();
    }
    private java.lang.String build_WRONG_PASSWORD() {
      // Creation section.
      final java.lang.String WRONG_PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.WRONG_PASSWORD;
      // Setup section.

      return WRONG_PASSWORD;
    }

    /**
     * Getter for DASHBOARD called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DASHBOARD() {
      return build_DASHBOARD();
    }
    private java.lang.String build_DASHBOARD() {
      // Creation section.
      final java.lang.String DASHBOARD = com.projectmas.gwt.reelvision.shared.Constants.DASHBOARD;
      // Setup section.

      return DASHBOARD;
    }

    /**
     * Getter for RESERVATIONS called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_RESERVATIONS() {
      return build_RESERVATIONS();
    }
    private java.lang.String build_RESERVATIONS() {
      // Creation section.
      final java.lang.String RESERVATIONS = com.projectmas.gwt.reelvision.shared.Constants.RESERVATIONS;
      // Setup section.

      return RESERVATIONS;
    }

    /**
     * Getter for SALE called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SALE() {
      return build_SALE();
    }
    private java.lang.String build_SALE() {
      // Creation section.
      final java.lang.String SALE = com.projectmas.gwt.reelvision.shared.Constants.SALE;
      // Setup section.

      return SALE;
    }

    /**
     * Getter for AUDIENCES called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_AUDIENCES() {
      return build_AUDIENCES();
    }
    private java.lang.String build_AUDIENCES() {
      // Creation section.
      final java.lang.String AUDIENCES = com.projectmas.gwt.reelvision.shared.Constants.AUDIENCES;
      // Setup section.

      return AUDIENCES;
    }

    /**
     * Getter for SHOWTIMES called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIMES() {
      return build_SHOWTIMES();
    }
    private java.lang.String build_SHOWTIMES() {
      // Creation section.
      final java.lang.String SHOWTIMES = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIMES;
      // Setup section.

      return SHOWTIMES;
    }

    /**
     * Getter for MOVIES called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIES() {
      return build_MOVIES();
    }
    private java.lang.String build_MOVIES() {
      // Creation section.
      final java.lang.String MOVIES = com.projectmas.gwt.reelvision.shared.Constants.MOVIES;
      // Setup section.

      return MOVIES;
    }

    /**
     * Getter for SHOWTIME_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIME_MANAGEMENT() {
      return build_SHOWTIME_MANAGEMENT();
    }
    private java.lang.String build_SHOWTIME_MANAGEMENT() {
      // Creation section.
      final java.lang.String SHOWTIME_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIME_MANAGEMENT;
      // Setup section.

      return SHOWTIME_MANAGEMENT;
    }

    /**
     * Getter for MOVIE_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_MANAGEMENT() {
      return build_MOVIE_MANAGEMENT();
    }
    private java.lang.String build_MOVIE_MANAGEMENT() {
      // Creation section.
      final java.lang.String MOVIE_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_MANAGEMENT;
      // Setup section.

      return MOVIE_MANAGEMENT;
    }

    /**
     * Getter for USER_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_MANAGEMENT() {
      return build_USER_MANAGEMENT();
    }
    private java.lang.String build_USER_MANAGEMENT() {
      // Creation section.
      final java.lang.String USER_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.USER_MANAGEMENT;
      // Setup section.

      return USER_MANAGEMENT;
    }

    /**
     * Getter for CUSTOMERS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CUSTOMERS() {
      return build_CUSTOMERS();
    }
    private java.lang.String build_CUSTOMERS() {
      // Creation section.
      final java.lang.String CUSTOMERS = com.projectmas.gwt.reelvision.shared.Constants.CUSTOMERS;
      // Setup section.

      return CUSTOMERS;
    }

    /**
     * Getter for REPORTS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPORTS() {
      return build_REPORTS();
    }
    private java.lang.String build_REPORTS() {
      // Creation section.
      final java.lang.String REPORTS = com.projectmas.gwt.reelvision.shared.Constants.REPORTS;
      // Setup section.

      return REPORTS;
    }

    /**
     * Getter for NEW called 2 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String NEW;
    private java.lang.String get_NEW() {
      return NEW;
    }
    private java.lang.String build_NEW() {
      // Creation section.
      NEW = com.projectmas.gwt.reelvision.shared.Constants.NEW;
      // Setup section.

      return NEW;
    }

    /**
     * Getter for FIND called 2 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String FIND;
    private java.lang.String get_FIND() {
      return FIND;
    }
    private java.lang.String build_FIND() {
      // Creation section.
      FIND = com.projectmas.gwt.reelvision.shared.Constants.FIND;
      // Setup section.

      return FIND;
    }

    /**
     * Getter for REPERTOIRE_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPERTOIRE_MANAGEMENT() {
      return build_REPERTOIRE_MANAGEMENT();
    }
    private java.lang.String build_REPERTOIRE_MANAGEMENT() {
      // Creation section.
      final java.lang.String REPERTOIRE_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.REPERTOIRE_MANAGEMENT;
      // Setup section.

      return REPERTOIRE_MANAGEMENT;
    }

    /**
     * Getter for PRIVCLASS_OBTAIN_PROBLEM called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PRIVCLASS_OBTAIN_PROBLEM() {
      return build_PRIVCLASS_OBTAIN_PROBLEM();
    }
    private java.lang.String build_PRIVCLASS_OBTAIN_PROBLEM() {
      // Creation section.
      final java.lang.String PRIVCLASS_OBTAIN_PROBLEM = com.projectmas.gwt.reelvision.shared.Constants.PRIVCLASS_OBTAIN_PROBLEM;
      // Setup section.

      return PRIVCLASS_OBTAIN_PROBLEM;
    }

    /**
     * Getter for LOG_OUT_PROBLEM called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOG_OUT_PROBLEM() {
      return build_LOG_OUT_PROBLEM();
    }
    private java.lang.String build_LOG_OUT_PROBLEM() {
      // Creation section.
      final java.lang.String LOG_OUT_PROBLEM = com.projectmas.gwt.reelvision.shared.Constants.LOG_OUT_PROBLEM;
      // Setup section.

      return LOG_OUT_PROBLEM;
    }

    /**
     * Getter for UNEXPECTED_ERROR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_UNEXPECTED_ERROR() {
      return build_UNEXPECTED_ERROR();
    }
    private java.lang.String build_UNEXPECTED_ERROR() {
      // Creation section.
      final java.lang.String UNEXPECTED_ERROR = com.projectmas.gwt.reelvision.shared.Constants.UNEXPECTED_ERROR;
      // Setup section.

      return UNEXPECTED_ERROR;
    }

    /**
     * Getter for PRIVILAGE_CLASS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PRIVILAGE_CLASS() {
      return build_PRIVILAGE_CLASS();
    }
    private java.lang.String build_PRIVILAGE_CLASS() {
      // Creation section.
      final java.lang.String PRIVILAGE_CLASS = com.projectmas.gwt.reelvision.shared.Constants.PRIVILAGE_CLASS;
      // Setup section.

      return PRIVILAGE_CLASS;
    }

    /**
     * Getter for ADMINISTRATOR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADMINISTRATOR() {
      return build_ADMINISTRATOR();
    }
    private java.lang.String build_ADMINISTRATOR() {
      // Creation section.
      final java.lang.String ADMINISTRATOR = com.projectmas.gwt.reelvision.shared.Constants.ADMINISTRATOR;
      // Setup section.

      return ADMINISTRATOR;
    }

    /**
     * Getter for MANAGER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MANAGER() {
      return build_MANAGER();
    }
    private java.lang.String build_MANAGER() {
      // Creation section.
      final java.lang.String MANAGER = com.projectmas.gwt.reelvision.shared.Constants.MANAGER;
      // Setup section.

      return MANAGER;
    }

    /**
     * Getter for USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER() {
      return build_USER();
    }
    private java.lang.String build_USER() {
      // Creation section.
      final java.lang.String USER = com.projectmas.gwt.reelvision.shared.Constants.USER;
      // Setup section.

      return USER;
    }

    /**
     * Getter for CONFIRM_PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CONFIRM_PASSWORD() {
      return build_CONFIRM_PASSWORD();
    }
    private java.lang.String build_CONFIRM_PASSWORD() {
      // Creation section.
      final java.lang.String CONFIRM_PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.CONFIRM_PASSWORD;
      // Setup section.

      return CONFIRM_PASSWORD;
    }

    /**
     * Getter for USERS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USERS() {
      return build_USERS();
    }
    private java.lang.String build_USERS() {
      // Creation section.
      final java.lang.String USERS = com.projectmas.gwt.reelvision.shared.Constants.USERS;
      // Setup section.

      return USERS;
    }

    /**
     * Getter for ENTER_PASSWORD_CONFIRMATION called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ENTER_PASSWORD_CONFIRMATION() {
      return build_ENTER_PASSWORD_CONFIRMATION();
    }
    private java.lang.String build_ENTER_PASSWORD_CONFIRMATION() {
      // Creation section.
      final java.lang.String ENTER_PASSWORD_CONFIRMATION = com.projectmas.gwt.reelvision.shared.Constants.ENTER_PASSWORD_CONFIRMATION;
      // Setup section.

      return ENTER_PASSWORD_CONFIRMATION;
    }

    /**
     * Getter for ADD_USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_USER() {
      return build_ADD_USER();
    }
    private java.lang.String build_ADD_USER() {
      // Creation section.
      final java.lang.String ADD_USER = com.projectmas.gwt.reelvision.shared.Constants.ADD_USER;
      // Setup section.

      return ADD_USER;
    }

    /**
     * Getter for DELETE_USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DELETE_USER() {
      return build_DELETE_USER();
    }
    private java.lang.String build_DELETE_USER() {
      // Creation section.
      final java.lang.String DELETE_USER = com.projectmas.gwt.reelvision.shared.Constants.DELETE_USER;
      // Setup section.

      return DELETE_USER;
    }

    /**
     * Getter for CREATED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CREATED() {
      return build_CREATED();
    }
    private java.lang.String build_CREATED() {
      // Creation section.
      final java.lang.String CREATED = com.projectmas.gwt.reelvision.shared.Constants.CREATED;
      // Setup section.

      return CREATED;
    }

    /**
     * Getter for UPDATED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_UPDATED() {
      return build_UPDATED();
    }
    private java.lang.String build_UPDATED() {
      // Creation section.
      final java.lang.String UPDATED = com.projectmas.gwt.reelvision.shared.Constants.UPDATED;
      // Setup section.

      return UPDATED;
    }

    /**
     * Getter for PRIVILAGE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PRIVILAGE() {
      return build_PRIVILAGE();
    }
    private java.lang.String build_PRIVILAGE() {
      // Creation section.
      final java.lang.String PRIVILAGE = com.projectmas.gwt.reelvision.shared.Constants.PRIVILAGE;
      // Setup section.

      return PRIVILAGE;
    }

    /**
     * Getter for LAST_LOGIN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LAST_LOGIN() {
      return build_LAST_LOGIN();
    }
    private java.lang.String build_LAST_LOGIN() {
      // Creation section.
      final java.lang.String LAST_LOGIN = com.projectmas.gwt.reelvision.shared.Constants.LAST_LOGIN;
      // Setup section.

      return LAST_LOGIN;
    }

    /**
     * Getter for USER_EXISTS_ALREADY_EXISTS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_EXISTS_ALREADY_EXISTS() {
      return build_USER_EXISTS_ALREADY_EXISTS();
    }
    private java.lang.String build_USER_EXISTS_ALREADY_EXISTS() {
      // Creation section.
      final java.lang.String USER_EXISTS_ALREADY_EXISTS = com.projectmas.gwt.reelvision.shared.Constants.USER_EXISTS_ALREADY_EXISTS;
      // Setup section.

      return USER_EXISTS_ALREADY_EXISTS;
    }

    /**
     * Getter for USER_ADDED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_ADDED() {
      return build_USER_ADDED();
    }
    private java.lang.String build_USER_ADDED() {
      // Creation section.
      final java.lang.String USER_ADDED = com.projectmas.gwt.reelvision.shared.Constants.USER_ADDED;
      // Setup section.

      return USER_ADDED;
    }

    /**
     * Getter for USER_DELETED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_DELETED() {
      return build_USER_DELETED();
    }
    private java.lang.String build_USER_DELETED() {
      // Creation section.
      final java.lang.String USER_DELETED = com.projectmas.gwt.reelvision.shared.Constants.USER_DELETED;
      // Setup section.

      return USER_DELETED;
    }

    /**
     * Getter for PROVIDED_PASSWORDS_ARE_DIFFERENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PROVIDED_PASSWORDS_ARE_DIFFERENT() {
      return build_PROVIDED_PASSWORDS_ARE_DIFFERENT();
    }
    private java.lang.String build_PROVIDED_PASSWORDS_ARE_DIFFERENT() {
      // Creation section.
      final java.lang.String PROVIDED_PASSWORDS_ARE_DIFFERENT = com.projectmas.gwt.reelvision.shared.Constants.PROVIDED_PASSWORDS_ARE_DIFFERENT;
      // Setup section.

      return PROVIDED_PASSWORDS_ARE_DIFFERENT;
    }

    /**
     * Getter for CANNOT_DELETE_CURRENT_LOGGED_IN_USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CANNOT_DELETE_CURRENT_LOGGED_IN_USER() {
      return build_CANNOT_DELETE_CURRENT_LOGGED_IN_USER();
    }
    private java.lang.String build_CANNOT_DELETE_CURRENT_LOGGED_IN_USER() {
      // Creation section.
      final java.lang.String CANNOT_DELETE_CURRENT_LOGGED_IN_USER = com.projectmas.gwt.reelvision.shared.Constants.CANNOT_DELETE_CURRENT_LOGGED_IN_USER;
      // Setup section.

      return CANNOT_DELETE_CURRENT_LOGGED_IN_USER;
    }

    /**
     * Getter for ADD_OR_EDIT_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_OR_EDIT_MOVIE() {
      return build_ADD_OR_EDIT_MOVIE();
    }
    private java.lang.String build_ADD_OR_EDIT_MOVIE() {
      // Creation section.
      final java.lang.String ADD_OR_EDIT_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.ADD_OR_EDIT_MOVIE;
      // Setup section.

      return ADD_OR_EDIT_MOVIE;
    }

    /**
     * Getter for DIRECTOR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DIRECTOR() {
      return build_DIRECTOR();
    }
    private java.lang.String build_DIRECTOR() {
      // Creation section.
      final java.lang.String DIRECTOR = com.projectmas.gwt.reelvision.shared.Constants.DIRECTOR;
      // Setup section.

      return DIRECTOR;
    }

    /**
     * Getter for TITLE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_TITLE() {
      return build_TITLE();
    }
    private java.lang.String build_TITLE() {
      // Creation section.
      final java.lang.String TITLE = com.projectmas.gwt.reelvision.shared.Constants.TITLE;
      // Setup section.

      return TITLE;
    }

    /**
     * Getter for RELEASE_DATE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_RELEASE_DATE() {
      return build_RELEASE_DATE();
    }
    private java.lang.String build_RELEASE_DATE() {
      // Creation section.
      final java.lang.String RELEASE_DATE = com.projectmas.gwt.reelvision.shared.Constants.RELEASE_DATE;
      // Setup section.

      return RELEASE_DATE;
    }

    /**
     * Getter for DESCRIPTION called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DESCRIPTION() {
      return build_DESCRIPTION();
    }
    private java.lang.String build_DESCRIPTION() {
      // Creation section.
      final java.lang.String DESCRIPTION = com.projectmas.gwt.reelvision.shared.Constants.DESCRIPTION;
      // Setup section.

      return DESCRIPTION;
    }

    /**
     * Getter for COUNTRY called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_COUNTRY() {
      return build_COUNTRY();
    }
    private java.lang.String build_COUNTRY() {
      // Creation section.
      final java.lang.String COUNTRY = com.projectmas.gwt.reelvision.shared.Constants.COUNTRY;
      // Setup section.

      return COUNTRY;
    }

    /**
     * Getter for ADD_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_MOVIE() {
      return build_ADD_MOVIE();
    }
    private java.lang.String build_ADD_MOVIE() {
      // Creation section.
      final java.lang.String ADD_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.ADD_MOVIE;
      // Setup section.

      return ADD_MOVIE;
    }

    /**
     * Getter for EDIT_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_EDIT_MOVIE() {
      return build_EDIT_MOVIE();
    }
    private java.lang.String build_EDIT_MOVIE() {
      // Creation section.
      final java.lang.String EDIT_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.EDIT_MOVIE;
      // Setup section.

      return EDIT_MOVIE;
    }

    /**
     * Getter for REMOVE_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REMOVE_MOVIE() {
      return build_REMOVE_MOVIE();
    }
    private java.lang.String build_REMOVE_MOVIE() {
      // Creation section.
      final java.lang.String REMOVE_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.REMOVE_MOVIE;
      // Setup section.

      return REMOVE_MOVIE;
    }

    /**
     * Getter for MOVIE_ADDED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_ADDED() {
      return build_MOVIE_ADDED();
    }
    private java.lang.String build_MOVIE_ADDED() {
      // Creation section.
      final java.lang.String MOVIE_ADDED = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_ADDED;
      // Setup section.

      return MOVIE_ADDED;
    }

    /**
     * Getter for MOVIE_DELETED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_DELETED() {
      return build_MOVIE_DELETED();
    }
    private java.lang.String build_MOVIE_DELETED() {
      // Creation section.
      final java.lang.String MOVIE_DELETED = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_DELETED;
      // Setup section.

      return MOVIE_DELETED;
    }

    /**
     * Getter for MOVIE_EDITED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_EDITED() {
      return build_MOVIE_EDITED();
    }
    private java.lang.String build_MOVIE_EDITED() {
      // Creation section.
      final java.lang.String MOVIE_EDITED = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_EDITED;
      // Setup section.

      return MOVIE_EDITED;
    }

    /**
     * Getter for PLEASE_SELECT_MOVIE_TO_DELETE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PLEASE_SELECT_MOVIE_TO_DELETE() {
      return build_PLEASE_SELECT_MOVIE_TO_DELETE();
    }
    private java.lang.String build_PLEASE_SELECT_MOVIE_TO_DELETE() {
      // Creation section.
      final java.lang.String PLEASE_SELECT_MOVIE_TO_DELETE = com.projectmas.gwt.reelvision.shared.Constants.PLEASE_SELECT_MOVIE_TO_DELETE;
      // Setup section.

      return PLEASE_SELECT_MOVIE_TO_DELETE;
    }

    /**
     * Getter for PLEASE_SELECT_MOVIE_TO_EDIT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PLEASE_SELECT_MOVIE_TO_EDIT() {
      return build_PLEASE_SELECT_MOVIE_TO_EDIT();
    }
    private java.lang.String build_PLEASE_SELECT_MOVIE_TO_EDIT() {
      // Creation section.
      final java.lang.String PLEASE_SELECT_MOVIE_TO_EDIT = com.projectmas.gwt.reelvision.shared.Constants.PLEASE_SELECT_MOVIE_TO_EDIT;
      // Setup section.

      return PLEASE_SELECT_MOVIE_TO_EDIT;
    }

    /**
     * Getter for ADD_OR_EDIT_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_OR_EDIT_REPERTOIRE() {
      return build_ADD_OR_EDIT_REPERTOIRE();
    }
    private java.lang.String build_ADD_OR_EDIT_REPERTOIRE() {
      // Creation section.
      final java.lang.String ADD_OR_EDIT_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.ADD_OR_EDIT_REPERTOIRE;
      // Setup section.

      return ADD_OR_EDIT_REPERTOIRE;
    }

    /**
     * Getter for REPERTOIRES called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPERTOIRES() {
      return build_REPERTOIRES();
    }
    private java.lang.String build_REPERTOIRES() {
      // Creation section.
      final java.lang.String REPERTOIRES = com.projectmas.gwt.reelvision.shared.Constants.REPERTOIRES;
      // Setup section.

      return REPERTOIRES;
    }

    /**
     * Getter for ADD_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_REPERTOIRE() {
      return build_ADD_REPERTOIRE();
    }
    private java.lang.String build_ADD_REPERTOIRE() {
      // Creation section.
      final java.lang.String ADD_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.ADD_REPERTOIRE;
      // Setup section.

      return ADD_REPERTOIRE;
    }

    /**
     * Getter for EDIT_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_EDIT_REPERTOIRE() {
      return build_EDIT_REPERTOIRE();
    }
    private java.lang.String build_EDIT_REPERTOIRE() {
      // Creation section.
      final java.lang.String EDIT_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.EDIT_REPERTOIRE;
      // Setup section.

      return EDIT_REPERTOIRE;
    }

    /**
     * Getter for REMOVE_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REMOVE_REPERTOIRE() {
      return build_REMOVE_REPERTOIRE();
    }
    private java.lang.String build_REMOVE_REPERTOIRE() {
      // Creation section.
      final java.lang.String REMOVE_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.REMOVE_REPERTOIRE;
      // Setup section.

      return REMOVE_REPERTOIRE;
    }

    /**
     * Getter for START_DATE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_START_DATE() {
      return build_START_DATE();
    }
    private java.lang.String build_START_DATE() {
      // Creation section.
      final java.lang.String START_DATE = com.projectmas.gwt.reelvision.shared.Constants.START_DATE;
      // Setup section.

      return START_DATE;
    }

    /**
     * Getter for END_DATE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_END_DATE() {
      return build_END_DATE();
    }
    private java.lang.String build_END_DATE() {
      // Creation section.
      final java.lang.String END_DATE = com.projectmas.gwt.reelvision.shared.Constants.END_DATE;
      // Setup section.

      return END_DATE;
    }

    /**
     * Getter for NEW_SALE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_NEW_SALE_ANCHOR_ID() {
      return build_NEW_SALE_ANCHOR_ID();
    }
    private java.lang.String build_NEW_SALE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String NEW_SALE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.NEW_SALE_ANCHOR_ID;
      // Setup section.

      return NEW_SALE_ANCHOR_ID;
    }

    /**
     * Getter for SEARCH_SALE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SEARCH_SALE_ANCHOR_ID() {
      return build_SEARCH_SALE_ANCHOR_ID();
    }
    private java.lang.String build_SEARCH_SALE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String SEARCH_SALE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.SEARCH_SALE_ANCHOR_ID;
      // Setup section.

      return SEARCH_SALE_ANCHOR_ID;
    }

    /**
     * Getter for NEW_RESERVE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_NEW_RESERVE_ANCHOR_ID() {
      return build_NEW_RESERVE_ANCHOR_ID();
    }
    private java.lang.String build_NEW_RESERVE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String NEW_RESERVE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.NEW_RESERVE_ANCHOR_ID;
      // Setup section.

      return NEW_RESERVE_ANCHOR_ID;
    }

    /**
     * Getter for SEARCH_RESERVE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SEARCH_RESERVE_ANCHOR_ID() {
      return build_SEARCH_RESERVE_ANCHOR_ID();
    }
    private java.lang.String build_SEARCH_RESERVE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String SEARCH_RESERVE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.SEARCH_RESERVE_ANCHOR_ID;
      // Setup section.

      return SEARCH_RESERVE_ANCHOR_ID;
    }

    /**
     * Getter for DASHBOARD_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DASHBOARD_ANCHOR_ID() {
      return build_DASHBOARD_ANCHOR_ID();
    }
    private java.lang.String build_DASHBOARD_ANCHOR_ID() {
      // Creation section.
      final java.lang.String DASHBOARD_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.DASHBOARD_ANCHOR_ID;
      // Setup section.

      return DASHBOARD_ANCHOR_ID;
    }

    /**
     * Getter for SHOWTIME_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIME_ANCHOR_ID() {
      return build_SHOWTIME_ANCHOR_ID();
    }
    private java.lang.String build_SHOWTIME_ANCHOR_ID() {
      // Creation section.
      final java.lang.String SHOWTIME_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIME_ANCHOR_ID;
      // Setup section.

      return SHOWTIME_ANCHOR_ID;
    }

    /**
     * Getter for MOVIES_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIES_ANCHOR_ID() {
      return build_MOVIES_ANCHOR_ID();
    }
    private java.lang.String build_MOVIES_ANCHOR_ID() {
      // Creation section.
      final java.lang.String MOVIES_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.MOVIES_ANCHOR_ID;
      // Setup section.

      return MOVIES_ANCHOR_ID;
    }

    /**
     * Getter for AUDIENCES_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_AUDIENCES_ANCHOR_ID() {
      return build_AUDIENCES_ANCHOR_ID();
    }
    private java.lang.String build_AUDIENCES_ANCHOR_ID() {
      // Creation section.
      final java.lang.String AUDIENCES_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.AUDIENCES_ANCHOR_ID;
      // Setup section.

      return AUDIENCES_ANCHOR_ID;
    }

    /**
     * Getter for REPORTS_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPORTS_ANCHOR_ID() {
      return build_REPORTS_ANCHOR_ID();
    }
    private java.lang.String build_REPORTS_ANCHOR_ID() {
      // Creation section.
      final java.lang.String REPORTS_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.REPORTS_ANCHOR_ID;
      // Setup section.

      return REPORTS_ANCHOR_ID;
    }

    /**
     * Getter for SHOWTIME_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIME_MANAGEMENT_ID() {
      return build_SHOWTIME_MANAGEMENT_ID();
    }
    private java.lang.String build_SHOWTIME_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String SHOWTIME_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIME_MANAGEMENT_ID;
      // Setup section.

      return SHOWTIME_MANAGEMENT_ID;
    }

    /**
     * Getter for MOVIE_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_MANAGEMENT_ID() {
      return build_MOVIE_MANAGEMENT_ID();
    }
    private java.lang.String build_MOVIE_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String MOVIE_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_MANAGEMENT_ID;
      // Setup section.

      return MOVIE_MANAGEMENT_ID;
    }

    /**
     * Getter for USER_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_MANAGEMENT_ID() {
      return build_USER_MANAGEMENT_ID();
    }
    private java.lang.String build_USER_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String USER_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.USER_MANAGEMENT_ID;
      // Setup section.

      return USER_MANAGEMENT_ID;
    }

    /**
     * Getter for REPERTOIRE_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPERTOIRE_MANAGEMENT_ID() {
      return build_REPERTOIRE_MANAGEMENT_ID();
    }
    private java.lang.String build_REPERTOIRE_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String REPERTOIRE_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.REPERTOIRE_MANAGEMENT_ID;
      // Setup section.

      return REPERTOIRE_MANAGEMENT_ID;
    }

    /**
     * Getter for USER_SESSION_ATTR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_SESSION_ATTR() {
      return build_USER_SESSION_ATTR();
    }
    private java.lang.String build_USER_SESSION_ATTR() {
      // Creation section.
      final java.lang.String USER_SESSION_ATTR = com.projectmas.gwt.reelvision.shared.Constants.USER_SESSION_ATTR;
      // Setup section.

      return USER_SESSION_ATTR;
    }

    /**
     * Getter for f_ListItem1 called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private org.gwtbootstrap3.client.ui.ListItem get_f_ListItem1() {
      return build_f_ListItem1();
    }
    private org.gwtbootstrap3.client.ui.ListItem build_f_ListItem1() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.ListItem f_ListItem1 = (org.gwtbootstrap3.client.ui.ListItem) GWT.create(org.gwtbootstrap3.client.ui.ListItem.class);
      // Setup section.
      f_ListItem1.add(get_operationList());

      return f_ListItem1;
    }

    /**
     * Getter for operationList called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private org.gwtbootstrap3.client.ui.html.UnorderedList get_operationList() {
      return build_operationList();
    }
    private org.gwtbootstrap3.client.ui.html.UnorderedList build_operationList() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.html.UnorderedList operationList = (org.gwtbootstrap3.client.ui.html.UnorderedList) GWT.create(org.gwtbootstrap3.client.ui.html.UnorderedList.class);
      // Setup section.
      operationList.add(get_f_ListItem2());
      operationList.add(get_f_ListItem3());
      operationList.add(get_f_ListItem5());
      operationList.add(get_f_ListItem7());
      operationList.add(get_f_ListItem8());
      operationList.add(get_f_ListItem9());

      this.owner.operationList = operationList;

      return operationList;
    }

    /**
     * Getter for f_ListItem2 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.ListItem get_f_ListItem2() {
      return build_f_ListItem2();
    }
    private org.gwtbootstrap3.client.ui.ListItem build_f_ListItem2() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.ListItem f_ListItem2 = (org.gwtbootstrap3.client.ui.ListItem) GWT.create(org.gwtbootstrap3.client.ui.ListItem.class);
      // Setup section.
      f_ListItem2.add(get_dashboardAnchor());

      return f_ListItem2;
    }

    /**
     * Getter for dashboardAnchor called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Anchor get_dashboardAnchor() {
      return build_dashboardAnchor();
    }
    private org.gwtbootstrap3.client.ui.Anchor build_dashboardAnchor() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Anchor dashboardAnchor = (org.gwtbootstrap3.client.ui.Anchor) GWT.create(org.gwtbootstrap3.client.ui.Anchor.class);
      // Setup section.
      dashboardAnchor.setIcon(org.gwtbootstrap3.client.ui.constants.IconType.HOME);
      dashboardAnchor.setText("" + get_DASHBOARD() + "");

      this.owner.dashboardAnchor = dashboardAnchor;

      return dashboardAnchor;
    }

    /**
     * Getter for f_ListItem3 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.ListItem get_f_ListItem3() {
      return build_f_ListItem3();
    }
    private org.gwtbootstrap3.client.ui.ListItem build_f_ListItem3() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.ListItem f_ListItem3 = (org.gwtbootstrap3.client.ui.ListItem) GWT.create(org.gwtbootstrap3.client.ui.ListItem.class);
      // Setup section.
      f_ListItem3.add(get_saleAnchor());
      f_ListItem3.add(get_saleCollapse());

      return f_ListItem3;
    }

    /**
     * Getter for saleAnchor called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Anchor get_saleAnchor() {
      return build_saleAnchor();
    }
    private org.gwtbootstrap3.client.ui.Anchor build_saleAnchor() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Anchor saleAnchor = (org.gwtbootstrap3.client.ui.Anchor) GWT.create(org.gwtbootstrap3.client.ui.Anchor.class);
      // Setup section.
      saleAnchor.setIcon(org.gwtbootstrap3.client.ui.constants.IconType.CARET_DOWN);
      saleAnchor.setDataToggle(org.gwtbootstrap3.client.ui.constants.Toggle.COLLAPSE);
      saleAnchor.setText("" + get_SALE() + "");
      saleAnchor.setDataTarget("#saleCollapse");

      this.owner.saleAnchor = saleAnchor;

      return saleAnchor;
    }

    /**
     * Getter for saleCollapse called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.html.UnorderedList get_saleCollapse() {
      return build_saleCollapse();
    }
    private org.gwtbootstrap3.client.ui.html.UnorderedList build_saleCollapse() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.html.UnorderedList saleCollapse = (org.gwtbootstrap3.client.ui.html.UnorderedList) GWT.create(org.gwtbootstrap3.client.ui.html.UnorderedList.class);
      // Setup section.
      saleCollapse.add(get_f_ListItem4());
      saleCollapse.setStyleName("collapse");

      this.owner.saleCollapse = saleCollapse;

      return saleCollapse;
    }

    /**
     * Getter for f_ListItem4 called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.ListItem get_f_ListItem4() {
      return build_f_ListItem4();
    }
    private org.gwtbootstrap3.client.ui.ListItem build_f_ListItem4() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.ListItem f_ListItem4 = (org.gwtbootstrap3.client.ui.ListItem) GWT.create(org.gwtbootstrap3.client.ui.ListItem.class);
      // Setup section.
      f_ListItem4.add(get_newSaleAnchor());
      f_ListItem4.add(get_searchSaleAnchor());

      return f_ListItem4;
    }

    /**
     * Getter for newSaleAnchor called 1 times. Type: DEFAULT. Build precedence: 6.
     */
    private org.gwtbootstrap3.client.ui.Anchor get_newSaleAnchor() {
      return build_newSaleAnchor();
    }
    private org.gwtbootstrap3.client.ui.Anchor build_newSaleAnchor() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Anchor newSaleAnchor = (org.gwtbootstrap3.client.ui.Anchor) GWT.create(org.gwtbootstrap3.client.ui.Anchor.class);
      // Setup section.
      newSaleAnchor.setIcon(org.gwtbootstrap3.client.ui.constants.IconType.CART_ARROW_DOWN);
      newSaleAnchor.setText("" + get_NEW() + "");

      this.owner.newSaleAnchor = newSaleAnchor;

      return newSaleAnchor;
    }

    /**
     * Getter for searchSaleAnchor called 1 times. Type: DEFAULT. Build precedence: 6.
     */
    private org.gwtbootstrap3.client.ui.Anchor get_searchSaleAnchor() {
      return build_searchSaleAnchor();
    }
    private org.gwtbootstrap3.client.ui.Anchor build_searchSaleAnchor() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Anchor searchSaleAnchor = (org.gwtbootstrap3.client.ui.Anchor) GWT.create(org.gwtbootstrap3.client.ui.Anchor.class);
      // Setup section.
      searchSaleAnchor.setIcon(org.gwtbootstrap3.client.ui.constants.IconType.SEARCH);
      searchSaleAnchor.setText("" + get_FIND() + "");

      this.owner.searchSaleAnchor = searchSaleAnchor;

      return searchSaleAnchor;
    }

    /**
     * Getter for f_ListItem5 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.ListItem get_f_ListItem5() {
      return build_f_ListItem5();
    }
    private org.gwtbootstrap3.client.ui.ListItem build_f_ListItem5() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.ListItem f_ListItem5 = (org.gwtbootstrap3.client.ui.ListItem) GWT.create(org.gwtbootstrap3.client.ui.ListItem.class);
      // Setup section.
      f_ListItem5.add(get_reserveAnchor());
      f_ListItem5.add(get_reserveCollapse());

      return f_ListItem5;
    }

    /**
     * Getter for reserveAnchor called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Anchor get_reserveAnchor() {
      return build_reserveAnchor();
    }
    private org.gwtbootstrap3.client.ui.Anchor build_reserveAnchor() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Anchor reserveAnchor = (org.gwtbootstrap3.client.ui.Anchor) GWT.create(org.gwtbootstrap3.client.ui.Anchor.class);
      // Setup section.
      reserveAnchor.setIcon(org.gwtbootstrap3.client.ui.constants.IconType.CARET_DOWN);
      reserveAnchor.setDataToggle(org.gwtbootstrap3.client.ui.constants.Toggle.COLLAPSE);
      reserveAnchor.setText("" + get_RESERVATIONS() + "");
      reserveAnchor.setDataTarget("#reserveCollapse");

      this.owner.reserveAnchor = reserveAnchor;

      return reserveAnchor;
    }

    /**
     * Getter for reserveCollapse called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.html.UnorderedList get_reserveCollapse() {
      return build_reserveCollapse();
    }
    private org.gwtbootstrap3.client.ui.html.UnorderedList build_reserveCollapse() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.html.UnorderedList reserveCollapse = (org.gwtbootstrap3.client.ui.html.UnorderedList) GWT.create(org.gwtbootstrap3.client.ui.html.UnorderedList.class);
      // Setup section.
      reserveCollapse.add(get_f_ListItem6());
      reserveCollapse.setStyleName("collapse");

      this.owner.reserveCollapse = reserveCollapse;

      return reserveCollapse;
    }

    /**
     * Getter for f_ListItem6 called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.ListItem get_f_ListItem6() {
      return build_f_ListItem6();
    }
    private org.gwtbootstrap3.client.ui.ListItem build_f_ListItem6() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.ListItem f_ListItem6 = (org.gwtbootstrap3.client.ui.ListItem) GWT.create(org.gwtbootstrap3.client.ui.ListItem.class);
      // Setup section.
      f_ListItem6.add(get_newReserveAnchor());
      f_ListItem6.add(get_searchReserveAnchor());

      return f_ListItem6;
    }

    /**
     * Getter for newReserveAnchor called 1 times. Type: DEFAULT. Build precedence: 6.
     */
    private org.gwtbootstrap3.client.ui.Anchor get_newReserveAnchor() {
      return build_newReserveAnchor();
    }
    private org.gwtbootstrap3.client.ui.Anchor build_newReserveAnchor() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Anchor newReserveAnchor = (org.gwtbootstrap3.client.ui.Anchor) GWT.create(org.gwtbootstrap3.client.ui.Anchor.class);
      // Setup section.
      newReserveAnchor.setIcon(org.gwtbootstrap3.client.ui.constants.IconType.PHONE);
      newReserveAnchor.setText("" + get_NEW() + "");

      this.owner.newReserveAnchor = newReserveAnchor;

      return newReserveAnchor;
    }

    /**
     * Getter for searchReserveAnchor called 1 times. Type: DEFAULT. Build precedence: 6.
     */
    private org.gwtbootstrap3.client.ui.Anchor get_searchReserveAnchor() {
      return build_searchReserveAnchor();
    }
    private org.gwtbootstrap3.client.ui.Anchor build_searchReserveAnchor() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Anchor searchReserveAnchor = (org.gwtbootstrap3.client.ui.Anchor) GWT.create(org.gwtbootstrap3.client.ui.Anchor.class);
      // Setup section.
      searchReserveAnchor.setIcon(org.gwtbootstrap3.client.ui.constants.IconType.SEARCH);
      searchReserveAnchor.setText("" + get_FIND() + "");

      this.owner.searchReserveAnchor = searchReserveAnchor;

      return searchReserveAnchor;
    }

    /**
     * Getter for f_ListItem7 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.ListItem get_f_ListItem7() {
      return build_f_ListItem7();
    }
    private org.gwtbootstrap3.client.ui.ListItem build_f_ListItem7() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.ListItem f_ListItem7 = (org.gwtbootstrap3.client.ui.ListItem) GWT.create(org.gwtbootstrap3.client.ui.ListItem.class);
      // Setup section.
      f_ListItem7.add(get_showtimesAnchor());

      return f_ListItem7;
    }

    /**
     * Getter for showtimesAnchor called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Anchor get_showtimesAnchor() {
      return build_showtimesAnchor();
    }
    private org.gwtbootstrap3.client.ui.Anchor build_showtimesAnchor() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Anchor showtimesAnchor = (org.gwtbootstrap3.client.ui.Anchor) GWT.create(org.gwtbootstrap3.client.ui.Anchor.class);
      // Setup section.
      showtimesAnchor.setIcon(org.gwtbootstrap3.client.ui.constants.IconType.STAR);
      showtimesAnchor.setText("" + get_SHOWTIMES() + "");

      this.owner.showtimesAnchor = showtimesAnchor;

      return showtimesAnchor;
    }

    /**
     * Getter for f_ListItem8 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.ListItem get_f_ListItem8() {
      return build_f_ListItem8();
    }
    private org.gwtbootstrap3.client.ui.ListItem build_f_ListItem8() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.ListItem f_ListItem8 = (org.gwtbootstrap3.client.ui.ListItem) GWT.create(org.gwtbootstrap3.client.ui.ListItem.class);
      // Setup section.
      f_ListItem8.add(get_moviesAnchor());

      return f_ListItem8;
    }

    /**
     * Getter for moviesAnchor called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Anchor get_moviesAnchor() {
      return build_moviesAnchor();
    }
    private org.gwtbootstrap3.client.ui.Anchor build_moviesAnchor() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Anchor moviesAnchor = (org.gwtbootstrap3.client.ui.Anchor) GWT.create(org.gwtbootstrap3.client.ui.Anchor.class);
      // Setup section.
      moviesAnchor.setIcon(org.gwtbootstrap3.client.ui.constants.IconType.TELEVISION);
      moviesAnchor.setText("" + get_MOVIES() + "");

      this.owner.moviesAnchor = moviesAnchor;

      return moviesAnchor;
    }

    /**
     * Getter for f_ListItem9 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.ListItem get_f_ListItem9() {
      return build_f_ListItem9();
    }
    private org.gwtbootstrap3.client.ui.ListItem build_f_ListItem9() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.ListItem f_ListItem9 = (org.gwtbootstrap3.client.ui.ListItem) GWT.create(org.gwtbootstrap3.client.ui.ListItem.class);
      // Setup section.
      f_ListItem9.add(get_audiencesAnchor());

      return f_ListItem9;
    }

    /**
     * Getter for audiencesAnchor called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Anchor get_audiencesAnchor() {
      return build_audiencesAnchor();
    }
    private org.gwtbootstrap3.client.ui.Anchor build_audiencesAnchor() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Anchor audiencesAnchor = (org.gwtbootstrap3.client.ui.Anchor) GWT.create(org.gwtbootstrap3.client.ui.Anchor.class);
      // Setup section.
      audiencesAnchor.setIcon(org.gwtbootstrap3.client.ui.constants.IconType.CLIPBOARD);
      audiencesAnchor.setText("" + get_AUDIENCES() + "");

      this.owner.audiencesAnchor = audiencesAnchor;

      return audiencesAnchor;
    }
  }
}
