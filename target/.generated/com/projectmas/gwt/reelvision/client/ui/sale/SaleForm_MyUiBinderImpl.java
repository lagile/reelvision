// .ui.xml template last modified: 1466190597872
package com.projectmas.gwt.reelvision.client.ui.sale;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiBinderUtil;
import com.google.gwt.user.client.ui.Widget;

public class SaleForm_MyUiBinderImpl implements UiBinder<com.google.gwt.user.client.ui.Widget, com.projectmas.gwt.reelvision.client.ui.sale.SaleForm>, com.projectmas.gwt.reelvision.client.ui.sale.SaleForm.MyUiBinder {


  public com.google.gwt.user.client.ui.Widget createAndBindUi(final com.projectmas.gwt.reelvision.client.ui.sale.SaleForm owner) {


    return new Widgets(owner).get_f_Container1();
  }

  /**
   * Encapsulates the access to all inner widgets
   */
  class Widgets {
    private final com.projectmas.gwt.reelvision.client.ui.sale.SaleForm owner;


    public Widgets(final com.projectmas.gwt.reelvision.client.ui.sale.SaleForm owner) {
      this.owner = owner;
    }


    /**
     * Getter for clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay called 0 times. Type: GENERATED_BUNDLE. Build precedence: 1.
     */
    private com.projectmas.gwt.reelvision.client.ui.sale.SaleForm_MyUiBinderImpl_GenBundle get_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      return build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay();
    }
    private com.projectmas.gwt.reelvision.client.ui.sale.SaleForm_MyUiBinderImpl_GenBundle build_clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay() {
      // Creation section.
      final com.projectmas.gwt.reelvision.client.ui.sale.SaleForm_MyUiBinderImpl_GenBundle clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay = (com.projectmas.gwt.reelvision.client.ui.sale.SaleForm_MyUiBinderImpl_GenBundle) GWT.create(com.projectmas.gwt.reelvision.client.ui.sale.SaleForm_MyUiBinderImpl_GenBundle.class);
      // Setup section.

      return clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay;
    }

    /**
     * Getter for COOKIE_EXPIRY called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.Long get_COOKIE_EXPIRY() {
      return build_COOKIE_EXPIRY();
    }
    private java.lang.Long build_COOKIE_EXPIRY() {
      // Creation section.
      final java.lang.Long COOKIE_EXPIRY = com.projectmas.gwt.reelvision.shared.Constants.COOKIE_EXPIRY;
      // Setup section.

      return COOKIE_EXPIRY;
    }

    /**
     * Getter for HASH called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_HASH() {
      return build_HASH();
    }
    private java.lang.String build_HASH() {
      // Creation section.
      final java.lang.String HASH = com.projectmas.gwt.reelvision.shared.Constants.HASH;
      // Setup section.

      return HASH;
    }

    /**
     * Getter for EMPTY_STRING called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_EMPTY_STRING() {
      return build_EMPTY_STRING();
    }
    private java.lang.String build_EMPTY_STRING() {
      // Creation section.
      final java.lang.String EMPTY_STRING = com.projectmas.gwt.reelvision.shared.Constants.EMPTY_STRING;
      // Setup section.

      return EMPTY_STRING;
    }

    /**
     * Getter for PX called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PX() {
      return build_PX();
    }
    private java.lang.String build_PX() {
      // Creation section.
      final java.lang.String PX = com.projectmas.gwt.reelvision.shared.Constants.PX;
      // Setup section.

      return PX;
    }

    /**
     * Getter for DASH called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DASH() {
      return build_DASH();
    }
    private java.lang.String build_DASH() {
      // Creation section.
      final java.lang.String DASH = com.projectmas.gwt.reelvision.shared.Constants.DASH;
      // Setup section.

      return DASH;
    }

    /**
     * Getter for YYYY_MM_DD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_YYYY_MM_DD() {
      return build_YYYY_MM_DD();
    }
    private java.lang.String build_YYYY_MM_DD() {
      // Creation section.
      final java.lang.String YYYY_MM_DD = com.projectmas.gwt.reelvision.shared.Constants.YYYY_MM_DD;
      // Setup section.

      return YYYY_MM_DD;
    }

    /**
     * Getter for YYYY_MM_DD_HH_MM_SS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_YYYY_MM_DD_HH_MM_SS() {
      return build_YYYY_MM_DD_HH_MM_SS();
    }
    private java.lang.String build_YYYY_MM_DD_HH_MM_SS() {
      // Creation section.
      final java.lang.String YYYY_MM_DD_HH_MM_SS = com.projectmas.gwt.reelvision.shared.Constants.YYYY_MM_DD_HH_MM_SS;
      // Setup section.

      return YYYY_MM_DD_HH_MM_SS;
    }

    /**
     * Getter for CHOOSE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CHOOSE() {
      return build_CHOOSE();
    }
    private java.lang.String build_CHOOSE() {
      // Creation section.
      final java.lang.String CHOOSE = com.projectmas.gwt.reelvision.shared.Constants.CHOOSE;
      // Setup section.

      return CHOOSE;
    }

    /**
     * Getter for TODAY called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_TODAY() {
      return build_TODAY();
    }
    private java.lang.String build_TODAY() {
      // Creation section.
      final java.lang.String TODAY = com.projectmas.gwt.reelvision.shared.Constants.TODAY;
      // Setup section.

      return TODAY;
    }

    /**
     * Getter for LOG_IN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOG_IN() {
      return build_LOG_IN();
    }
    private java.lang.String build_LOG_IN() {
      // Creation section.
      final java.lang.String LOG_IN = com.projectmas.gwt.reelvision.shared.Constants.LOG_IN;
      // Setup section.

      return LOG_IN;
    }

    /**
     * Getter for LOG_OUT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOG_OUT() {
      return build_LOG_OUT();
    }
    private java.lang.String build_LOG_OUT() {
      // Creation section.
      final java.lang.String LOG_OUT = com.projectmas.gwt.reelvision.shared.Constants.LOG_OUT;
      // Setup section.

      return LOG_OUT;
    }

    /**
     * Getter for LOGIN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOGIN() {
      return build_LOGIN();
    }
    private java.lang.String build_LOGIN() {
      // Creation section.
      final java.lang.String LOGIN = com.projectmas.gwt.reelvision.shared.Constants.LOGIN;
      // Setup section.

      return LOGIN;
    }

    /**
     * Getter for PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PASSWORD() {
      return build_PASSWORD();
    }
    private java.lang.String build_PASSWORD() {
      // Creation section.
      final java.lang.String PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.PASSWORD;
      // Setup section.

      return PASSWORD;
    }

    /**
     * Getter for ENTER_LOGIN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ENTER_LOGIN() {
      return build_ENTER_LOGIN();
    }
    private java.lang.String build_ENTER_LOGIN() {
      // Creation section.
      final java.lang.String ENTER_LOGIN = com.projectmas.gwt.reelvision.shared.Constants.ENTER_LOGIN;
      // Setup section.

      return ENTER_LOGIN;
    }

    /**
     * Getter for ENTER_PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ENTER_PASSWORD() {
      return build_ENTER_PASSWORD();
    }
    private java.lang.String build_ENTER_PASSWORD() {
      // Creation section.
      final java.lang.String ENTER_PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.ENTER_PASSWORD;
      // Setup section.

      return ENTER_PASSWORD;
    }

    /**
     * Getter for SETTINGS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SETTINGS() {
      return build_SETTINGS();
    }
    private java.lang.String build_SETTINGS() {
      // Creation section.
      final java.lang.String SETTINGS = com.projectmas.gwt.reelvision.shared.Constants.SETTINGS;
      // Setup section.

      return SETTINGS;
    }

    /**
     * Getter for WRONG_PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_WRONG_PASSWORD() {
      return build_WRONG_PASSWORD();
    }
    private java.lang.String build_WRONG_PASSWORD() {
      // Creation section.
      final java.lang.String WRONG_PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.WRONG_PASSWORD;
      // Setup section.

      return WRONG_PASSWORD;
    }

    /**
     * Getter for DASHBOARD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DASHBOARD() {
      return build_DASHBOARD();
    }
    private java.lang.String build_DASHBOARD() {
      // Creation section.
      final java.lang.String DASHBOARD = com.projectmas.gwt.reelvision.shared.Constants.DASHBOARD;
      // Setup section.

      return DASHBOARD;
    }

    /**
     * Getter for RESERVATIONS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_RESERVATIONS() {
      return build_RESERVATIONS();
    }
    private java.lang.String build_RESERVATIONS() {
      // Creation section.
      final java.lang.String RESERVATIONS = com.projectmas.gwt.reelvision.shared.Constants.RESERVATIONS;
      // Setup section.

      return RESERVATIONS;
    }

    /**
     * Getter for SALE called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SALE() {
      return build_SALE();
    }
    private java.lang.String build_SALE() {
      // Creation section.
      final java.lang.String SALE = com.projectmas.gwt.reelvision.shared.Constants.SALE;
      // Setup section.

      return SALE;
    }

    /**
     * Getter for AUDIENCES called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_AUDIENCES() {
      return build_AUDIENCES();
    }
    private java.lang.String build_AUDIENCES() {
      // Creation section.
      final java.lang.String AUDIENCES = com.projectmas.gwt.reelvision.shared.Constants.AUDIENCES;
      // Setup section.

      return AUDIENCES;
    }

    /**
     * Getter for SHOWTIMES called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIMES() {
      return build_SHOWTIMES();
    }
    private java.lang.String build_SHOWTIMES() {
      // Creation section.
      final java.lang.String SHOWTIMES = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIMES;
      // Setup section.

      return SHOWTIMES;
    }

    /**
     * Getter for MOVIES called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIES() {
      return build_MOVIES();
    }
    private java.lang.String build_MOVIES() {
      // Creation section.
      final java.lang.String MOVIES = com.projectmas.gwt.reelvision.shared.Constants.MOVIES;
      // Setup section.

      return MOVIES;
    }

    /**
     * Getter for SHOWTIME_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIME_MANAGEMENT() {
      return build_SHOWTIME_MANAGEMENT();
    }
    private java.lang.String build_SHOWTIME_MANAGEMENT() {
      // Creation section.
      final java.lang.String SHOWTIME_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIME_MANAGEMENT;
      // Setup section.

      return SHOWTIME_MANAGEMENT;
    }

    /**
     * Getter for MOVIE_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_MANAGEMENT() {
      return build_MOVIE_MANAGEMENT();
    }
    private java.lang.String build_MOVIE_MANAGEMENT() {
      // Creation section.
      final java.lang.String MOVIE_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_MANAGEMENT;
      // Setup section.

      return MOVIE_MANAGEMENT;
    }

    /**
     * Getter for USER_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_MANAGEMENT() {
      return build_USER_MANAGEMENT();
    }
    private java.lang.String build_USER_MANAGEMENT() {
      // Creation section.
      final java.lang.String USER_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.USER_MANAGEMENT;
      // Setup section.

      return USER_MANAGEMENT;
    }

    /**
     * Getter for CUSTOMERS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CUSTOMERS() {
      return build_CUSTOMERS();
    }
    private java.lang.String build_CUSTOMERS() {
      // Creation section.
      final java.lang.String CUSTOMERS = com.projectmas.gwt.reelvision.shared.Constants.CUSTOMERS;
      // Setup section.

      return CUSTOMERS;
    }

    /**
     * Getter for REPORTS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPORTS() {
      return build_REPORTS();
    }
    private java.lang.String build_REPORTS() {
      // Creation section.
      final java.lang.String REPORTS = com.projectmas.gwt.reelvision.shared.Constants.REPORTS;
      // Setup section.

      return REPORTS;
    }

    /**
     * Getter for NEW called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_NEW() {
      return build_NEW();
    }
    private java.lang.String build_NEW() {
      // Creation section.
      final java.lang.String NEW = com.projectmas.gwt.reelvision.shared.Constants.NEW;
      // Setup section.

      return NEW;
    }

    /**
     * Getter for FIND called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_FIND() {
      return build_FIND();
    }
    private java.lang.String build_FIND() {
      // Creation section.
      final java.lang.String FIND = com.projectmas.gwt.reelvision.shared.Constants.FIND;
      // Setup section.

      return FIND;
    }

    /**
     * Getter for REPERTOIRE_MANAGEMENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPERTOIRE_MANAGEMENT() {
      return build_REPERTOIRE_MANAGEMENT();
    }
    private java.lang.String build_REPERTOIRE_MANAGEMENT() {
      // Creation section.
      final java.lang.String REPERTOIRE_MANAGEMENT = com.projectmas.gwt.reelvision.shared.Constants.REPERTOIRE_MANAGEMENT;
      // Setup section.

      return REPERTOIRE_MANAGEMENT;
    }

    /**
     * Getter for PRIVCLASS_OBTAIN_PROBLEM called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PRIVCLASS_OBTAIN_PROBLEM() {
      return build_PRIVCLASS_OBTAIN_PROBLEM();
    }
    private java.lang.String build_PRIVCLASS_OBTAIN_PROBLEM() {
      // Creation section.
      final java.lang.String PRIVCLASS_OBTAIN_PROBLEM = com.projectmas.gwt.reelvision.shared.Constants.PRIVCLASS_OBTAIN_PROBLEM;
      // Setup section.

      return PRIVCLASS_OBTAIN_PROBLEM;
    }

    /**
     * Getter for LOG_OUT_PROBLEM called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LOG_OUT_PROBLEM() {
      return build_LOG_OUT_PROBLEM();
    }
    private java.lang.String build_LOG_OUT_PROBLEM() {
      // Creation section.
      final java.lang.String LOG_OUT_PROBLEM = com.projectmas.gwt.reelvision.shared.Constants.LOG_OUT_PROBLEM;
      // Setup section.

      return LOG_OUT_PROBLEM;
    }

    /**
     * Getter for UNEXPECTED_ERROR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_UNEXPECTED_ERROR() {
      return build_UNEXPECTED_ERROR();
    }
    private java.lang.String build_UNEXPECTED_ERROR() {
      // Creation section.
      final java.lang.String UNEXPECTED_ERROR = com.projectmas.gwt.reelvision.shared.Constants.UNEXPECTED_ERROR;
      // Setup section.

      return UNEXPECTED_ERROR;
    }

    /**
     * Getter for PRIVILAGE_CLASS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PRIVILAGE_CLASS() {
      return build_PRIVILAGE_CLASS();
    }
    private java.lang.String build_PRIVILAGE_CLASS() {
      // Creation section.
      final java.lang.String PRIVILAGE_CLASS = com.projectmas.gwt.reelvision.shared.Constants.PRIVILAGE_CLASS;
      // Setup section.

      return PRIVILAGE_CLASS;
    }

    /**
     * Getter for ADMINISTRATOR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADMINISTRATOR() {
      return build_ADMINISTRATOR();
    }
    private java.lang.String build_ADMINISTRATOR() {
      // Creation section.
      final java.lang.String ADMINISTRATOR = com.projectmas.gwt.reelvision.shared.Constants.ADMINISTRATOR;
      // Setup section.

      return ADMINISTRATOR;
    }

    /**
     * Getter for MANAGER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MANAGER() {
      return build_MANAGER();
    }
    private java.lang.String build_MANAGER() {
      // Creation section.
      final java.lang.String MANAGER = com.projectmas.gwt.reelvision.shared.Constants.MANAGER;
      // Setup section.

      return MANAGER;
    }

    /**
     * Getter for USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER() {
      return build_USER();
    }
    private java.lang.String build_USER() {
      // Creation section.
      final java.lang.String USER = com.projectmas.gwt.reelvision.shared.Constants.USER;
      // Setup section.

      return USER;
    }

    /**
     * Getter for CONFIRM_PASSWORD called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CONFIRM_PASSWORD() {
      return build_CONFIRM_PASSWORD();
    }
    private java.lang.String build_CONFIRM_PASSWORD() {
      // Creation section.
      final java.lang.String CONFIRM_PASSWORD = com.projectmas.gwt.reelvision.shared.Constants.CONFIRM_PASSWORD;
      // Setup section.

      return CONFIRM_PASSWORD;
    }

    /**
     * Getter for USERS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USERS() {
      return build_USERS();
    }
    private java.lang.String build_USERS() {
      // Creation section.
      final java.lang.String USERS = com.projectmas.gwt.reelvision.shared.Constants.USERS;
      // Setup section.

      return USERS;
    }

    /**
     * Getter for ENTER_PASSWORD_CONFIRMATION called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ENTER_PASSWORD_CONFIRMATION() {
      return build_ENTER_PASSWORD_CONFIRMATION();
    }
    private java.lang.String build_ENTER_PASSWORD_CONFIRMATION() {
      // Creation section.
      final java.lang.String ENTER_PASSWORD_CONFIRMATION = com.projectmas.gwt.reelvision.shared.Constants.ENTER_PASSWORD_CONFIRMATION;
      // Setup section.

      return ENTER_PASSWORD_CONFIRMATION;
    }

    /**
     * Getter for ADD_USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_USER() {
      return build_ADD_USER();
    }
    private java.lang.String build_ADD_USER() {
      // Creation section.
      final java.lang.String ADD_USER = com.projectmas.gwt.reelvision.shared.Constants.ADD_USER;
      // Setup section.

      return ADD_USER;
    }

    /**
     * Getter for DELETE_USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DELETE_USER() {
      return build_DELETE_USER();
    }
    private java.lang.String build_DELETE_USER() {
      // Creation section.
      final java.lang.String DELETE_USER = com.projectmas.gwt.reelvision.shared.Constants.DELETE_USER;
      // Setup section.

      return DELETE_USER;
    }

    /**
     * Getter for CREATED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CREATED() {
      return build_CREATED();
    }
    private java.lang.String build_CREATED() {
      // Creation section.
      final java.lang.String CREATED = com.projectmas.gwt.reelvision.shared.Constants.CREATED;
      // Setup section.

      return CREATED;
    }

    /**
     * Getter for UPDATED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_UPDATED() {
      return build_UPDATED();
    }
    private java.lang.String build_UPDATED() {
      // Creation section.
      final java.lang.String UPDATED = com.projectmas.gwt.reelvision.shared.Constants.UPDATED;
      // Setup section.

      return UPDATED;
    }

    /**
     * Getter for PRIVILAGE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PRIVILAGE() {
      return build_PRIVILAGE();
    }
    private java.lang.String build_PRIVILAGE() {
      // Creation section.
      final java.lang.String PRIVILAGE = com.projectmas.gwt.reelvision.shared.Constants.PRIVILAGE;
      // Setup section.

      return PRIVILAGE;
    }

    /**
     * Getter for LAST_LOGIN called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_LAST_LOGIN() {
      return build_LAST_LOGIN();
    }
    private java.lang.String build_LAST_LOGIN() {
      // Creation section.
      final java.lang.String LAST_LOGIN = com.projectmas.gwt.reelvision.shared.Constants.LAST_LOGIN;
      // Setup section.

      return LAST_LOGIN;
    }

    /**
     * Getter for USER_EXISTS_ALREADY_EXISTS called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_EXISTS_ALREADY_EXISTS() {
      return build_USER_EXISTS_ALREADY_EXISTS();
    }
    private java.lang.String build_USER_EXISTS_ALREADY_EXISTS() {
      // Creation section.
      final java.lang.String USER_EXISTS_ALREADY_EXISTS = com.projectmas.gwt.reelvision.shared.Constants.USER_EXISTS_ALREADY_EXISTS;
      // Setup section.

      return USER_EXISTS_ALREADY_EXISTS;
    }

    /**
     * Getter for USER_ADDED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_ADDED() {
      return build_USER_ADDED();
    }
    private java.lang.String build_USER_ADDED() {
      // Creation section.
      final java.lang.String USER_ADDED = com.projectmas.gwt.reelvision.shared.Constants.USER_ADDED;
      // Setup section.

      return USER_ADDED;
    }

    /**
     * Getter for USER_DELETED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_DELETED() {
      return build_USER_DELETED();
    }
    private java.lang.String build_USER_DELETED() {
      // Creation section.
      final java.lang.String USER_DELETED = com.projectmas.gwt.reelvision.shared.Constants.USER_DELETED;
      // Setup section.

      return USER_DELETED;
    }

    /**
     * Getter for PROVIDED_PASSWORDS_ARE_DIFFERENT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PROVIDED_PASSWORDS_ARE_DIFFERENT() {
      return build_PROVIDED_PASSWORDS_ARE_DIFFERENT();
    }
    private java.lang.String build_PROVIDED_PASSWORDS_ARE_DIFFERENT() {
      // Creation section.
      final java.lang.String PROVIDED_PASSWORDS_ARE_DIFFERENT = com.projectmas.gwt.reelvision.shared.Constants.PROVIDED_PASSWORDS_ARE_DIFFERENT;
      // Setup section.

      return PROVIDED_PASSWORDS_ARE_DIFFERENT;
    }

    /**
     * Getter for CANNOT_DELETE_CURRENT_LOGGED_IN_USER called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_CANNOT_DELETE_CURRENT_LOGGED_IN_USER() {
      return build_CANNOT_DELETE_CURRENT_LOGGED_IN_USER();
    }
    private java.lang.String build_CANNOT_DELETE_CURRENT_LOGGED_IN_USER() {
      // Creation section.
      final java.lang.String CANNOT_DELETE_CURRENT_LOGGED_IN_USER = com.projectmas.gwt.reelvision.shared.Constants.CANNOT_DELETE_CURRENT_LOGGED_IN_USER;
      // Setup section.

      return CANNOT_DELETE_CURRENT_LOGGED_IN_USER;
    }

    /**
     * Getter for ADD_OR_EDIT_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_OR_EDIT_MOVIE() {
      return build_ADD_OR_EDIT_MOVIE();
    }
    private java.lang.String build_ADD_OR_EDIT_MOVIE() {
      // Creation section.
      final java.lang.String ADD_OR_EDIT_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.ADD_OR_EDIT_MOVIE;
      // Setup section.

      return ADD_OR_EDIT_MOVIE;
    }

    /**
     * Getter for DIRECTOR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DIRECTOR() {
      return build_DIRECTOR();
    }
    private java.lang.String build_DIRECTOR() {
      // Creation section.
      final java.lang.String DIRECTOR = com.projectmas.gwt.reelvision.shared.Constants.DIRECTOR;
      // Setup section.

      return DIRECTOR;
    }

    /**
     * Getter for TITLE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_TITLE() {
      return build_TITLE();
    }
    private java.lang.String build_TITLE() {
      // Creation section.
      final java.lang.String TITLE = com.projectmas.gwt.reelvision.shared.Constants.TITLE;
      // Setup section.

      return TITLE;
    }

    /**
     * Getter for RELEASE_DATE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_RELEASE_DATE() {
      return build_RELEASE_DATE();
    }
    private java.lang.String build_RELEASE_DATE() {
      // Creation section.
      final java.lang.String RELEASE_DATE = com.projectmas.gwt.reelvision.shared.Constants.RELEASE_DATE;
      // Setup section.

      return RELEASE_DATE;
    }

    /**
     * Getter for DESCRIPTION called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DESCRIPTION() {
      return build_DESCRIPTION();
    }
    private java.lang.String build_DESCRIPTION() {
      // Creation section.
      final java.lang.String DESCRIPTION = com.projectmas.gwt.reelvision.shared.Constants.DESCRIPTION;
      // Setup section.

      return DESCRIPTION;
    }

    /**
     * Getter for COUNTRY called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_COUNTRY() {
      return build_COUNTRY();
    }
    private java.lang.String build_COUNTRY() {
      // Creation section.
      final java.lang.String COUNTRY = com.projectmas.gwt.reelvision.shared.Constants.COUNTRY;
      // Setup section.

      return COUNTRY;
    }

    /**
     * Getter for ADD_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_MOVIE() {
      return build_ADD_MOVIE();
    }
    private java.lang.String build_ADD_MOVIE() {
      // Creation section.
      final java.lang.String ADD_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.ADD_MOVIE;
      // Setup section.

      return ADD_MOVIE;
    }

    /**
     * Getter for EDIT_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_EDIT_MOVIE() {
      return build_EDIT_MOVIE();
    }
    private java.lang.String build_EDIT_MOVIE() {
      // Creation section.
      final java.lang.String EDIT_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.EDIT_MOVIE;
      // Setup section.

      return EDIT_MOVIE;
    }

    /**
     * Getter for REMOVE_MOVIE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REMOVE_MOVIE() {
      return build_REMOVE_MOVIE();
    }
    private java.lang.String build_REMOVE_MOVIE() {
      // Creation section.
      final java.lang.String REMOVE_MOVIE = com.projectmas.gwt.reelvision.shared.Constants.REMOVE_MOVIE;
      // Setup section.

      return REMOVE_MOVIE;
    }

    /**
     * Getter for MOVIE_ADDED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_ADDED() {
      return build_MOVIE_ADDED();
    }
    private java.lang.String build_MOVIE_ADDED() {
      // Creation section.
      final java.lang.String MOVIE_ADDED = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_ADDED;
      // Setup section.

      return MOVIE_ADDED;
    }

    /**
     * Getter for MOVIE_DELETED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_DELETED() {
      return build_MOVIE_DELETED();
    }
    private java.lang.String build_MOVIE_DELETED() {
      // Creation section.
      final java.lang.String MOVIE_DELETED = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_DELETED;
      // Setup section.

      return MOVIE_DELETED;
    }

    /**
     * Getter for MOVIE_EDITED called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_EDITED() {
      return build_MOVIE_EDITED();
    }
    private java.lang.String build_MOVIE_EDITED() {
      // Creation section.
      final java.lang.String MOVIE_EDITED = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_EDITED;
      // Setup section.

      return MOVIE_EDITED;
    }

    /**
     * Getter for PLEASE_SELECT_MOVIE_TO_DELETE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PLEASE_SELECT_MOVIE_TO_DELETE() {
      return build_PLEASE_SELECT_MOVIE_TO_DELETE();
    }
    private java.lang.String build_PLEASE_SELECT_MOVIE_TO_DELETE() {
      // Creation section.
      final java.lang.String PLEASE_SELECT_MOVIE_TO_DELETE = com.projectmas.gwt.reelvision.shared.Constants.PLEASE_SELECT_MOVIE_TO_DELETE;
      // Setup section.

      return PLEASE_SELECT_MOVIE_TO_DELETE;
    }

    /**
     * Getter for PLEASE_SELECT_MOVIE_TO_EDIT called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_PLEASE_SELECT_MOVIE_TO_EDIT() {
      return build_PLEASE_SELECT_MOVIE_TO_EDIT();
    }
    private java.lang.String build_PLEASE_SELECT_MOVIE_TO_EDIT() {
      // Creation section.
      final java.lang.String PLEASE_SELECT_MOVIE_TO_EDIT = com.projectmas.gwt.reelvision.shared.Constants.PLEASE_SELECT_MOVIE_TO_EDIT;
      // Setup section.

      return PLEASE_SELECT_MOVIE_TO_EDIT;
    }

    /**
     * Getter for ADD_OR_EDIT_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_OR_EDIT_REPERTOIRE() {
      return build_ADD_OR_EDIT_REPERTOIRE();
    }
    private java.lang.String build_ADD_OR_EDIT_REPERTOIRE() {
      // Creation section.
      final java.lang.String ADD_OR_EDIT_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.ADD_OR_EDIT_REPERTOIRE;
      // Setup section.

      return ADD_OR_EDIT_REPERTOIRE;
    }

    /**
     * Getter for REPERTOIRES called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPERTOIRES() {
      return build_REPERTOIRES();
    }
    private java.lang.String build_REPERTOIRES() {
      // Creation section.
      final java.lang.String REPERTOIRES = com.projectmas.gwt.reelvision.shared.Constants.REPERTOIRES;
      // Setup section.

      return REPERTOIRES;
    }

    /**
     * Getter for ADD_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_ADD_REPERTOIRE() {
      return build_ADD_REPERTOIRE();
    }
    private java.lang.String build_ADD_REPERTOIRE() {
      // Creation section.
      final java.lang.String ADD_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.ADD_REPERTOIRE;
      // Setup section.

      return ADD_REPERTOIRE;
    }

    /**
     * Getter for EDIT_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_EDIT_REPERTOIRE() {
      return build_EDIT_REPERTOIRE();
    }
    private java.lang.String build_EDIT_REPERTOIRE() {
      // Creation section.
      final java.lang.String EDIT_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.EDIT_REPERTOIRE;
      // Setup section.

      return EDIT_REPERTOIRE;
    }

    /**
     * Getter for REMOVE_REPERTOIRE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REMOVE_REPERTOIRE() {
      return build_REMOVE_REPERTOIRE();
    }
    private java.lang.String build_REMOVE_REPERTOIRE() {
      // Creation section.
      final java.lang.String REMOVE_REPERTOIRE = com.projectmas.gwt.reelvision.shared.Constants.REMOVE_REPERTOIRE;
      // Setup section.

      return REMOVE_REPERTOIRE;
    }

    /**
     * Getter for START_DATE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_START_DATE() {
      return build_START_DATE();
    }
    private java.lang.String build_START_DATE() {
      // Creation section.
      final java.lang.String START_DATE = com.projectmas.gwt.reelvision.shared.Constants.START_DATE;
      // Setup section.

      return START_DATE;
    }

    /**
     * Getter for END_DATE called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_END_DATE() {
      return build_END_DATE();
    }
    private java.lang.String build_END_DATE() {
      // Creation section.
      final java.lang.String END_DATE = com.projectmas.gwt.reelvision.shared.Constants.END_DATE;
      // Setup section.

      return END_DATE;
    }

    /**
     * Getter for NEW_SALE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_NEW_SALE_ANCHOR_ID() {
      return build_NEW_SALE_ANCHOR_ID();
    }
    private java.lang.String build_NEW_SALE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String NEW_SALE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.NEW_SALE_ANCHOR_ID;
      // Setup section.

      return NEW_SALE_ANCHOR_ID;
    }

    /**
     * Getter for SEARCH_SALE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SEARCH_SALE_ANCHOR_ID() {
      return build_SEARCH_SALE_ANCHOR_ID();
    }
    private java.lang.String build_SEARCH_SALE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String SEARCH_SALE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.SEARCH_SALE_ANCHOR_ID;
      // Setup section.

      return SEARCH_SALE_ANCHOR_ID;
    }

    /**
     * Getter for NEW_RESERVE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_NEW_RESERVE_ANCHOR_ID() {
      return build_NEW_RESERVE_ANCHOR_ID();
    }
    private java.lang.String build_NEW_RESERVE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String NEW_RESERVE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.NEW_RESERVE_ANCHOR_ID;
      // Setup section.

      return NEW_RESERVE_ANCHOR_ID;
    }

    /**
     * Getter for SEARCH_RESERVE_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SEARCH_RESERVE_ANCHOR_ID() {
      return build_SEARCH_RESERVE_ANCHOR_ID();
    }
    private java.lang.String build_SEARCH_RESERVE_ANCHOR_ID() {
      // Creation section.
      final java.lang.String SEARCH_RESERVE_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.SEARCH_RESERVE_ANCHOR_ID;
      // Setup section.

      return SEARCH_RESERVE_ANCHOR_ID;
    }

    /**
     * Getter for DASHBOARD_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_DASHBOARD_ANCHOR_ID() {
      return build_DASHBOARD_ANCHOR_ID();
    }
    private java.lang.String build_DASHBOARD_ANCHOR_ID() {
      // Creation section.
      final java.lang.String DASHBOARD_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.DASHBOARD_ANCHOR_ID;
      // Setup section.

      return DASHBOARD_ANCHOR_ID;
    }

    /**
     * Getter for SHOWTIME_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIME_ANCHOR_ID() {
      return build_SHOWTIME_ANCHOR_ID();
    }
    private java.lang.String build_SHOWTIME_ANCHOR_ID() {
      // Creation section.
      final java.lang.String SHOWTIME_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIME_ANCHOR_ID;
      // Setup section.

      return SHOWTIME_ANCHOR_ID;
    }

    /**
     * Getter for MOVIES_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIES_ANCHOR_ID() {
      return build_MOVIES_ANCHOR_ID();
    }
    private java.lang.String build_MOVIES_ANCHOR_ID() {
      // Creation section.
      final java.lang.String MOVIES_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.MOVIES_ANCHOR_ID;
      // Setup section.

      return MOVIES_ANCHOR_ID;
    }

    /**
     * Getter for AUDIENCES_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_AUDIENCES_ANCHOR_ID() {
      return build_AUDIENCES_ANCHOR_ID();
    }
    private java.lang.String build_AUDIENCES_ANCHOR_ID() {
      // Creation section.
      final java.lang.String AUDIENCES_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.AUDIENCES_ANCHOR_ID;
      // Setup section.

      return AUDIENCES_ANCHOR_ID;
    }

    /**
     * Getter for REPORTS_ANCHOR_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPORTS_ANCHOR_ID() {
      return build_REPORTS_ANCHOR_ID();
    }
    private java.lang.String build_REPORTS_ANCHOR_ID() {
      // Creation section.
      final java.lang.String REPORTS_ANCHOR_ID = com.projectmas.gwt.reelvision.shared.Constants.REPORTS_ANCHOR_ID;
      // Setup section.

      return REPORTS_ANCHOR_ID;
    }

    /**
     * Getter for SHOWTIME_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_SHOWTIME_MANAGEMENT_ID() {
      return build_SHOWTIME_MANAGEMENT_ID();
    }
    private java.lang.String build_SHOWTIME_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String SHOWTIME_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.SHOWTIME_MANAGEMENT_ID;
      // Setup section.

      return SHOWTIME_MANAGEMENT_ID;
    }

    /**
     * Getter for MOVIE_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_MOVIE_MANAGEMENT_ID() {
      return build_MOVIE_MANAGEMENT_ID();
    }
    private java.lang.String build_MOVIE_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String MOVIE_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.MOVIE_MANAGEMENT_ID;
      // Setup section.

      return MOVIE_MANAGEMENT_ID;
    }

    /**
     * Getter for USER_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_MANAGEMENT_ID() {
      return build_USER_MANAGEMENT_ID();
    }
    private java.lang.String build_USER_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String USER_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.USER_MANAGEMENT_ID;
      // Setup section.

      return USER_MANAGEMENT_ID;
    }

    /**
     * Getter for REPERTOIRE_MANAGEMENT_ID called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_REPERTOIRE_MANAGEMENT_ID() {
      return build_REPERTOIRE_MANAGEMENT_ID();
    }
    private java.lang.String build_REPERTOIRE_MANAGEMENT_ID() {
      // Creation section.
      final java.lang.String REPERTOIRE_MANAGEMENT_ID = com.projectmas.gwt.reelvision.shared.Constants.REPERTOIRE_MANAGEMENT_ID;
      // Setup section.

      return REPERTOIRE_MANAGEMENT_ID;
    }

    /**
     * Getter for USER_SESSION_ATTR called 0 times. Type: DEFAULT. Build precedence: 1.
     */
    private java.lang.String get_USER_SESSION_ATTR() {
      return build_USER_SESSION_ATTR();
    }
    private java.lang.String build_USER_SESSION_ATTR() {
      // Creation section.
      final java.lang.String USER_SESSION_ATTR = com.projectmas.gwt.reelvision.shared.Constants.USER_SESSION_ATTR;
      // Setup section.

      return USER_SESSION_ATTR;
    }

    /**
     * Getter for f_Container1 called 1 times. Type: DEFAULT. Build precedence: 1.
     */
    private org.gwtbootstrap3.client.ui.Container get_f_Container1() {
      return build_f_Container1();
    }
    private org.gwtbootstrap3.client.ui.Container build_f_Container1() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Container f_Container1 = (org.gwtbootstrap3.client.ui.Container) GWT.create(org.gwtbootstrap3.client.ui.Container.class);
      // Setup section.
      f_Container1.add(get_f_Heading2());
      f_Container1.add(get_f_Row3());
      f_Container1.add(get_f_Row22());
      f_Container1.add(get_f_Row27());
      f_Container1.add(get_f_Row32());
      f_Container1.setFluid(true);

      return f_Container1;
    }

    /**
     * Getter for f_Heading2 called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private org.gwtbootstrap3.client.ui.Heading get_f_Heading2() {
      return build_f_Heading2();
    }
    private org.gwtbootstrap3.client.ui.Heading build_f_Heading2() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Heading f_Heading2 = new org.gwtbootstrap3.client.ui.Heading(org.gwtbootstrap3.client.ui.constants.HeadingSize.H1);
      // Setup section.
      f_Heading2.setStyleName("page-header");
      f_Heading2.setText("" + get_SALE() + "");

      return f_Heading2;
    }

    /**
     * Getter for f_Row3 called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private org.gwtbootstrap3.client.ui.Row get_f_Row3() {
      return build_f_Row3();
    }
    private org.gwtbootstrap3.client.ui.Row build_f_Row3() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Row f_Row3 = (org.gwtbootstrap3.client.ui.Row) GWT.create(org.gwtbootstrap3.client.ui.Row.class);
      // Setup section.
      f_Row3.add(get_f_Column4());
      f_Row3.add(get_f_Column18());

      return f_Row3;
    }

    /**
     * Getter for f_Column4 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.Column get_f_Column4() {
      return build_f_Column4();
    }
    private org.gwtbootstrap3.client.ui.Column build_f_Column4() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Column f_Column4 = new org.gwtbootstrap3.client.ui.Column("0");
      // Setup section.
      f_Column4.add(get_f_Panel5());
      f_Column4.setStyleName("col-lg-3");

      return f_Column4;
    }

    /**
     * Getter for f_Panel5 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Panel get_f_Panel5() {
      return build_f_Panel5();
    }
    private org.gwtbootstrap3.client.ui.Panel build_f_Panel5() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Panel f_Panel5 = (org.gwtbootstrap3.client.ui.Panel) GWT.create(org.gwtbootstrap3.client.ui.Panel.class);
      // Setup section.
      f_Panel5.add(get_f_Panel6());
      f_Panel5.add(get_f_Panel8());
      f_Panel5.addStyleName("panel-default");

      return f_Panel5;
    }

    /**
     * Getter for f_Panel6 called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.Panel get_f_Panel6() {
      return build_f_Panel6();
    }
    private org.gwtbootstrap3.client.ui.Panel build_f_Panel6() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Panel f_Panel6 = (org.gwtbootstrap3.client.ui.Panel) GWT.create(org.gwtbootstrap3.client.ui.Panel.class);
      // Setup section.
      f_Panel6.add(get_f_Heading7());
      f_Panel6.addStyleDependentName("heading");

      return f_Panel6;
    }

    /**
     * Getter for f_Heading7 called 1 times. Type: DEFAULT. Build precedence: 6.
     */
    private org.gwtbootstrap3.client.ui.Heading get_f_Heading7() {
      return build_f_Heading7();
    }
    private org.gwtbootstrap3.client.ui.Heading build_f_Heading7() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Heading f_Heading7 = new org.gwtbootstrap3.client.ui.Heading(org.gwtbootstrap3.client.ui.constants.HeadingSize.H3);
      // Setup section.
      f_Heading7.setText("Repertoire");

      return f_Heading7;
    }

    /**
     * Getter for f_Panel8 called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.Panel get_f_Panel8() {
      return build_f_Panel8();
    }
    private org.gwtbootstrap3.client.ui.Panel build_f_Panel8() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Panel f_Panel8 = (org.gwtbootstrap3.client.ui.Panel) GWT.create(org.gwtbootstrap3.client.ui.Panel.class);
      // Setup section.
      f_Panel8.add(get_saleForm());
      f_Panel8.addStyleDependentName("body");

      return f_Panel8;
    }

    /**
     * Getter for saleForm called 1 times. Type: DEFAULT. Build precedence: 6.
     */
    private org.gwtbootstrap3.client.ui.Form get_saleForm() {
      return build_saleForm();
    }
    private org.gwtbootstrap3.client.ui.Form build_saleForm() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Form saleForm = (org.gwtbootstrap3.client.ui.Form) GWT.create(org.gwtbootstrap3.client.ui.Form.class);
      // Setup section.
      saleForm.add(get_f_FormGroup9());
      saleForm.add(get_f_FormGroup12());
      saleForm.add(get_f_FormGroup15());
      saleForm.addStyleName("form-horizontal");

      return saleForm;
    }

    /**
     * Getter for f_FormGroup9 called 1 times. Type: DEFAULT. Build precedence: 7.
     */
    private org.gwtbootstrap3.client.ui.FormGroup get_f_FormGroup9() {
      return build_f_FormGroup9();
    }
    private org.gwtbootstrap3.client.ui.FormGroup build_f_FormGroup9() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.FormGroup f_FormGroup9 = (org.gwtbootstrap3.client.ui.FormGroup) GWT.create(org.gwtbootstrap3.client.ui.FormGroup.class);
      // Setup section.
      f_FormGroup9.add(get_f_FormLabel10());
      f_FormGroup9.add(get_f_Column11());

      return f_FormGroup9;
    }

    /**
     * Getter for f_FormLabel10 called 1 times. Type: DEFAULT. Build precedence: 8.
     */
    private org.gwtbootstrap3.client.ui.FormLabel get_f_FormLabel10() {
      return build_f_FormLabel10();
    }
    private org.gwtbootstrap3.client.ui.FormLabel build_f_FormLabel10() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.FormLabel f_FormLabel10 = (org.gwtbootstrap3.client.ui.FormLabel) GWT.create(org.gwtbootstrap3.client.ui.FormLabel.class);
      // Setup section.
      f_FormLabel10.addStyleName("control-label");
      f_FormLabel10.addStyleName("col-sm-2");
      f_FormLabel10.setText("Reper.");

      return f_FormLabel10;
    }

    /**
     * Getter for f_Column11 called 1 times. Type: DEFAULT. Build precedence: 8.
     */
    private org.gwtbootstrap3.client.ui.Column get_f_Column11() {
      return build_f_Column11();
    }
    private org.gwtbootstrap3.client.ui.Column build_f_Column11() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Column f_Column11 = new org.gwtbootstrap3.client.ui.Column("0");
      // Setup section.
      f_Column11.add(get_repertoireListBox());
      f_Column11.addStyleName("col-sm-10");

      return f_Column11;
    }

    /**
     * Getter for repertoireListBox called 1 times. Type: DEFAULT. Build precedence: 9.
     */
    private com.projectmas.gwt.reelvision.client.ui.component.CustomListBox get_repertoireListBox() {
      return build_repertoireListBox();
    }
    private com.projectmas.gwt.reelvision.client.ui.component.CustomListBox build_repertoireListBox() {
      // Creation section.
      final com.projectmas.gwt.reelvision.client.ui.component.CustomListBox repertoireListBox = (com.projectmas.gwt.reelvision.client.ui.component.CustomListBox) GWT.create(com.projectmas.gwt.reelvision.client.ui.component.CustomListBox.class);
      // Setup section.

      this.owner.repertoireListBox = repertoireListBox;

      return repertoireListBox;
    }

    /**
     * Getter for f_FormGroup12 called 1 times. Type: DEFAULT. Build precedence: 7.
     */
    private org.gwtbootstrap3.client.ui.FormGroup get_f_FormGroup12() {
      return build_f_FormGroup12();
    }
    private org.gwtbootstrap3.client.ui.FormGroup build_f_FormGroup12() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.FormGroup f_FormGroup12 = (org.gwtbootstrap3.client.ui.FormGroup) GWT.create(org.gwtbootstrap3.client.ui.FormGroup.class);
      // Setup section.
      f_FormGroup12.add(get_f_FormLabel13());
      f_FormGroup12.add(get_f_Column14());

      return f_FormGroup12;
    }

    /**
     * Getter for f_FormLabel13 called 1 times. Type: DEFAULT. Build precedence: 8.
     */
    private org.gwtbootstrap3.client.ui.FormLabel get_f_FormLabel13() {
      return build_f_FormLabel13();
    }
    private org.gwtbootstrap3.client.ui.FormLabel build_f_FormLabel13() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.FormLabel f_FormLabel13 = (org.gwtbootstrap3.client.ui.FormLabel) GWT.create(org.gwtbootstrap3.client.ui.FormLabel.class);
      // Setup section.
      f_FormLabel13.addStyleName("control-label");
      f_FormLabel13.addStyleName("col-sm-2");
      f_FormLabel13.setText("Movie");

      return f_FormLabel13;
    }

    /**
     * Getter for f_Column14 called 1 times. Type: DEFAULT. Build precedence: 8.
     */
    private org.gwtbootstrap3.client.ui.Column get_f_Column14() {
      return build_f_Column14();
    }
    private org.gwtbootstrap3.client.ui.Column build_f_Column14() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Column f_Column14 = new org.gwtbootstrap3.client.ui.Column("0");
      // Setup section.
      f_Column14.add(get_movieListBox());
      f_Column14.addStyleName("col-sm-10");

      return f_Column14;
    }

    /**
     * Getter for movieListBox called 1 times. Type: DEFAULT. Build precedence: 9.
     */
    private com.projectmas.gwt.reelvision.client.ui.component.CustomListBox get_movieListBox() {
      return build_movieListBox();
    }
    private com.projectmas.gwt.reelvision.client.ui.component.CustomListBox build_movieListBox() {
      // Creation section.
      final com.projectmas.gwt.reelvision.client.ui.component.CustomListBox movieListBox = (com.projectmas.gwt.reelvision.client.ui.component.CustomListBox) GWT.create(com.projectmas.gwt.reelvision.client.ui.component.CustomListBox.class);
      // Setup section.

      this.owner.movieListBox = movieListBox;

      return movieListBox;
    }

    /**
     * Getter for f_FormGroup15 called 1 times. Type: DEFAULT. Build precedence: 7.
     */
    private org.gwtbootstrap3.client.ui.FormGroup get_f_FormGroup15() {
      return build_f_FormGroup15();
    }
    private org.gwtbootstrap3.client.ui.FormGroup build_f_FormGroup15() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.FormGroup f_FormGroup15 = (org.gwtbootstrap3.client.ui.FormGroup) GWT.create(org.gwtbootstrap3.client.ui.FormGroup.class);
      // Setup section.
      f_FormGroup15.add(get_f_FormLabel16());
      f_FormGroup15.add(get_f_Column17());

      return f_FormGroup15;
    }

    /**
     * Getter for f_FormLabel16 called 1 times. Type: DEFAULT. Build precedence: 8.
     */
    private org.gwtbootstrap3.client.ui.FormLabel get_f_FormLabel16() {
      return build_f_FormLabel16();
    }
    private org.gwtbootstrap3.client.ui.FormLabel build_f_FormLabel16() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.FormLabel f_FormLabel16 = (org.gwtbootstrap3.client.ui.FormLabel) GWT.create(org.gwtbootstrap3.client.ui.FormLabel.class);
      // Setup section.
      f_FormLabel16.addStyleName("control-label");
      f_FormLabel16.addStyleName("col-sm-2");
      f_FormLabel16.setText("Hour");

      return f_FormLabel16;
    }

    /**
     * Getter for f_Column17 called 1 times. Type: DEFAULT. Build precedence: 8.
     */
    private org.gwtbootstrap3.client.ui.Column get_f_Column17() {
      return build_f_Column17();
    }
    private org.gwtbootstrap3.client.ui.Column build_f_Column17() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Column f_Column17 = new org.gwtbootstrap3.client.ui.Column("0");
      // Setup section.
      f_Column17.add(get_screeningListBox());
      f_Column17.addStyleName("col-sm-10");

      return f_Column17;
    }

    /**
     * Getter for screeningListBox called 1 times. Type: DEFAULT. Build precedence: 9.
     */
    private com.projectmas.gwt.reelvision.client.ui.component.CustomListBox get_screeningListBox() {
      return build_screeningListBox();
    }
    private com.projectmas.gwt.reelvision.client.ui.component.CustomListBox build_screeningListBox() {
      // Creation section.
      final com.projectmas.gwt.reelvision.client.ui.component.CustomListBox screeningListBox = (com.projectmas.gwt.reelvision.client.ui.component.CustomListBox) GWT.create(com.projectmas.gwt.reelvision.client.ui.component.CustomListBox.class);
      // Setup section.

      this.owner.screeningListBox = screeningListBox;

      return screeningListBox;
    }

    /**
     * Getter for f_Column18 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.Column get_f_Column18() {
      return build_f_Column18();
    }
    private org.gwtbootstrap3.client.ui.Column build_f_Column18() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Column f_Column18 = new org.gwtbootstrap3.client.ui.Column("0");
      // Setup section.
      f_Column18.add(get_f_Panel19());
      f_Column18.setStyleName("col-lg-6");

      return f_Column18;
    }

    /**
     * Getter for f_Panel19 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Panel get_f_Panel19() {
      return build_f_Panel19();
    }
    private org.gwtbootstrap3.client.ui.Panel build_f_Panel19() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Panel f_Panel19 = (org.gwtbootstrap3.client.ui.Panel) GWT.create(org.gwtbootstrap3.client.ui.Panel.class);
      // Setup section.
      f_Panel19.add(get_f_Panel20());
      f_Panel19.add(get_seatingPlanPanel());
      f_Panel19.addStyleName("panel-default");

      return f_Panel19;
    }

    /**
     * Getter for f_Panel20 called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.Panel get_f_Panel20() {
      return build_f_Panel20();
    }
    private org.gwtbootstrap3.client.ui.Panel build_f_Panel20() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Panel f_Panel20 = (org.gwtbootstrap3.client.ui.Panel) GWT.create(org.gwtbootstrap3.client.ui.Panel.class);
      // Setup section.
      f_Panel20.add(get_f_Heading21());
      f_Panel20.addStyleDependentName("heading");

      return f_Panel20;
    }

    /**
     * Getter for f_Heading21 called 1 times. Type: DEFAULT. Build precedence: 6.
     */
    private org.gwtbootstrap3.client.ui.Heading get_f_Heading21() {
      return build_f_Heading21();
    }
    private org.gwtbootstrap3.client.ui.Heading build_f_Heading21() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Heading f_Heading21 = new org.gwtbootstrap3.client.ui.Heading(org.gwtbootstrap3.client.ui.constants.HeadingSize.H3);
      // Setup section.
      f_Heading21.setText("Seats");

      return f_Heading21;
    }

    /**
     * Getter for seatingPlanPanel called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.Panel get_seatingPlanPanel() {
      return build_seatingPlanPanel();
    }
    private org.gwtbootstrap3.client.ui.Panel build_seatingPlanPanel() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Panel seatingPlanPanel = (org.gwtbootstrap3.client.ui.Panel) GWT.create(org.gwtbootstrap3.client.ui.Panel.class);
      // Setup section.
      seatingPlanPanel.addStyleDependentName("body");

      this.owner.seatingPlanPanel = seatingPlanPanel;

      return seatingPlanPanel;
    }

    /**
     * Getter for f_Row22 called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private org.gwtbootstrap3.client.ui.Row get_f_Row22() {
      return build_f_Row22();
    }
    private org.gwtbootstrap3.client.ui.Row build_f_Row22() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Row f_Row22 = (org.gwtbootstrap3.client.ui.Row) GWT.create(org.gwtbootstrap3.client.ui.Row.class);
      // Setup section.
      f_Row22.add(get_f_Column23());

      return f_Row22;
    }

    /**
     * Getter for f_Column23 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.Column get_f_Column23() {
      return build_f_Column23();
    }
    private org.gwtbootstrap3.client.ui.Column build_f_Column23() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Column f_Column23 = new org.gwtbootstrap3.client.ui.Column("0");
      // Setup section.
      f_Column23.add(get_f_Form24());
      f_Column23.setStyleName("col-lg-12");

      return f_Column23;
    }

    /**
     * Getter for f_Form24 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Form get_f_Form24() {
      return build_f_Form24();
    }
    private org.gwtbootstrap3.client.ui.Form build_f_Form24() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Form f_Form24 = (org.gwtbootstrap3.client.ui.Form) GWT.create(org.gwtbootstrap3.client.ui.Form.class);
      // Setup section.
      f_Form24.add(get_f_FormGroup25());
      f_Form24.add(get_f_FormGroup26());

      return f_Form24;
    }

    /**
     * Getter for f_FormGroup25 called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.FormGroup get_f_FormGroup25() {
      return build_f_FormGroup25();
    }
    private org.gwtbootstrap3.client.ui.FormGroup build_f_FormGroup25() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.FormGroup f_FormGroup25 = (org.gwtbootstrap3.client.ui.FormGroup) GWT.create(org.gwtbootstrap3.client.ui.FormGroup.class);
      // Setup section.

      return f_FormGroup25;
    }

    /**
     * Getter for f_FormGroup26 called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.FormGroup get_f_FormGroup26() {
      return build_f_FormGroup26();
    }
    private org.gwtbootstrap3.client.ui.FormGroup build_f_FormGroup26() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.FormGroup f_FormGroup26 = (org.gwtbootstrap3.client.ui.FormGroup) GWT.create(org.gwtbootstrap3.client.ui.FormGroup.class);
      // Setup section.
      f_FormGroup26.add(get_confirmSeatsButton());

      return f_FormGroup26;
    }

    /**
     * Getter for confirmSeatsButton called 1 times. Type: DEFAULT. Build precedence: 6.
     */
    private org.gwtbootstrap3.client.ui.Button get_confirmSeatsButton() {
      return build_confirmSeatsButton();
    }
    private org.gwtbootstrap3.client.ui.Button build_confirmSeatsButton() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Button confirmSeatsButton = (org.gwtbootstrap3.client.ui.Button) GWT.create(org.gwtbootstrap3.client.ui.Button.class);
      // Setup section.
      confirmSeatsButton.addStyleDependentName("primary");
      confirmSeatsButton.setText("Confirm Seats");

      this.owner.confirmSeatsButton = confirmSeatsButton;

      return confirmSeatsButton;
    }

    /**
     * Getter for f_Row27 called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private org.gwtbootstrap3.client.ui.Row get_f_Row27() {
      return build_f_Row27();
    }
    private org.gwtbootstrap3.client.ui.Row build_f_Row27() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Row f_Row27 = (org.gwtbootstrap3.client.ui.Row) GWT.create(org.gwtbootstrap3.client.ui.Row.class);
      // Setup section.
      f_Row27.add(get_f_Column28());

      return f_Row27;
    }

    /**
     * Getter for f_Column28 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.Column get_f_Column28() {
      return build_f_Column28();
    }
    private org.gwtbootstrap3.client.ui.Column build_f_Column28() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Column f_Column28 = new org.gwtbootstrap3.client.ui.Column("0");
      // Setup section.
      f_Column28.add(get_f_Panel29());
      f_Column28.setStyleName("col-lg-12");

      return f_Column28;
    }

    /**
     * Getter for f_Panel29 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Panel get_f_Panel29() {
      return build_f_Panel29();
    }
    private org.gwtbootstrap3.client.ui.Panel build_f_Panel29() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Panel f_Panel29 = (org.gwtbootstrap3.client.ui.Panel) GWT.create(org.gwtbootstrap3.client.ui.Panel.class);
      // Setup section.
      f_Panel29.add(get_f_Panel30());
      f_Panel29.add(get_ticketsPanel());
      f_Panel29.addStyleName("panel-default");

      return f_Panel29;
    }

    /**
     * Getter for f_Panel30 called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.Panel get_f_Panel30() {
      return build_f_Panel30();
    }
    private org.gwtbootstrap3.client.ui.Panel build_f_Panel30() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Panel f_Panel30 = (org.gwtbootstrap3.client.ui.Panel) GWT.create(org.gwtbootstrap3.client.ui.Panel.class);
      // Setup section.
      f_Panel30.add(get_f_Heading31());
      f_Panel30.addStyleDependentName("heading");

      return f_Panel30;
    }

    /**
     * Getter for f_Heading31 called 1 times. Type: DEFAULT. Build precedence: 6.
     */
    private org.gwtbootstrap3.client.ui.Heading get_f_Heading31() {
      return build_f_Heading31();
    }
    private org.gwtbootstrap3.client.ui.Heading build_f_Heading31() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Heading f_Heading31 = new org.gwtbootstrap3.client.ui.Heading(org.gwtbootstrap3.client.ui.constants.HeadingSize.H2);
      // Setup section.
      f_Heading31.setText("Tickets");

      return f_Heading31;
    }

    /**
     * Getter for ticketsPanel called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.Panel get_ticketsPanel() {
      return build_ticketsPanel();
    }
    private org.gwtbootstrap3.client.ui.Panel build_ticketsPanel() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Panel ticketsPanel = (org.gwtbootstrap3.client.ui.Panel) GWT.create(org.gwtbootstrap3.client.ui.Panel.class);
      // Setup section.
      ticketsPanel.addStyleDependentName("body");

      this.owner.ticketsPanel = ticketsPanel;

      return ticketsPanel;
    }

    /**
     * Getter for f_Row32 called 1 times. Type: DEFAULT. Build precedence: 2.
     */
    private org.gwtbootstrap3.client.ui.Row get_f_Row32() {
      return build_f_Row32();
    }
    private org.gwtbootstrap3.client.ui.Row build_f_Row32() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Row f_Row32 = (org.gwtbootstrap3.client.ui.Row) GWT.create(org.gwtbootstrap3.client.ui.Row.class);
      // Setup section.
      f_Row32.add(get_f_Column33());

      return f_Row32;
    }

    /**
     * Getter for f_Column33 called 1 times. Type: DEFAULT. Build precedence: 3.
     */
    private org.gwtbootstrap3.client.ui.Column get_f_Column33() {
      return build_f_Column33();
    }
    private org.gwtbootstrap3.client.ui.Column build_f_Column33() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Column f_Column33 = new org.gwtbootstrap3.client.ui.Column("0");
      // Setup section.
      f_Column33.add(get_f_Form34());
      f_Column33.setStyleName("col-lg-12");

      return f_Column33;
    }

    /**
     * Getter for f_Form34 called 1 times. Type: DEFAULT. Build precedence: 4.
     */
    private org.gwtbootstrap3.client.ui.Form get_f_Form34() {
      return build_f_Form34();
    }
    private org.gwtbootstrap3.client.ui.Form build_f_Form34() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Form f_Form34 = (org.gwtbootstrap3.client.ui.Form) GWT.create(org.gwtbootstrap3.client.ui.Form.class);
      // Setup section.
      f_Form34.add(get_f_FormGroup35());
      f_Form34.add(get_f_FormGroup36());

      return f_Form34;
    }

    /**
     * Getter for f_FormGroup35 called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.FormGroup get_f_FormGroup35() {
      return build_f_FormGroup35();
    }
    private org.gwtbootstrap3.client.ui.FormGroup build_f_FormGroup35() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.FormGroup f_FormGroup35 = (org.gwtbootstrap3.client.ui.FormGroup) GWT.create(org.gwtbootstrap3.client.ui.FormGroup.class);
      // Setup section.

      return f_FormGroup35;
    }

    /**
     * Getter for f_FormGroup36 called 1 times. Type: DEFAULT. Build precedence: 5.
     */
    private org.gwtbootstrap3.client.ui.FormGroup get_f_FormGroup36() {
      return build_f_FormGroup36();
    }
    private org.gwtbootstrap3.client.ui.FormGroup build_f_FormGroup36() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.FormGroup f_FormGroup36 = (org.gwtbootstrap3.client.ui.FormGroup) GWT.create(org.gwtbootstrap3.client.ui.FormGroup.class);
      // Setup section.
      f_FormGroup36.add(get_confirmButton());

      return f_FormGroup36;
    }

    /**
     * Getter for confirmButton called 1 times. Type: DEFAULT. Build precedence: 6.
     */
    private org.gwtbootstrap3.client.ui.Button get_confirmButton() {
      return build_confirmButton();
    }
    private org.gwtbootstrap3.client.ui.Button build_confirmButton() {
      // Creation section.
      final org.gwtbootstrap3.client.ui.Button confirmButton = (org.gwtbootstrap3.client.ui.Button) GWT.create(org.gwtbootstrap3.client.ui.Button.class);
      // Setup section.
      confirmButton.addStyleDependentName("primary");
      confirmButton.setText("Confirm Sale");

      this.owner.confirmButton = confirmButton;

      return confirmButton;
    }
  }
}
