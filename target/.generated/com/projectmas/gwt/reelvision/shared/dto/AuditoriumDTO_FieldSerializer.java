package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class AuditoriumDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getId(com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO::id;
  }-*/;
  
  private static native void setId(com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO instance, java.lang.Long value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO::id = value;
  }-*/;
  
  private static native java.lang.String getName(com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO::name;
  }-*/;
  
  private static native void setName(com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO::name = value;
  }-*/;
  
  private static native java.lang.Integer getNo(com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO::no;
  }-*/;
  
  private static native void setNo(com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO instance, java.lang.Integer value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO::no = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO instance) throws SerializationException {
    setId(instance, (java.lang.Long) streamReader.readObject());
    setName(instance, streamReader.readString());
    setNo(instance, (java.lang.Integer) streamReader.readObject());
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO instance) throws SerializationException {
    streamWriter.writeObject(getId(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeObject(getNo(instance));
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO)object);
  }
  
}
