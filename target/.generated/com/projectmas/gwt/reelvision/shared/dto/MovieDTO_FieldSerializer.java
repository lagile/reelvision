package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class MovieDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCountry(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::country;
  }-*/;
  
  private static native void setCountry(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::country = value;
  }-*/;
  
  private static native java.lang.String getDescription(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::description;
  }-*/;
  
  private static native void setDescription(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::description = value;
  }-*/;
  
  private static native java.lang.String getDirector(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::director;
  }-*/;
  
  private static native void setDirector(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::director = value;
  }-*/;
  
  private static native java.lang.Long getId(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::id;
  }-*/;
  
  private static native void setId(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance, java.lang.Long value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::id = value;
  }-*/;
  
  private static native java.util.Date getReleaseDate(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::releaseDate;
  }-*/;
  
  private static native void setReleaseDate(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance, java.util.Date value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::releaseDate = value;
  }-*/;
  
  private static native java.lang.String getTitle(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::title;
  }-*/;
  
  private static native void setTitle(com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.MovieDTO::title = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance) throws SerializationException {
    setCountry(instance, streamReader.readString());
    setDescription(instance, streamReader.readString());
    setDirector(instance, streamReader.readString());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setReleaseDate(instance, (java.util.Date) streamReader.readObject());
    setTitle(instance, streamReader.readString());
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.MovieDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.projectmas.gwt.reelvision.shared.dto.MovieDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.MovieDTO instance) throws SerializationException {
    streamWriter.writeString(getCountry(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeString(getDirector(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getReleaseDate(instance));
    streamWriter.writeString(getTitle(instance));
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.MovieDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.MovieDTO_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.MovieDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.MovieDTO_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.MovieDTO)object);
  }
  
}
