package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ObjectDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.ObjectDTO instance) throws SerializationException {
    
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.ObjectDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.projectmas.gwt.reelvision.shared.dto.ObjectDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.ObjectDTO instance) throws SerializationException {
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.ObjectDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.ObjectDTO)object);
  }
  
}
