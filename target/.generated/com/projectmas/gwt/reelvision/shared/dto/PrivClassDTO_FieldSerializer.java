package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class PrivClassDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getClassName(com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO::className;
  }-*/;
  
  private static native void setClassName(com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO::className = value;
  }-*/;
  
  private static native java.lang.String getDescription(com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO::description;
  }-*/;
  
  private static native void setDescription(com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO::description = value;
  }-*/;
  
  private static native java.lang.Long getId(com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO::id;
  }-*/;
  
  private static native void setId(com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO instance, java.lang.Long value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO::id = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO instance) throws SerializationException {
    setClassName(instance, streamReader.readString());
    setDescription(instance, streamReader.readString());
    setId(instance, (java.lang.Long) streamReader.readObject());
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO instance) throws SerializationException {
    streamWriter.writeString(getClassName(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeObject(getId(instance));
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO)object);
  }
  
}
