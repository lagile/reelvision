package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class RepertoireDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getEndDate(com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO::endDate;
  }-*/;
  
  private static native void setEndDate(com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instance, java.util.Date value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO::endDate = value;
  }-*/;
  
  private static native java.lang.Long getId(com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO::id;
  }-*/;
  
  private static native void setId(com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instance, java.lang.Long value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO::id = value;
  }-*/;
  
  private static native java.util.Date getStartDate(com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO::startDate;
  }-*/;
  
  private static native void setStartDate(com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instance, java.util.Date value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO::startDate = value;
  }-*/;
  
  private static native java.lang.String getTitle(com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO::title;
  }-*/;
  
  private static native void setTitle(com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO::title = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instance) throws SerializationException {
    setEndDate(instance, (java.util.Date) streamReader.readObject());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setStartDate(instance, (java.util.Date) streamReader.readObject());
    setTitle(instance, streamReader.readString());
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO instance) throws SerializationException {
    streamWriter.writeObject(getEndDate(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getStartDate(instance));
    streamWriter.writeString(getTitle(instance));
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO)object);
  }
  
}
