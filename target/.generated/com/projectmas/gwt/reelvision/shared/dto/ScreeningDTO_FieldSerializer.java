package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ScreeningDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO getAuditorium(com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO::auditorium;
  }-*/;
  
  private static native void setAuditorium(com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instance, com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO::auditorium = value;
  }-*/;
  
  private static native java.lang.Long getId(com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO::id;
  }-*/;
  
  private static native void setId(com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instance, java.lang.Long value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO::id = value;
  }-*/;
  
  private static native java.util.Date getScreeningDate(com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO::screeningDate;
  }-*/;
  
  private static native void setScreeningDate(com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instance, java.util.Date value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO::screeningDate = value;
  }-*/;
  
  private static native com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO getShowcase(com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO::showcase;
  }-*/;
  
  private static native void setShowcase(com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instance, com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO::showcase = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instance) throws SerializationException {
    setAuditorium(instance, (com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO) streamReader.readObject());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setScreeningDate(instance, (java.util.Date) streamReader.readObject());
    setShowcase(instance, (com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO) streamReader.readObject());
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO instance) throws SerializationException {
    streamWriter.writeObject(getAuditorium(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getScreeningDate(instance));
    streamWriter.writeObject(getShowcase(instance));
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO)object);
  }
  
}
