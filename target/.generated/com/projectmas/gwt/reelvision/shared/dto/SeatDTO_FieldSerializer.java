package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class SeatDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO getAuditorium(com.projectmas.gwt.reelvision.shared.dto.SeatDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.SeatDTO::auditorium;
  }-*/;
  
  private static native void setAuditorium(com.projectmas.gwt.reelvision.shared.dto.SeatDTO instance, com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.SeatDTO::auditorium = value;
  }-*/;
  
  private static native java.lang.Long getId(com.projectmas.gwt.reelvision.shared.dto.SeatDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.SeatDTO::id;
  }-*/;
  
  private static native void setId(com.projectmas.gwt.reelvision.shared.dto.SeatDTO instance, java.lang.Long value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.SeatDTO::id = value;
  }-*/;
  
  private static native java.lang.Integer getNo(com.projectmas.gwt.reelvision.shared.dto.SeatDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.SeatDTO::no;
  }-*/;
  
  private static native void setNo(com.projectmas.gwt.reelvision.shared.dto.SeatDTO instance, java.lang.Integer value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.SeatDTO::no = value;
  }-*/;
  
  private static native java.lang.Character getRow(com.projectmas.gwt.reelvision.shared.dto.SeatDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.SeatDTO::row;
  }-*/;
  
  private static native void setRow(com.projectmas.gwt.reelvision.shared.dto.SeatDTO instance, java.lang.Character value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.SeatDTO::row = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.SeatDTO instance) throws SerializationException {
    setAuditorium(instance, (com.projectmas.gwt.reelvision.shared.dto.AuditoriumDTO) streamReader.readObject());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setNo(instance, (java.lang.Integer) streamReader.readObject());
    setRow(instance, (java.lang.Character) streamReader.readObject());
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.SeatDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.projectmas.gwt.reelvision.shared.dto.SeatDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.SeatDTO instance) throws SerializationException {
    streamWriter.writeObject(getAuditorium(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getNo(instance));
    streamWriter.writeObject(getRow(instance));
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.SeatDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.SeatDTO_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.SeatDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.SeatDTO_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.SeatDTO)object);
  }
  
}
