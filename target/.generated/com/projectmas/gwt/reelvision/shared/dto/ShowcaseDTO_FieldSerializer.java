package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ShowcaseDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getId(com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO::id;
  }-*/;
  
  private static native void setId(com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO instance, java.lang.Long value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO::id = value;
  }-*/;
  
  private static native com.projectmas.gwt.reelvision.shared.dto.MovieDTO getMovie(com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO::movie;
  }-*/;
  
  private static native void setMovie(com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO instance, com.projectmas.gwt.reelvision.shared.dto.MovieDTO value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO::movie = value;
  }-*/;
  
  private static native com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO getRepertoire(com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO::repertoire;
  }-*/;
  
  private static native void setRepertoire(com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO instance, com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO::repertoire = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO instance) throws SerializationException {
    setId(instance, (java.lang.Long) streamReader.readObject());
    setMovie(instance, (com.projectmas.gwt.reelvision.shared.dto.MovieDTO) streamReader.readObject());
    setRepertoire(instance, (com.projectmas.gwt.reelvision.shared.dto.RepertoireDTO) streamReader.readObject());
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO instance) throws SerializationException {
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getMovie(instance));
    streamWriter.writeObject(getRepertoire(instance));
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.ShowcaseDTO)object);
  }
  
}
