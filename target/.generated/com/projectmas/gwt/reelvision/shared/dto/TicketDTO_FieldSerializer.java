package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class TicketDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getId(com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.TicketDTO::id;
  }-*/;
  
  private static native void setId(com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance, java.lang.Long value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.TicketDTO::id = value;
  }-*/;
  
  private static native java.lang.Double getPrice(com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.TicketDTO::price;
  }-*/;
  
  private static native void setPrice(com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance, java.lang.Double value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.TicketDTO::price = value;
  }-*/;
  
  private static native com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO getScreening(com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.TicketDTO::screening;
  }-*/;
  
  private static native void setScreening(com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance, com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.TicketDTO::screening = value;
  }-*/;
  
  private static native com.projectmas.gwt.reelvision.shared.dto.SeatDTO getSeat(com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.TicketDTO::seat;
  }-*/;
  
  private static native void setSeat(com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance, com.projectmas.gwt.reelvision.shared.dto.SeatDTO value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.TicketDTO::seat = value;
  }-*/;
  
  private static native com.projectmas.gwt.reelvision.shared.dto.TicketDTO.TicketType getTicketType(com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.TicketDTO::ticketType;
  }-*/;
  
  private static native void setTicketType(com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance, com.projectmas.gwt.reelvision.shared.dto.TicketDTO.TicketType value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.TicketDTO::ticketType = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance) throws SerializationException {
    setId(instance, (java.lang.Long) streamReader.readObject());
    setPrice(instance, (java.lang.Double) streamReader.readObject());
    setScreening(instance, (com.projectmas.gwt.reelvision.shared.dto.ScreeningDTO) streamReader.readObject());
    setSeat(instance, (com.projectmas.gwt.reelvision.shared.dto.SeatDTO) streamReader.readObject());
    setTicketType(instance, (com.projectmas.gwt.reelvision.shared.dto.TicketDTO.TicketType) streamReader.readObject());
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.TicketDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.projectmas.gwt.reelvision.shared.dto.TicketDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.TicketDTO instance) throws SerializationException {
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getPrice(instance));
    streamWriter.writeObject(getScreening(instance));
    streamWriter.writeObject(getSeat(instance));
    streamWriter.writeObject(getTicketType(instance));
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.TicketDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.TicketDTO_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.TicketDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.TicketDTO_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.TicketDTO)object);
  }
  
}
