package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class TicketDTO_TicketType_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.TicketDTO.TicketType instance) throws SerializationException {
    // Enum deserialization is handled via the instantiate method
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.TicketDTO.TicketType instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int ordinal = streamReader.readInt();
    com.projectmas.gwt.reelvision.shared.dto.TicketDTO.TicketType[] values = com.projectmas.gwt.reelvision.shared.dto.TicketDTO.TicketType.values();
    assert (ordinal >= 0 && ordinal < values.length);
    return values[ordinal];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.TicketDTO.TicketType instance) throws SerializationException {
    assert (instance != null);
    streamWriter.writeInt(instance.ordinal());
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.TicketDTO_TicketType_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.TicketDTO_TicketType_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.TicketDTO.TicketType)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.TicketDTO_TicketType_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.TicketDTO.TicketType)object);
  }
  
}
