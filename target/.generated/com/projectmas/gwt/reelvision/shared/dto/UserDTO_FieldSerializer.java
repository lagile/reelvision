package com.projectmas.gwt.reelvision.shared.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UserDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getCreatedAt(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::createdAt;
  }-*/;
  
  private static native void setCreatedAt(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance, java.util.Date value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::createdAt = value;
  }-*/;
  
  private static native java.lang.Long getId(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::id;
  }-*/;
  
  private static native void setId(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance, java.lang.Long value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::id = value;
  }-*/;
  
  private static native java.util.Date getLastLogin(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::lastLogin;
  }-*/;
  
  private static native void setLastLogin(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance, java.util.Date value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::lastLogin = value;
  }-*/;
  
  private static native java.lang.String getLogin(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::login;
  }-*/;
  
  private static native void setLogin(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::login = value;
  }-*/;
  
  private static native java.lang.String getPassword(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::password;
  }-*/;
  
  private static native void setPassword(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::password = value;
  }-*/;
  
  private static native com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO getPrivClassDTO(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::privClassDTO;
  }-*/;
  
  private static native void setPrivClassDTO(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance, com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::privClassDTO = value;
  }-*/;
  
  private static native java.lang.String getSessionId(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::sessionId;
  }-*/;
  
  private static native void setSessionId(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance, java.lang.String value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::sessionId = value;
  }-*/;
  
  private static native java.util.Date getUpdatedAt(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance) /*-{
    return instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::updatedAt;
  }-*/;
  
  private static native void setUpdatedAt(com.projectmas.gwt.reelvision.shared.dto.UserDTO instance, java.util.Date value) 
  /*-{
    instance.@com.projectmas.gwt.reelvision.shared.dto.UserDTO::updatedAt = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.projectmas.gwt.reelvision.shared.dto.UserDTO instance) throws SerializationException {
    setCreatedAt(instance, (java.util.Date) streamReader.readObject());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setLastLogin(instance, (java.util.Date) streamReader.readObject());
    setLogin(instance, streamReader.readString());
    setPassword(instance, streamReader.readString());
    setPrivClassDTO(instance, (com.projectmas.gwt.reelvision.shared.dto.PrivClassDTO) streamReader.readObject());
    setSessionId(instance, streamReader.readString());
    setUpdatedAt(instance, (java.util.Date) streamReader.readObject());
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.projectmas.gwt.reelvision.shared.dto.UserDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.projectmas.gwt.reelvision.shared.dto.UserDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.projectmas.gwt.reelvision.shared.dto.UserDTO instance) throws SerializationException {
    streamWriter.writeObject(getCreatedAt(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getLastLogin(instance));
    streamWriter.writeString(getLogin(instance));
    streamWriter.writeString(getPassword(instance));
    streamWriter.writeObject(getPrivClassDTO(instance));
    streamWriter.writeString(getSessionId(instance));
    streamWriter.writeObject(getUpdatedAt(instance));
    
    com.projectmas.gwt.reelvision.shared.dto.ObjectDTO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.projectmas.gwt.reelvision.shared.dto.UserDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.UserDTO_FieldSerializer.deserialize(reader, (com.projectmas.gwt.reelvision.shared.dto.UserDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.projectmas.gwt.reelvision.shared.dto.UserDTO_FieldSerializer.serialize(writer, (com.projectmas.gwt.reelvision.shared.dto.UserDTO)object);
  }
  
}
